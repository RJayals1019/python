# record = {'PUN': 'I3Mwc'}
PUN='I3Mwc'
pun_dic = {'F': 'Fine', 'P': 'Post Poned', 'M': 'Month', 'w': 'Without', 'I': 'Increment', 'c': 'Cumulative'}

# Split the 'PUN' field into individual characters
pun_list = list(PUN)
print(pun_list)
output = []

for i, char in enumerate(pun_list):
    # If the current character is a digit, concatenate it with the previous digit(s)
    if char.isdigit() and i > 0 and pun_list[i-1].isdigit():
        output[-1] = output[-1] + char
    else:
        # Get the corresponding meaning from the 'pun_dic' dictionary
        if char in pun_dic:
            output.append(pun_dic[char])
        else:
            output.append(char)

# Join the output list into a string
output_str = ' '.join(output)

# Print the final output string
print(output_str)
