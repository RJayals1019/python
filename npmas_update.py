################################
#UPDATE NPMAS DBBASE 4 TABLE
#INTO MYSQL TABLE
# THIS CAN NE DONE MULTIPLEY TIMES.
################################

import tkinter as tk
import dbcon
import pymysql
from tkinter import filedialog
import dbfread
import os

class npmas_update(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')

        def dbf_browse():
            browse_btn.config(state='disabled')
            dbf_filepath = filedialog.askopenfilename(filetypes=[("dBase IV Files", "*.dbf")])
            dbf_filename = os.path.basename(dbf_filepath)
            
            select_ent.delete(0,tk.END)
            select_ent.insert(0,dbf_filename)

            #Delete query 
            update_con=dbcon.connect1()
            update_cur=update_con.cursor()
            delete_query = "DELETE FROM npmas"
            update_cur.execute(delete_query)
            update_con.commit()
            lbl2.config(text='Exisiting Records deleted')
            # Insert query
            # Read records from dBase IV table
            dbf_table = dbfread.DBF(dbf_filepath)
            # print(dbf_table)
            # Get the field names from the dBase IV table
            field_names = dbf_table.field_names
            # print(field_names)
            # Generate the placeholder string for the insert query
            placeholders = ', '.join(['%s'] * len(field_names))

            # Insert records into MySQL table
            insert_query = f"INSERT INTO npmas ({', '.join(field_names)}) VALUES ({placeholders})"
            rec_count=0
            for record in dbf_table:
                values = tuple(record.values())
                update_cur.execute(insert_query, values)
                rec_count+=1
                lbl3.config(text=f"Processing... Records updated: {rec_count}")
                lbl3.update_idletasks()
            
            # lbl3_update = tk.StringVar()
            # lbl3_update.set(f'Records updated: {rec_count}')
            # lbl3.config(textvariable=lbl3_update)
            browse_btn.config(state='active')
            update_con.commit()
            update_con.close()

        ### Frame
        self.topframe=tk.LabelFrame(self,text='Update Employee Master')
        self.topframe.grid(row=0,column=0)
        

        ###Label and Entries
        lbl1=tk.Label(self.topframe,text='Select Emp Master :')
        lbl1.grid(row=0,column=0)

        select_ent=tk.Entry(self.topframe,width='12')
        select_ent.grid(row=0,column=1)

        browse_btn=tk.Button(self.topframe,text='Browse',command=dbf_browse)
        browse_btn.grid(row=0,column=2)

        lbl2=tk.Label(self.topframe,text='Browse to update....')
        lbl3=tk.Label(self.topframe,text=' ')
        
        lbl2.grid(row=1,column=0,columnspan=2)
        lbl3.grid(row=2,column=0,columnspan=3)


        for widget in self.topframe.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
            widget.update_idletasks()

        self.place(width=800,height=500, x=0,y=0)
        # self.grid_propagate(False)
        # self.update_idletasks()
        self.update()



# if __name__ == "__main__":
#      root = tk.Tk()
#      npmas_update = npmas_update(root)
#      npmas_update.mainloop()
