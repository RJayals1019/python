import tkinter as tk
from tkinter import ttk
import dbcon
from datetime import datetime
from tkinter import messagebox
from tkcalendar import DateEntry
import calen_class

class class_corr(tk.Frame):
    
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')

    # Frames
        self.top_frame=tk.LabelFrame(self,text='Basic',width=790,height=100,background='#C9C0BB')
        self.bottom_frame=tk.LabelFrame(self,text='Correction Pane',width=790,height=380,background='#C9C0BB')

        self.top_frame.grid(row=0,column=0)
        self.bottom_frame.grid(row=1,column=0)

        # Load data from alldis table
        def load_data():
            # for widget in self.bottom_frame.winfo_children():
            #     widget.configure(state='normal')

            case_ent_data=case_ent.get()
            seat_ent_data=seat_ent.get()
            curr_ent_data=curr_ent.get()
            #INTIAL DELETE
            case_entb.configure(state='normal')  
            case_entb.delete(0,tk.END)
            seat_entb.configure(state='normal')
            seat_entb.delete(0,tk.END)
            curr_entb.configure(state='normal')
            curr_entb.delete(0,tk.END)
            case_entb.configure(state='readonly') 
            seat_entb.configure(state='readonly') 
            curr_entb.configure(state='readonly')   
            cat_ent2.delete(0,tk.END)          
            month_ent5.delete(0,tk.END)
            year_ent6.delete(0, tk.END)
            empno_ent7.delete(0, tk.END)
            branch_ent8.delete(0, tk.END)
            veh_ent9.delete(0, tk.END)
            off_code_ent10.delete(0, tk.END)
            off_other_ent22.delete(0, tk.END)
            off_dt1_ent11.delete(0, tk.END)
            off_dt2_ent12.delete(0, tk.END)
            off_rem_ent15.delete(0, tk.END)
            add_rem_ent16.delete(0, tk.END)
            ref_no_ent17.delete(0, tk.END)
            from_ent19.delete(0, tk.END)
            
            corr_data=dbcon.connect1()
            corr_data=corr_data.cursor()
            with corr_data as cursor:
                corr_query = "SELECT * FROM ALLDIS WHERE SNO=%s AND SEAT_NO=%s AND CRTNO=%s"
                cursor.execute(corr_query, (case_ent_data, seat_ent_data, curr_ent_data))
                corr_result=cursor.fetchall()

                if corr_result:
                    for widget in self.bottom_frame.winfo_children():
                        widget.configure(state='normal')
                        
                    for row in corr_result:
                        global cat_rec, case_rec, seat_rec, crt_rec, empno_rec, month_rec, year_rec, branch_rec, veh_rec, off_code_rec, off_other_rec, off_dt1_rec, off_dt2_rec, off_rem_rec, add_rem_rec, ref_no_rec, ref_dt_rec, from_rec

                        # print(row)
                        cat_rec=row[1]
                        case_rec=row[4]
                        seat_rec=row[5]
                        crt_rec=row[6]
                        empno_rec=row[7]
                        month_rec=row[2]
                        year_rec=row[3]
                        branch_rec=row[10]
                        veh_rec=row[11]
                        off_code_rec=row[12]
                        off_other_rec=row[50]
                        off_dt1_rec=row[15]
                        off_dt2_rec=row[51]
                        off_rem_rec=row[13]
                        add_rem_rec=row[14]
                        ref_no_rec=row[52]
                        ref_dt_rec=row[53]
                        from_rec=row[54]

                        # cat_ent2.delete(0,tk.END)
                        if cat_rec==1:
                            cat_ent2.current(0)
                        elif cat_rec==2:
                            cat_ent2.current(3)    
                        elif cat_rec==3:
                            cat_ent2.current(2)    
                        elif cat_rec==4:
                            cat_ent2.current(1)    

                        case_entb.delete(0,tk.END)
                        case_entb.insert(0,case_rec)
                        seat_entb.delete(0,tk.END)
                        seat_entb.insert(0,seat_rec)
                        curr_entb.delete(0,tk.END)
                        curr_entb.insert(0,crt_rec)
                        case_entb.configure(state='readonly')
                        seat_entb.configure(state='readonly')
                        curr_entb.configure(state='readonly')
                        month_ent5.delete(0,tk.END)
                        month_ent5.insert(0,month_rec)
                        year_ent6.delete(0, tk.END)
                        year_ent6.insert(0,year_rec)
                        empno_ent7.delete(0, tk.END)
                        empno_ent7.insert(0, empno_rec)
                        branch_ent8.delete(0, tk.END)
                        branch_ent8.insert(0,branch_rec)
                        veh_ent9.delete(0, tk.END)
                        if veh_ent9:
                            veh_ent9.insert(0,veh_rec)
                        else:
                            veh_ent9.insert(0,' ')    
                        off_code_ent10.delete(0, tk.END)
                        off_code_ent10.set(off_code_rec)
                        off_code_lbl.config(text=' ')
                        off_other_ent22.delete(0, tk.END)
                        if off_other_rec:
                            off_other_ent22.insert(0, str(off_other_rec))
                        else :
                            off_other_ent22.insert(0, ' ')      

                        off_dt1_ent11.delete(0, tk.END)
                        if off_dt1_rec:
                            offdate_obj1 = datetime.strptime(str(off_dt1_rec), '%Y-%m-%d')
                            date_str1 = datetime.strftime(offdate_obj1, '%d-%m-%Y')
                            off_dt1_ent11.insert(0, date_str1)
                        else:    
                            off_dt1_ent11.delete(0, tk.END)

                        off_dt2_ent12.delete(0, tk.END)
                        if off_dt2_rec:
                            # print(off_dt2_rec)
                            offdate_obj3 = datetime.strptime(str(off_dt2_rec), '%Y-%m-%d')
                            date_str2 = datetime.strftime(off_dt2_rec, '%d-%m-%Y')
                            off_dt2_ent12.insert(0,date_str2)
                        else:
                            # print(off_dt2_rec)
                            off_dt2_ent12.delete(0,tk.END)

                        off_rem_ent15.delete(0, tk.END)
                        off_rem_ent15.insert(0, off_rem_rec)
                        add_rem_ent16.delete(0, tk.END)
                        add_rem_ent16.insert(0, add_rem_rec)
                        ref_no_ent17.delete(0, tk.END)
                        if ref_no_rec:
                            ref_no_ent17.insert(0, str(ref_no_rec))
                        else:
                            ref_no_ent17.insert(0,' ')

                        ref_dt_ent18.delete(0, tk.END)
                        if ref_dt_rec:
                            refdate_obj=datetime.strptime(str(ref_dt_rec),'%Y-%m-%d')
                            refdate_obj=datetime.strftime(ref_dt_rec,'%d-%m-%Y')
                            ref_dt_ent18.insert(0,refdate_obj)
                        else:
                            ref_dt_ent18.insert(0,' ') 

                        # ref_dt_ent18.insert(0, ref_dt_rec)
                        from_ent19.delete(0, tk.END)
                        if from_rec:
                            from_ent19.insert(0, str(from_rec))
                        else:
                            from_ent19.insert(0,' ')    
                    cat_ent2.focus_set()
                                      

                else:
                    for widget in self.bottom_frame.winfo_children():
                        widget.configure(state='disabled')
                    messagebox.showerror("No Data","No Data avaialble-Check your entries ")
            
            cursor.close()
            corr_data.close()

        def update_corr():
            if cat_ent2.get()=='ADMIN':
                up_cat=1
            elif cat_ent2.get()=='CONDUCTOR':    
                up_cat=4
            elif cat_ent2.get()=='DRIVER':
                up_cat=3
            elif cat_ent2.get()=='TECH':
                up_cat=2 

            up_month_rec1=month_ent5.get()
            up_year_ent6=year_ent6.get()
            up_empno_ent7=empno_ent7.get()
            up_branch_ent8=branch_ent8.get()
            if veh_ent9.get():
                up_veh_ent9=veh_ent9.get()
            else:
                up_veh_ent9=' '
            
            up_off_code_ent10=off_code_ent10.get()

            if off_other_ent22.get():
                up_off_other_ent22=off_other_ent22. get()
            else:   
                up_off_other_ent22=' ' 

            if off_dt1_ent11.get():
                up_off_dt1_ent11= (lambda x: datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(off_dt1_ent11.get())
            else:
                up_off_dt1_ent11=None 

            if off_dt2_ent12.get():
                up_off_dt2_ent12=(lambda x: datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(off_dt2_ent12.get())
            else:
                up_off_dt2_ent12=None

            if off_rem_ent15.get():
                up_off_rem_ent15=off_rem_ent15. get()
            else:
                up_off_rem_ent15=' '

            if add_rem_ent16.get():
                up_add_rem_ent16=add_rem_ent16. get()
            else:
                up_add_rem_ent16=' '

            if ref_no_ent17.get():
                up_ref_no_ent17=ref_no_ent17. get()
            else:
                up_ref_no_ent17=' '

            if ref_dt_ent18.get().strip():
                up_ref_dt_ent18=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ref_dt_ent18.get())
            else:
                up_ref_dt_ent18=None

            if from_ent19.get():
                up_from_ent19=from_ent19.get()
            else:
                up_from_ent19=' '    

            # update query
            try:
                upcorr_conn = dbcon.connect1()
                upcorr_data = upcorr_conn.cursor()

                update_query = ''' UPDATE ALLDIS SET CAT=%s,MTH=%s,YEA=%s,STNO=%s,BRANCH=%s,VEHNO=%s,OFFEN=%s,OFF_OTHERS=%s,DT_OFFEN=%s,
                                    DT_OFFEN2=%s,OFF_REM=%s,OFF_REM2=%s,REF_NO=%s,REF_DATE=%s,REF_FROM=%s
                                    WHERE SNO=%s AND SEAT_NO=%s AND CRTNO=%s '''
                value=(up_cat,up_month_rec1,up_year_ent6, up_empno_ent7, up_branch_ent8, up_veh_ent9, up_off_code_ent10, 
                       up_off_other_ent22, up_off_dt1_ent11, up_off_dt2_ent12, up_off_rem_ent15, up_add_rem_ent16, up_ref_no_ent17,
                       up_ref_dt_ent18, up_from_ent19, case_rec,seat_rec, crt_rec)
                
                upcorr_data.execute(update_query, value     )
                upcorr_conn.commit()
                print(upcorr_data.rowcount, "records updated successfully.")
                ##after updation clear screen
                for widget in self.bottom_frame.winfo_children():
                    widget.configure(state='normal')
                    if isinstance(widget, tk.Entry):
                        widget.delete(0, tk.END)
                    widget.configure(state='disabled')    
            except Exception as e:
                print("Error updating records:", e)
            finally:
                upcorr_data.close()
                upcorr_conn.close()


        #Label Widgets 
        case_lbl=tk.Label(self.top_frame,text='Case no',bg='#C9C0BB')
        seat_lbl=tk.Label(self.top_frame,text='Seat no',bg='#C9C0BB')
        curr_lbl=tk.Label(self.top_frame,text='Current no',bg='#C9C0BB')
        #Entry Widgets
        case_ent=tk.Entry(self.top_frame,width=10)
        seat_ent=tk.Entry(self.top_frame,width=10)
        curr_ent=tk.Entry(self.top_frame,width=10)
        # Load Data
        load_btn=tk.Button(self.top_frame,text='Get Data',command=load_data)
        load_btn.grid(row=1,column=3,sticky='EW')
        case_lbl.grid(row=0,column=0, sticky='EW')
        seat_lbl.grid(row=0,column=1, sticky='EW')
        curr_lbl.grid(row=0,column=2, sticky='EW')
        case_ent.grid(row=1,column=0, padx=50, pady=5, sticky='EW')
        seat_ent.grid(row=1,column=1, padx=50, pady=5, sticky='EW')
        curr_ent.grid(row=1,column=2, padx=50, pady=5, sticky='EW')
        
        #Bottom Frame widgets
        global tcase1
        tcase1=dbcon.lastcaseno()
        off_data=dbcon.offcode()
        offence_values = [d["O_CODE"] for d in off_data]
        sorted_offence_values = sorted(offence_values, key=lambda x: x)
        # offence_remarks = [d["OFFENCE"] for d in off_data]

        def on_off_sele(event):
            selected_offence = off_code_ent10.get()
            for d in off_data:
                if d["O_CODE"] == selected_offence:
                    # offence_remarks = d["OFFENCE"]
                    offence_remarks=d["OFFENCE"][:25] + "..." if len(d["OFFENCE"]) > 25 else d["OFFENCE"]
                    off_lbl.configure(text=offence_remarks)
                    break

        #Label Widgets 
        case_lblb=tk.Label(self.bottom_frame,text='Case no',bg='#C9C0BB')
        seat_lblb=tk.Label(self.bottom_frame,text='Seat no',bg='#C9C0BB')
        curr_lblb=tk.Label(self.bottom_frame,text='Current no',bg='#C9C0BB')
        #Entry Widgets
        case_entb=tk.Entry(self.bottom_frame,width=10)
        seat_entb=tk.Entry(self.bottom_frame,width=10)
        curr_entb=tk.Entry(self.bottom_frame,width=10)
        
        cat_lbl=tk.Label(self.bottom_frame,text="Category :",bg='#C9C0BB')
        month_lbl=tk.Label(self.bottom_frame,text="Month :",bg='#C9C0BB')  
        year_lbl=tk.Label(self.bottom_frame,text="Year :",bg='#C9C0BB')      
        empno_lbl=tk.Label(self.bottom_frame,text="Emp No :",bg='#C9C0BB')       
        branch_lbl=tk.Label(self.bottom_frame,text="Branch :",bg='#C9C0BB')      
        veh_lbl=tk.Label(self.bottom_frame,text="Vehicle No :",bg='#C9C0BB')
        off_code_lbl=tk.Label(self.bottom_frame,text="Offence Code :",bg='#C9C0BB')
        off_dt1_lbl=tk.Label(self.bottom_frame,text="Offence Date1 :",bg='#C9C0BB')
        off_dt2_lbl=tk.Label(self.bottom_frame,text="Offence Date 2 :",bg='#C9C0BB')
        off_lbl=tk.Label(self.bottom_frame,text='Offence',bg='#C9C0BB',width=25,anchor='w')
        off_rem_lbl=tk.Label(self.bottom_frame,text="Offence Remarks :",bg='#C9C0BB')
        add_rem_lbl=tk.Label(self.bottom_frame,text="Additional Remarks :",bg='#C9C0BB')
        ref_no_lbl=tk.Label(self.bottom_frame,text="Ref. No :",bg='#C9C0BB')
        ref_dt_lbl=tk.Label(self.bottom_frame,text="Ref. Date :",bg='#C9C0BB')
        from_lbl=tk.Label(self.bottom_frame,text="From :",bg='#C9C0BB')
       

        cat_lbl.grid(row=0, column=0, padx=3, pady=3, sticky="e")
        month_lbl.grid(row=1,column=0,padx=3,pady=3,sticky="e")
        year_lbl.grid(row=2,column=0,padx=3,pady=3,sticky="e")
        empno_lbl.grid(row=3,column=0,padx=3,pady=3,sticky="e")
        branch_lbl.grid(row=4,column=0,padx=3,pady=3,sticky="e")
        veh_lbl.grid(row=5,column=0,padx=3,pady=3,sticky="e")
        off_code_lbl.grid(row=6,column=0,padx=3,pady=3,sticky="e")
        off_dt1_lbl.grid(row=8,column=0,padx=3,pady=3,sticky="e")
        off_dt2_lbl.grid(row=9,column=0,padx=3,pady=3,sticky="e")
        off_lbl.grid(row=6,column=1,padx=3,pady=3,sticky="w")
       
        off_rem_lbl.grid(row=0,column=2,padx=3,pady=3,sticky="e")
        add_rem_lbl.grid(row=1,column=2,padx=3,pady=3,sticky="e")
        ref_no_lbl.grid(row=2,column=2,padx=3,pady=3,sticky="e")
        ref_dt_lbl.grid(row=3,column=2,padx=3,pady=3,sticky="e")
        from_lbl.grid(row=4,column=2,padx=3,pady=3,sticky="e")
        case_lblb.grid(row=6,column=2,sticky="e")
        seat_lblb.grid(row=7,column=2,sticky="e")
        curr_lblb.grid(row=8,column=2,sticky="e")
        # curr_ent4=tk.Entry(self.bottom_frame,width=15)                                         # case no
        cat_list=["ADMIN","CONDUCTOR","DRIVER","TECH"]
        cat_ent2=ttk.Combobox(self.bottom_frame,values=cat_list,width=15)
        # cat_ent2.focus_set()
        month_ent5=tk.Entry(self.bottom_frame,width=15)                                        # category
        year_ent6=tk.Entry(self.bottom_frame,width=15)                                         # Month
        empno_ent7=tk.Entry(self.bottom_frame,width=15)                                        # year 
        branch_ent8=tk.Entry(self.bottom_frame,width=15)                                       #emp No.
        veh_ent9=tk.Entry(self.bottom_frame,width=15)                                          # Branch
        off_code_ent10=ttk.Combobox(self.bottom_frame,values=sorted_offence_values,width=15)                                       #vehicle
        off_code_ent10.bind('<<ComboboxSelected>>',on_off_sele)
        off_other_ent22=tk.Entry(self.bottom_frame,width=15)
        # off_dt1_ent11=DateEntry(self.bottom_frame,selectmode='day',date_pattern='dd-mm-y')             # offcode
        # off_dt1_ent11=calen_class.NullableDateEntry(self.bottom_frame,selectmode='day',date_pattern='dd-mm-y')             # offcode
        # def validate_date1(new_value):
        #     try:
        #         datetime.strptime(new_value, '%d-%m-%Y')
        #         return True
        #     except ValueError:
        #         return False
            
        # off_dt1_ent11=tk.Entry(self.bottom_frame,width=12,validate="key",validatecommand=(self.register(validate_date1),"%P"))
        off_dt1_ent11=tk.Entry(self.bottom_frame,width=12)
        off_dt2_ent12=tk.Entry(self.bottom_frame,width=12)
        # off_dt2_ent12=DateEntry(self.bottom_frame,selectmode='day',date_pattern='dd-mm-y') 
        # off_dt2_ent12=calen_class.NullableDateEntry(self.bottom_frame,selectmode='day',date_pattern='dd-mm-y') 

        off_rem_ent15=tk.Entry(self.bottom_frame,width=25)                                      # Date of Exp
        add_rem_ent16=tk.Entry(self.bottom_frame,width=25)                                      # offe remar
        ref_no_ent17=tk.Entry(self.bottom_frame,width=25)                                        # additional remarks
        # ref_dt_ent18=DateEntry(self.bottom_frame, selectmode='day', date_pattern='dd-mm-y')            # ref .no
        # ref_dt_ent18=calen_class.NullableDateEntry(self.bottom_frame, selectmode='day', date_pattern='dd-mm-y')            # ref .no
        ref_dt_ent18=tk.Entry(self.bottom_frame,width=12)
        from_ent19=tk.Entry(self.bottom_frame,width=25)       

        # curr_ent4.grid(row=0,column=1,sticky="w",padx=3,pady=3)

        cat_ent2.grid(row=0,column=1,sticky="w",padx=3,pady=3)
        month_ent5.grid(row=1,column=1,sticky="w",padx=3,pady=3)
        year_ent6.grid(row=2,column=1,sticky="w",padx=3,pady=3)
        empno_ent7.grid(row=3,column=1,sticky="w",padx=3,pady=3)
        branch_ent8.grid(row=4,column=1,sticky="w",padx=3,pady=3)
        veh_ent9.grid(row=5,column=1,sticky="w",padx=3,pady=3)
        off_code_ent10.grid(row=6,column=1,sticky="w",padx=3,pady=3)
        off_other_ent22.grid(row=7,column=1,sticky="w",padx=3,pady=3)
        off_dt1_ent11.grid(row=8,column=1,sticky="w",padx=3,pady=3)
        off_dt2_ent12.grid(row=9,column=1,sticky="w",padx=3,pady=3)
       
        off_rem_ent15.grid(row=0,column=3,sticky="w",padx=3,pady=3)
        add_rem_ent16.grid(row=1,column=3,sticky="w",padx=3,pady=3)
        ref_no_ent17.grid(row=2,column=3,sticky="w",padx=3,pady=3)
        ref_dt_ent18.grid(row=3,column=3,sticky="w",padx=3,pady=3)
        from_ent19.grid(row=4,column=3,sticky="w",padx=3,pady=3)
        case_entb.grid(row=6,column=3,sticky="w")
        seat_entb.grid(row=7,column=3,sticky="w")
        curr_entb.grid(row=8,column=3,sticky="w")
        # def update_corr():
        #     pass

        updt_btn=tk.Button(self.bottom_frame,text='Update',command=update_corr)
        updt_btn.grid(row=9,column=3,sticky='w')

        # Placing this self frame at right palce and widgets aligned at rigth way
        self.place(width=800,height=500, x=0,y=0)
        self.grid_propagate(False)
        self.update_idletasks()
        for widget in self.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
        for widget in self.top_frame.winfo_children():
            # widget.grid_configure(padx=15,pady=5,ipadx=1,ipady=1)
            widget.grid_propagate(False)
        for widget in self.bottom_frame.winfo_children():
            widget.grid_propagate(False)
            widget.configure(state='disabled')