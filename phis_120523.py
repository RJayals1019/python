import tkinter as tk
from tkinter import *
from datetime import datetime
from PIL import Image
import dbcon
import pymysql
from docxtpl import DocxTemplate
import os
import shutil
import subprocess
from tkinter import messagebox
from tkcalendar import DateEntry
import mysql.connector
# from tabulate import tabulate
from prettytable import PrettyTable
import os
import textwrap
from collections import Counter
################################ Punishment History Print Details
class phis(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')
       
        ### Frame
        self.topframe=tk.LabelFrame(self,text='Print Punishment History')
        self.topframe.grid(row=0,column=0)
        
        # def phis_print():
        #     phis_conn=dbcon.connect1
        #     phis_cur=phis_conn.cursor()
        #     pquery='select * from alldis,inner join alldis.stno=palldis.stno where STNO=%s'
        #     pvalue=(pnstno)
        #     phis_cur.execute(pquery,pvalue)
        #     result=phis_cur.fetchall()
        #     print(result)
        def phis_print():
            global pnstno
            pnstno=ent_staff.get()
            print(pnstno)
            phis_conn = dbcon.connect1()
            
            
            ###-------------------------NPMAS DATA QUERY
            npmasquery_cur=phis_conn.cursor()
            npmasquery='SELECT * FROM NPMAS WHERE STNO=%s'
            pvalue=(pnstno,)
            npmasquery_cur.execute(npmasquery,pvalue)
            npmas_result=npmasquery_cur.fetchall()
            
            filename1=f"PH{pnstno}.txt"
            filepath = os.path.join("D:\\", filename1)
            
            if npmas_result:
                column_names=[column[0] for column in npmasquery_cur.description]
                # print(column_names)
                npmas_result_dict=[dict(zip(column_names,row)) for row in npmas_result]
                # print(npmas_result_dict)
                for record in npmas_result_dict:
                    with open(filepath,"w") as file:
                        file.write('T.N.S.T.C (SALEM) LTD., DHARMAPURI REGION : : PUNISHMENT HISTORY \n')
                        file.write('-'*96+"\n")
                        # date_fmt=  datetime.strptime(str(record['DATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        npmas_dob= datetime.strptime(str(record['DOB']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        npmas_doj= datetime.strptime(str(record['DOJ']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        # npmas_dreg= datetime.strptime(str(record['DOCT']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        npmas_retd= datetime.strptime(str(record['RTDATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        file.write(f'Name          :{record["NAME"]:<25} | Date of Birth      :{npmas_dob:<15}\n')
                        file.write(f'Staff No.     :{record["STNO"]}-{record["OSTNO"]:<15} | Date of Join       :{npmas_doj:<15}\n')
                        file.write(f"Father's Name :{record['FNAME']:<25} | Date of Regular    :{record['DOCT']:<15}\n")
                        file.write(f'Designation   :{record["DESIGN"]:<26}| Date of Retirement :{npmas_retd:<15}\n')
                        file.write(f'Branch        :{record["BRNAME"]:<26}|\n')
                        # file.write('-'*96+"\n")

                    file.close()
            else:   
                tk.messagebox.showinfo("Info","No data")
            ###-------------------------END OF NPMAS DATE QUERY
            
            ###---------------------- PALLDIS DATA QUERY                     
            phis_cur = phis_conn.cursor()
            pquery = 'SELECT * FROM palldis WHERE STNO = %s'
            pvalue = (pnstno,)
            phis_cur.execute(pquery, pvalue)
            result = phis_cur.fetchall()
            
            fomas_cur=phis_conn.cursor()
            fomas_query='SELECT * FROM FOMAS'
            fomas_cur.execute(fomas_query)
            fomas_result=fomas_cur.fetchall()
            if fomas_result:
                column_names=[column[0] for column in fomas_cur.description]
                # fomas_result_dict=[dict(zip(column_names,row)) for row in fomas_result]
                fomas_result_dict = {row[0]: row[1] for row in fomas_result}
                # print(fomas_result_dict)
            
            if result:
                column_names=[column[0] for column in phis_cur.description]
                # print(column_names)
                result_dict = [dict(zip(column_names, row)) for row in result]
                x=PrettyTable()
                x.field_names=['SN','OFF_DATE','VEH','OFFENCE','PUNISHMENT','FILE/YEAR','F.O.DATE']      ##PUNISHMENT','FILE/YEAR','F.O.DATE']
                x.align['OFFENCE']='l'
                x.align['PUNISHMENT']='l'
                max_width=15
                for j, record in enumerate(result_dict):
                    if record['DATE'] and record['DATE']!= '':
                        date_fmt=  datetime.strptime(str(record['DATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                    else:
                        date_fmt='-'
                    ### TO PRINT THE PUNISHMNET CODE TO TEXT
                    forder_list=list(record['PUN'])
                    forder_output=[]
                    for i,char in enumerate(forder_list):
                        # If the current character is a digit, concatenate it with the previous digit(s)
                        if char.isdigit() and i>0 and forder_list[i-1].isdigit():
                            forder_output[-1]=forder_output[-1]+char
                        else:
                            # Get the corresponding meaning from the 'pun_dic' dictionary
                            if char in fomas_result_dict:
                                forder_output.append(fomas_result_dict[char])
                            else:
                                forder_output.append(char)
                    #join the all the strings
                    prt_punis=' '.join(forder_output)    
                    

                    # x.add_row([j+1,date_fmt,record['VEH'],record['OFF'],prt_punis])
                    # Set the OFFENCE attribute to a fixed width of 15 characters and enable word wrap

                    wrapped_offence = textwrap.fill(record['OFF'], width=20)
                    wrapped_punish = textwrap.fill(prt_punis,width=20)
                    wrapped_file_year=record['SNO']+'/'+str(record['YEA'])
                    if record['FO_DATE'] and record['FO_DATE']!= '':
                        date_fmt1=  datetime.strptime(str(record['FO_DATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                    else:
                        date_fmt1='-'
                    
                    x.add_row([j+1,date_fmt,record['VEH'],wrapped_offence,wrapped_punish,wrapped_file_year,date_fmt1])
               
                # # for abstract :
                # offences = [record['OFF'] for record in result_dict]
                # offence_counts = Counter(offences)
                # # print(offence_counts)
                # for offence, count in offence_counts.items():
                #     print(f'{offence}: {count}')

                # print(x)    
                with open(filepath,"a") as file:
                    file.write(str(x))
                    file.write("\n")
                file.close()
            else:
                tk.messagebox.showinfo("Info","No data")
            ####------------------END OF PALLDIS DATA QUERY
            
            ####------------------QUERY ALLDIS DATA
            y=PrettyTable()
            y.field_names=['SN','OFF_DATE','VEH','OFFENCE','PUNISHMENT','FILE/YEAR','F.O.DATE'] 
            y.header = False
            y.align['OFFENCE']='l'
            y.align['PUNISHMENT']='l'
            max_width=15
            y.min_width['VEH']=4
            y.min_width['F.O.DATE']=10
            y.min_width['OFFENCE']=20
            y.min_width['PUNISHMENT']=18
            
            aldis_cur=phis_conn.cursor()
            aldisquery='SELECT * FROM alldis WHERE STNO=%s'
            pvalue=(pnstno,)
            aldis_cur.execute(aldisquery,pvalue)
            aldis_result=aldis_cur.fetchall()
         
            offmas_cur=phis_conn.cursor()
            offmasquery='SELECT OFFEN, OFFENCE FROM offmas'
            offmas_cur.execute(offmasquery)
            offmas_result=offmas_cur.fetchall()
            
            if offmas_result:
                offmas_offence=[]
                column_names=[column[0] for column in offmas_cur.description]
                offmas_result_dict={row[0]:row[1] for row in offmas_result}
                # print(offmas_result_dict)
                for row in offmas_result:
                    offmas_result_dict1 = dict(zip(column_names, row))
                    offmas_offence.append(offmas_result_dict1['OFFENCE'])
                    # offmas_offence.append(offmas_result_dict['OFFENCE'])
                # print(offmas_offence)

            if aldis_result:
                with open(filepath,"a") as file:
                    file.write('File in process..\n')
                file.close()
                column_names = [column[0] for column in aldis_cur.description]
                SN=0
                al_dis_offences=[]
                for row in aldis_result:
                    al_dis_record = dict(zip(column_names, row))
                    # al_dis_offences.append(al_dis_record['OFFENCE'])
                    al_dis_offences.append(al_dis_record.get(row))
                    print(al_dis_offences)    
                    # print(al_dis_record)
                for items in al_dis_offences:
                    al_dis_offecode=[]
                    print(items)
                    al_dis_offecode.append(offmas_result_dict1.get(items))   
                    print(al_dis_offecode) 
                # for h,record in enumerate(al_dis_record):
                    # print(h,record)
                    # if record['DT_OFFEN'] is not None:
                    # if 'DT_OFFEN' in record and al_dis_record['DT_OFFEN'] is not None:
                    if al_dis_record['DT_OFFEN'] is not None:
                        date_fmt2=  datetime.strptime(str(al_dis_record['DT_OFFEN']),'%Y-%m-%d').strftime('%d-%m-%Y')
                    else:
                        date_fmt2='-'

                    off_code1=al_dis_record['OFFEN']
                    if off_code1:
                        off_code1_dis=offmas_result_dict.get(off_code1)
                    else:
                        off_code1_dis=al_dis_record['OFF_REM']
                    # print(off_code1_dis)
                    SN=+1
                    wrapped_sn=textwrap.fill(str(SN),width=3)    
                    wrapped_offence2=textwrap.fill(off_code1_dis,width=20)
                    wrapped_punish2=textwrap.fill('**pending**',width=20)
                    wrapped_file_year1=al_dis_record['SNO']+'/'+str(al_dis_record['YEA'])
                    wrapped_vehno1 = textwrap.fill(al_dis_record['VEHNO'], width=20)
                    wrapped_fodate1=textwrap.fill(' ',width=10)
                    y.add_row([wrapped_sn,date_fmt2,wrapped_vehno1,wrapped_offence2,wrapped_punish2,wrapped_file_year1,wrapped_fodate1])
                    SN=+1
                    # print(y)    

            with open(filepath,"a") as file:
                file.write(str(y))
            file.close()
        # else:
            offences =[record['OFF'] for record in result_dict]
            offences.extend(al_dis_offences)
            # offences.extend(offmas_offence)
            # print(offences)
            offence_counts = Counter(offences)
            # print(offences)
            # print(al_dis_offences)
            # sorted_counts = dict(sorted(offence_counts.items()))

            offence_list = []
            count_list = []
            for offence, count in offence_counts.items():
                offence_list.append(offence)
                count_list.append(count)
            # print(offence_counts)
            with open(filepath,"a") as file:
                file.write('\nABSTRACT\n')
                file.write('--------\n')
                # Calculate the number of offenses to be printed in each column
                num_offences = len(offence_list)
                num_offences_col1 = (num_offences + 1) // 2
                num_offences_col2 = num_offences - num_offences_col1

                # Print offenses and counts in two columns
                for i in range(num_offences_col1):
                    file.write(f"{offence_list[i]:<33} - {count_list[i]}")
                    if i + num_offences_col1 < num_offences:
                            file.write(f"    {offence_list[i+num_offences_col1]:<33} - {count_list[i+num_offences_col1]}")
                    file.write("\n")
                   
            file.close()
            ####------------------END OF ALLDIS DAT

        # WIDGETS
        lbl_staff=tk.Label(self.topframe,text='New Staff No :')
        lbl_staff.grid(row=0,column=0)
        ent_staff=tk.Entry(self.topframe,width=15)
        ent_staff.grid(row=0,column=1)
        bnt_submit=tk.Button(self.topframe,text='Submit',command=phis_print)
        bnt_submit.grid(row=0,column=2)
        
        lbl_ostaff=tk.Label(self.topframe,text='Old Staff No :')
        lbl_ostaff.grid(row=1,column=0)
        ent_ostaff=tk.Entry(self.topframe,width=15)
        ent_ostaff.grid(row=1,column=1)
        bnt_osubmit=tk.Button(self.topframe,text='Submit')
        bnt_osubmit.grid(row=1,column=2)



        
        ############################ PADDING AND ALIGMENT
        for widget in self.topframe.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
            widget.update_idletasks()

        ########################### TO DISPLAY ALL WIDGETS
        self.place(width=800,height=500, x=0,y=0)
        self.grid_propagate(False)
        self.update_idletasks()



if __name__ == "__main__":
     root = tk.Tk()
     phis_show = phis(root)
     phis_show.mainloop()

