import tkinter as tk
from tkinter import *
# import memo_entry
from tkinter import messagebox
import tkinter as ttk
from tkinter import ttk
from dbfread import DBF
from docxtpl import DocxTemplate
import datetime
import subprocess
from datetime import datetime
import os
import dbfread
import shutil
from PIL import Image, ImageTk

class classmemo_prt(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')
        
        self.mpath=r'd:\main_python\disp\memo_temp'
        self.tpath=r'd:\disp_prints'    

        import os
        import shutil
        import dbfread
        import dbf
        from dbf import Table, Field
        # Create DBF from 

        self.alldis_table = Table(os.path.join(self.tpath,'alldis.dbf'))
        self.alldis_table.open(mode=dbf.READ_WRITE)

    #############  Top_frame
        top_frame = LabelFrame(self,width=100,height=550,bg="#900C3F")
        top_frame.grid(row=0,column=0,padx=5,pady=5,rowspan=2,sticky='nsew')
        top_frame.grid_propagate(False)
    #############

        template_files = {
            'UA': os.path.join(self.mpath, 'ABSMEMO_CUT.DOCX'),
            'FC': 'template2.docx',
            'DT': os.path.join(self.mpath,'TYREMEMO_SUS.DOCX'),
                       }
        template_files1={key: os.path.join(self.mpath, 'DAMAGE.DOCX') for key in [ 'DL', 'DK','DQ','DB','D1','D3','D2','D5','D4','D6','D7','DM','DN'
                                                                    'DL','DA','DS','D8','DZ','D9','DV','DO']}
        template_files2={key: os.path.join(self.mpath,'FNC_MEMO.DOCX') for key in ['FN','FF','LQ','LN']}
        template_files.update(template_files1)
        template_files.update(template_files2)
        
        # memo_entry = LabelFrame(self.top_frame,width=600,height=150)
        # memo_entry.grid(row=0,column=1,padx=5,pady=5,sticky='nsew')
        # memo_entry.grid_propagate(False)
        
        lbl_seat_no=Label(self.top_frame,text='Seat No:')
        lbl_seat_no.grid(row=0,column=0)
        lbl_sno=Label(self.top_frame,text='Sno    :')
        lbl_sno.grid(row=1,column=0)
        lbl_crtno=Label(self.top_frame,text='CRTNO   :')
        lbl_crtno.grid(row=2,column=0)
        
        ent_seat_no=Entry(self.top_frame,width=15)
        ent_seat_no.grid(row=0,column=1)
        ent_seat_no.focus_set()
        ent_sno=Entry(self.top_frame,width=15)
        ent_sno.grid(row=1,column=1)
        ent_crtno=Entry(self.top_frame,width=15)
        ent_crtno.grid(row=2,column=1)
       
        #############  bottom Frame
        # bottom_frame = LabelFrame(self,width=600,height=150)
        # bottom_frame.grid(row=1,column=1,padx=5,pady=5)
        # bottom_frame.grid_propagate(False)
        # txt_box=Text(bottom_frame,width=400,height=40)
        # txt_box.grid(row=0,column=0,padx=3,pady=3,sticky='nsew')
        # txt_box.grid_propagate(False)
    
        # def disp_details():
        #     # pass
        #     txt_box.delete(1.0, tk.END)
        #     seatno1=ent_seat_no.get()
        #     sno1=ent_sno.get()
        #     crtno1=ent_crtno.get()
        #     # txt_box.insert(tk.END, 'Seat No:'+seatno1+','+'Sno:'+sno1+','+'CRTNO:'+crtno1+'\n')
        def return_key_print(event):
            print_details()
            btn_print.focus_clear()
        def print_details():
            global new_reco
            global tostno
            global toffen
            seatno2=ent_seat_no.get().strip()
            sno2=ent_sno.get().strip()
            crtno2=ent_crtno.get().strip()
           
            records=[]
            
            table1=DBF(r'd:\main_python\disp_python\alldis.dbf')
            table2=DBF(r'd:\main_python\disp_python\paymast.dbf')
            alldis_records = list(table1)
            empmas_records = list(table2)
        
            table3=DBF(r'd:\main_python\disp_python\palldis.dbf')
            palldis_records=list(table3)
            table4=DBF(r'd:\main_python\disp_python\npmas.dbf')
            tnpmas=list(table4)
            for record in alldis_records:
                if record['SNO'] == sno2 and record['SEAT_NO'] == seatno2 and record['CRTNO'] == int(crtno2):
                            tstno = record['STNO']
                            toffen=record['OFFEN']
                            for empmas_rec in empmas_records:
                                if empmas_rec['EMPNO'] == tstno:
                                    tempname = empmas_rec['EPMNAME']
                                    break

            fieldstoinclude=['SNO','CRTNO','SEAT_NO','STNO','OSTNO','BRANCH','OFFEN','OFF_REM','DT_OFFEN','BNAME','VEHNO']
            match_found=False
            for record in DBF(r'd:\main_python\disp_python\alldis.dbf'):
                if record['SNO']==sno2 and record['SEAT_NO']==seatno2 and record['CRTNO']==int(crtno2):
                    
                    new_record={}
                    
                    for field in fieldstoinclude:
                        new_record[field]=record[field]
                        tostno=record['STNO']
                    records.append(record)
                    match_found=True
                    break
            if not match_found:
                tk.messagebox.showinfo("","Please check the input details")
                
            ##############  PREVIOUS PUNISHMENT HISTORY
            print(tostno)
            filtered_palldis1=[record for record in palldis_records if record['STNO']==tostno]
            tot_pun=len(filtered_palldis1)
            
            filtered_palldis2=[record for record in filtered_palldis1 
                        if (len(record['PUN']) > 3 and record['PUN'][3] == 'O') 
                    or (len(record['PUN']) > 4 and record['PUN'][4] == 'O')]
            twoc=len(filtered_palldis2)
            # filtered_palldis3=[record for record in filtered_palldis1 if record['PUN'].endswith('C')]
            filtered_palldis3=[record for record in filtered_palldis1 
                        if (len(record['PUN']) > 3 and record['PUN'][3] == 'C') 
                    or (len(record['PUN']) > 4 and record['PUN'][4] == 'C')]
            twc=len(filtered_palldis3)
            filtered_palldis4=[record for record in filtered_palldis1
                            if record['PUN'][0]=='P' and (len(record['PUN'])>2 and record['PUN'][2]=='T')]
            tstg_red=len(filtered_palldis4)
            tbal=tot_pun-twoc-twc-tstg_red
            print(tot_pun)
            print(twoc)
            print(twc)
            print(tstg_red)
            #filtered_records = [r for r in records if r.endswith('O')]
            filtered_npmas=[record for record in tnpmas if record['STNO']==tostno]
            for record in filtered_npmas:
                tbrname=record['BRNAME']
                tdesign=record['DESIGN']   
                print(tbrname)
        ###########################################
            
            date_str=new_record['DT_OFFEN']
            date_obj=datetime.strptime(str(date_str), '%Y-%m-%d')
            formatted_date1=date_obj.strftime('%d-%m-%Y')
            template_file=template_files.get(toffen)
            template_name = os.path.splitext(os.path.basename(template_files[toffen]))[0]
            doc=DocxTemplate(template_file)
            context = {**new_record, "TEMPNAME": tempname,"FORMATTED_DATE1":formatted_date1,"TOT_PUN":tot_pun,"TWOC":twoc,
                    "TWC":twc,"TSTG_RED":tstg_red,"TBAL":tbal,"BRNAME":tbrname,"DESIGN":tdesign}
            doc.render(context)
            ent_seat_no.delete("0",END)
            ent_sno.delete("0",END)
            ent_crtno.delete("0",END)
            doc_name=template_name+str(new_record['OSTNO'])+".docx"
            doc.save(doc_name)
            subprocess.call(['start', '', doc_name], shell=True)
        btn_print=Button(self.top_frame,text='Print File',command=print_details)
        btn_print.grid(row=3,column=1,padx=5,pady=5)
        btn_print.bind("<Return>",return_key_print)

    # btn_memo=Button(Left_frame,text='Memo',width=11,command=frame_memo)
    # btn_memo.grid(row=0,column=0,padx=3,pady=3)




    ###########MEMO ENTRY FRAME



    # root.mainloop()