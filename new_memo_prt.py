from datetime import datetime
from tkinter import *
import tkinter as tk
from tkinter import ttk
import dbcon
import pymysql
from docxtpl import DocxTemplate
import os
import shutil
import subprocess

class memo_prt_class(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')

        self.top_frame=tk.LabelFrame(self,text='Entry',width=790,height=100,background='#C9C0BB')
        self.top_frame.grid(row=0,column=0)
       
        #Labels
        lbl_seat_no=tk.Label(self.top_frame,text='Seat No:',background='#C9C0BB')
        lbl_seat_no.grid(row=0,column=0)
        lbl_sno=tk.Label(self.top_frame,text='Sno    :',background='#C9C0BB')
        lbl_sno.grid(row=1,column=0)
        lbl_crtno=tk.Label(self.top_frame,text='CRTNO   :',background='#C9C0BB')
        lbl_crtno.grid(row=2,column=0)
        
        ent_seat_no=tk.Entry(self.top_frame,width=15)
        ent_seat_no.grid(row=0,column=1)
        ent_seat_no.focus_set()
        ent_sno=tk.Entry(self.top_frame,width=15)
        ent_sno.grid(row=1,column=1)
        ent_crtno=tk.Entry(self.top_frame,width=15)
        ent_crtno.grid(row=2,column=1)
        
        def prt_memo():
            global toffen
            if ent_seat_no.get() and ent_crtno.get() and ent_sno.get() :
                global seat_rec,sno_rec,crtno_rec
                seat_rec=ent_seat_no.get()
                sno_rec=ent_sno.get()
                crtno_rec=ent_crtno.get()
                if crtno_rec.isdigit() and crtno_rec !=' ':
                ## current live alldis
                    crtno_val=int(crtno_rec)
                    crtno_rec=crtno_val
                else:
                    crtno_rec=None    
                # print(crtno_rec)
                try:
                    prt_con = dbcon.connect1()
                    prt_date = prt_con.cursor()
                    # query = 'SELECT * FROM ALLDIS WHERE SEAT_NO=%s AND SNO=%s AND CRTNO=%s'
                    query='SELECT ALLDIS.*, NPMAS.NAME, NPMAS.DESIGN, NPMAS.BRNAME FROM ALLDIS JOIN NPMAS ON ALLDIS.STNO=NPMAS.STNO WHERE SEAT_NO=%s AND SNO=%s AND CRTNO=%s'
                    values = (seat_rec, sno_rec, crtno_rec)
                    prt_date.execute(query, values)
                    result = prt_date.fetchone()
                    # print(result_dict)
                    result_dict={}
                    # print(result_dict)
                    # print("JK")
                    if result:
                        field_names = [i[0] for i in prt_date.description]
                        result_dict = dict(zip(field_names, result))
                        empname=result_dict['NAME']
                        design=result_dict['DESIGN']
                        brname=result_dict['BRNAME']
                        toffen=result_dict['OFFEN']
                        print(toffen)
                        tstno=result_dict['STNO']
                    else:
                        print("No record found.")
                        tstno=None
                    # print(result_dict)
                except Exception as e:
                    print(f"Error: {e}")
                finally:
                    prt_date.close()
                    prt_con.close()
                print(tstno)
                palldis_con=dbcon.connect1()
                palldis_cur = palldis_con.cursor(pymysql.cursors.DictCursor)
                query=('select * from palldis where STNO=%s')
                values=(tstno)
                if tstno:
                    palldis_cur.execute(query,values)
                    result=palldis_cur.fetchall()
                else:
                    tk.messagebox.showerror("No Data","Check your entries ")

                TWOC=0          # WITHOUT CUMULATIVE
                if result:
                    for row in result:
                        pun_value=row.get('PUN')
                        if len(pun_value)>3 and pun_value[3]=='O':
                            # print(pun_value)
                            TWOC+=1

                    TWC=0           # WITH CUMULATIVE
                    for row in result:
                        pun_value=row.get('PUN')
                        if len(pun_value)>3 and pun_value[3]=='Y':
                            # print(pun_value)
                            TWC+=1

                    TSTG_RED=0          # STAGE REDUCTION
                    for row in result:
                        pun_value=row.get('PUN')
                        if len(pun_value)>3 and pun_value[0]=='P' and pun_value[2]=='T':
                            # print(pun_value)
                            TSTG_RED+=1

                    TOTPUN=0
                    for row in result:
                        TOTPUN+=1

                    BALPUN=TOTPUN-TWOC-TWC-TSTG_RED
                    # else:
                    #     TWOC=0
                    #     TWC=0
                    #     TSTG_RED=0 
                    #     TOTPUN=0

                    ######## Data for Template
                    # GET TEMPLATE
                  #  toffen=result_dict['OFFEN']
                    gettoffen=toffen
                    dt_offen = result_dict['DT_OFFEN'].strftime('%d-%m-%Y')
                    memo_con=dbcon.connect1()
                    memo_cur=memo_con.cursor()
                    query='SELECT MEMO_TEMPLATE FROM MEMO_MASTER WHERE OFFEN=%s'
                    print(gettoffen)
                    value=(gettoffen)
                    memo_cur.execute(query,value)
                    result=memo_cur.fetchone()
                    for row in result:
                        if row:
                            template_file=result[0]
                            print(template_file)
                            context={"OFFEN":toffen,"BRNAME":result_dict['BRNAME'],"SNO":result_dict['SNO'],
                                    "SEAT_NO":result_dict['SEAT_NO'],"CRTNO":result_dict['CRTNO'],"STNO":result_dict['STNO'],
                                    "OSTNO":result_dict['OSTNO'],"DT_OFFEN":dt_offen,"DESIGN":result_dict['DESIGN'],
                                    "VEHNO":result_dict['VEHNO'],"EMPNAME":empname,"TWOC":TWOC,"TWC":TWC,"TOTPUN":TOTPUN,"BALPUN":BALPUN,
                                    "TSTG_RED":TSTG_RED}
                            doc=DocxTemplate(template_file)
                            getfilename=doc
                            docsave_name=f'D:\\{os.path.splitext((os.path.basename(template_file)))[0]}{toffen}.DOCX'
                            # print(docsave_name)
                            doc.render(context)
                            doc.save(docsave_name)
                            subprocess.call(['start', '', docsave_name], shell=True)
                            ent_seat_no.delete(0,tk.END)
                            ent_seat_no.focus_set()
                            ent_sno.delete(0,tk.END)
                            ent_crtno.delete(0,tk.END)
                            with open(docsave_name,"r") as file:
                                data = file.read()
                            text_box = Text(self,height=20,width=190)
                            text_box.pack(expand=True)
                            text_box.insert('end',data)
                            text_box.config(state='disabled')

                        else:
                            print('No Template for this offence.Please try it manualy')    
                            tk.messagebox.showerror("No Data","No Template for this offence2")
                else:           
                    tk.messagebox.showerror("No Data","No Template for this offence1") 
            else:
                tk.messagebox.showerror("No Data","Check your entries ")

        #Print Button
        btn_prt=tk.Button(self.top_frame,text='Print',width=15,command=prt_memo)
        btn_prt.grid(row=3,column=1)

        # self.top_frame.grid_propagate(False)

        for widget in self.top_frame.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
            widget.update_idletasks()

        self.place(width=800,height=500, x=0,y=0)
        self.grid_propagate(False)
        self.update_idletasks()



# # # To check this frame
# if __name__ == "__main__":
#     root = tk.Tk()
#     memo_prt = memo_prt_class(root)
#     memo_prt.mainloop()