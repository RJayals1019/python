import tkinter as tk
from tkinter import ttk
import dbcon
from datetime import datetime
from tkinter import messagebox
from tkcalendar import DateEntry
import calen_class

class Entry1(tk.Frame):              # GPT

    def __init__(self, master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')

################# Submit Button Code
        
        def submit():
            def validate_fields(entry_list):
                for entry in entry_list:
                    if not entry.get():
                        entry.config(bg='#0000FF')
                        self.after(1500, lambda: entry.config(bg="white")) # set back to default after 1.5 seconds
                        return False
                return True
                
            if validate_fields([seat_ent3,month_ent5,year_ent6,curr_ent4,empno_ent7,branch_ent8,off_code_ent10]):
                global caseno_ent1
                caseno1=str(caseno_ent1.get())      #case no  -Auto increment
                cat1=cat_ent2.get()
                if cat1=='ADMIN':
                    updatecat1=1
                elif cat1=='CONDUCTOR':    
                    updatecat1=4
                elif cat1=='DRIVER':
                    updatecat1=3
                elif cat1=='TECH':
                    updatecat1=2
 
             
                seat1=seat_ent3.get()   #seat no
                month1=int(month_ent5.get()) # month
                year1=year_ent6.get()   # year
                curr1=curr_ent4.get()   #current no
                empno1=empno_ent7.get() # empno 
                branch1=branch_ent8.get() #branch
                veh1=veh_ent9.get()       # vehicle
                off_code1=off_code_ent10.get() #off_code
                off_other1=off_other_ent22.get() #off_others
                
                off_dt1_str = off_dt1_ent11.get()  # get input string
                if off_dt1_str:  # check if input string is not empty
                    Toff_dt1 = datetime.strptime(off_dt1_str, '%d-%m-%Y')  # parse date
                    off_dt1 = Toff_dt1.strftime('%Y-%m-%d')  # convert date to desired format
                else:
                    off_dt1 = None  # set date to None or default value

                off_dt2_str=off_dt2_ent12.get()
                if off_dt2_str:
                    Toff_dt2=datetime.strptime(off_dt2_ent12.get(),'%d-%m-%Y')
                    off_dt2=Toff_dt2.strftime('%Y-%m-%d')     # off date2
                else:
                    off_dt2=None

                # memo_dt_str=memo_dt_ent21.get()
                # if memo_dt_str:
                #     memo_dt1 = (lambda x: datetime.strptime(x, '%d-%m-%Y').strftime('%Y-%m-%d'))(memo_dt_ent21.get())
                # else:
                #     memo_dt1=None
                    
                # sus_dt_str=sus_dt_ent13.get()
                # if sus_dt_str:
                #     sus_dt1= (lambda x: datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(sus_dt_ent13.get())
                # else:
                #     sus_dt1=None

                # dt_exp_str=dt_exp_ent14.get()
                # if dt_exp_str:
                #     dt_exp1=( lambda x: datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(dt_exp_ent14.get())
                # else:
                #     dt_exp1=None

                off_rem1=off_rem_ent15.get()   # off remarks
                add_rem1=add_rem_ent16.get()

                ref_no1=ref_no_ent17.get()     # ref_no
                ref_dt1_str=ref_dt_ent18.get()
                if ref_dt1_str:
                    ref_dt1= (lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ref_dt_ent18.get() )    # ref date
                else:
                    ref_dt1=None

                from1=from_ent19.get()  
                

                #Connection and Insert entry data        
                newentry=dbcon.connect1()
                newentry_cursor=newentry.cursor()
                with newentry.cursor() as cursor:
                    query ="INSERT INTO ALLDIS (SNO,CAT,SEAT_NO,CRTNO,MTH,YEA,STNO,BRANCH,VEHNO,OFFEN,OFF_OTHERS,DT_OFFEN,DT_OFFEN2,OFF_REM,OFF_REM2,REF_NO,REF_DATE,REF_FROM) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                    cursor.execute(query, (caseno1, updatecat1,seat1,curr1,month1,year1,empno1,branch1,veh1,off_code1,off_other1,off_dt1,off_dt2,off_rem1,add_rem1,ref_no1,ref_dt1,from1))
                newentry.commit()

                # After submission  Delete entries:
                caseno1=str(int(caseno1)+1)
                caseno_ent1.config(state='normal') 
                caseno_ent1.delete(0,tk.END)
                caseno_ent1.insert(0,caseno1)  
                caseno_ent1.config(state='readonly')   
                off_lbl21.config(text='Offence ')
                entry_widgets1=[child for child in self.winfo_children() if isinstance(child,(tk.Entry,tk.Text))]
                for entry in entry_widgets1:
                    if not isinstance(entry, DateEntry):
                        entry.delete(0,tk.END)
                cat_ent2.focus_set()
            else:
                tk.messagebox.showinfo("Info","Please check the entries")

#########################End of Submit Button Code
        global tcase1
        tcase1=dbcon.lastcaseno()
        off_data=dbcon.offcode()
        offence_values = [d["O_CODE"] for d in off_data]
        sorted_offence_values = sorted(offence_values, key=lambda x: x)
        # offence_remarks = [d["OFFENCE"] for d in off_data]

        def on_off_sele(event):
            selected_offence = off_code_ent10.get()
            for d in off_data:
                if d["O_CODE"] == selected_offence:
                    # offence_remarks = d["OFFENCE"]
                    offence_remarks=d["OFFENCE"][:25] + "..." if len(d["OFFENCE"]) > 25 else d["OFFENCE"]
                    off_lbl21.configure(text=offence_remarks)
                    break
        
        def clear_date(event):
            event.widget.delete(0,tk.END) 

        #Label and Entry Widgets
        caseno_lbl1=tk.Label(self,text="Case No :",bg='#C9C0BB')               
        cat_lbl2=tk.Label(self,text="Category :",bg='#C9C0BB')   
        seat_lbl3=tk.Label(self,text="Seat No :",bg='#C9C0BB')    
        curr_lbl4=tk.Label(self,text="Current No :",bg='#C9C0BB')    
        month_lbl5=tk.Label(self,text="Month :",bg='#C9C0BB')  
        year_lbl6=tk.Label(self,text="Year :",bg='#C9C0BB')      
        empno_lbl7=tk.Label(self,text="Emp No :",bg='#C9C0BB')       
        branch_lbl8=tk.Label(self,text="Branch :",bg='#C9C0BB')      
        veh_lbl9=tk.Label(self,text="Vehicle No :",bg='#C9C0BB')
        off_code_lbl10=tk.Label(self,text="Offence Code :",bg='#C9C0BB')
        off_dt1_lbl11=tk.Label(self,text="Offence Date1 :",bg='#C9C0BB')
        # memo_dt_lbl12=tk.Label(self,text="Memo Date :",bg='#C9C0BB')
        # sus_dt_lbl13=tk.Label(self,text="Suspension Date :",bg='#C9C0BB')
        # dt_exp_lbl14=tk.Label(self,text="Date of Exp :",bg='#C9C0BB')
        off_rem_lbl15=tk.Label(self,text="Offence Remarks :",bg='#C9C0BB')
        add_rem_lbl16=tk.Label(self,text="Additional Remarks :",bg='#C9C0BB')
        ref_no_lbl17=tk.Label(self,text="Ref. No :",bg='#C9C0BB')
        ref_dt_lbl18=tk.Label(self,text="Ref. Date :",bg='#C9C0BB')
        from_lbl19=tk.Label(self,text="From :",bg='#C9C0BB')
        off_dt2_lbl20=tk.Label(self,text="Offence Date 2 :",bg='#C9C0BB')
        off_lbl21=tk.Label(self,text='Offence',bg='#C9C0BB',width=25,anchor='w')
        empname_lbl=tk.Label(self,text='Kaleem')
        empname_lbl.gird(row=16,column=1,sticky='w')

        caseno_lbl1.grid(row=0, column=0, padx=3, pady=3, sticky="e")
        cat_lbl2.grid(row=1, column=0, padx=3, pady=3, sticky="e")
        seat_lbl3.grid(row=2, column=0, padx=3, pady=3, sticky="e")
        curr_lbl4.grid(row=3,column=0,padx=3,pady=3,sticky="e")
        month_lbl5.grid(row=4,column=0,padx=3,pady=3,sticky="e")
        year_lbl6.grid(row=5,column=0,padx=3,pady=3,sticky="e")
        empno_lbl7.grid(row=6,column=0,padx=3,pady=3,sticky="e")
        branch_lbl8.grid(row=7,column=0,padx=3,pady=3,sticky="e")
        veh_lbl9.grid(row=8,column=0,padx=3,pady=3,sticky="e")
        off_code_lbl10.grid(row=9,column=0,padx=3,pady=3,sticky="e")
        off_dt1_lbl11.grid(row=11,column=0,padx=3,pady=3,sticky="e")
        off_dt2_lbl20.grid(row=12,column=0,padx=3,pady=3,sticky="e")
        # memo_dt_lbl12.grid(row=13,column=0,padx=3,pady=3,sticky="e")
        
        # sus_dt_lbl13.grid(row=14,column=0,padx=3,pady=3,sticky="e")
        # dt_exp_lbl14.grid(row=15,column=0,padx=3,pady=3,sticky="e")
        off_rem_lbl15.grid(row=0,column=2,padx=3,pady=3,sticky="e")
        add_rem_lbl16.grid(row=1,column=2,padx=3,pady=3,sticky="e")
        ref_no_lbl17.grid(row=2,column=2,padx=3,pady=3,sticky="e")
        ref_dt_lbl18.grid(row=3,column=2,padx=3,pady=3,sticky="e")
        from_lbl19.grid(row=4,column=2,padx=3,pady=3,sticky="e")
        # of_dt2_lbl20.grid(row=5,column=2,padx=3,pady=3,sticky="nw")f
        off_lbl21.grid(row=9,column=2,padx=3,pady=3,sticky="e")
        # lbl22.grid(row=10,column=2,padx=3,pady=3,sticky="nw")
       

       
            
        global caseno_ent1
        caseno_ent1=tk.Entry(self,width=15,state='normal') 
        caseno_ent1.insert(0,tcase1)  
        caseno_ent1.config(state='readonly')                                      #cat no
        cat_list=["ADMIN","CONDUCTOR","DRIVER","TECH"]
        cat_ent2=ttk.Combobox(self,values=cat_list,width=15)
        cat_ent2.focus_set()
        seat_ent3=tk.Entry(self,width=15)                                         #seat no
        curr_ent4=tk.Entry(self,width=15)                                         # case no
        month_ent5=tk.Entry(self,width=15)                                        # category
        year_ent6=tk.Entry(self,width=15)                                         # Month
        empno_ent7=tk.Entry(self,width=15)                                        # year 
        branch_ent8=tk.Entry(self,width=15)                                       #emp No.
        veh_ent9=tk.Entry(self,width=15)                                          # Branch
        off_code_ent10=ttk.Combobox(self,values=sorted_offence_values,width=15)                                       #vehicle
        off_code_ent10.bind('<<ComboboxSelected>>',on_off_sele)
        off_other_ent22=tk.Entry(self,width=15)
        off_dt1_ent11=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')             # offcode
        # off_dt1_ent11=calen_class.NullableDateEntry(self,selectmode='day',date_pattern='dd-mm-y')             # offcode
        # off_dt1_ent11.delete(0,tk.END)
        

        
        off_dt2_ent12=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')       # off date1
        # off_dt2_ent12.bind("<Delete>",clear_date)
        # memo_dt_ent21=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')      # off date 2
        # sus_dt_ent13=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')       # memo date
        # dt_exp_ent14=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')       # sus date
        off_rem_ent15=tk.Entry(self,width=25)                                      # Date of Exp
        add_rem_ent16=tk.Entry(self,width=25)                                      # offe remar
        ref_no_ent17=tk.Entry(self,width=25)                                        # additional remarks
        ref_dt_ent18=DateEntry(self, selectmode='day', date_pattern='dd-mm-y')            # ref .no
        from_ent19=tk.Entry(self,width=25)                                           # ref .date   
        # ent20=tk.Entry(self,width=15)                                        # from


        caseno_ent1.grid(row=0,column=1,sticky="w",padx=3,pady=3)
        cat_ent2.grid(row=1,column=1,sticky="w",padx=3,pady=3)
        seat_ent3.grid(row=2,column=1,sticky="w",padx=3,pady=3)
        curr_ent4.grid(row=3,column=1,sticky="w",padx=3,pady=3)
        month_ent5.grid(row=4,column=1,sticky="w",padx=3,pady=3)
        year_ent6.grid(row=5,column=1,sticky="w",padx=3,pady=3)
        empno_ent7.grid(row=6,column=1,sticky="w",padx=3,pady=3)
       

        
        branch_ent8.grid(row=7,column=1,sticky="w",padx=3,pady=3)
        veh_ent9.grid(row=8,column=1,sticky="w",padx=3,pady=3)
        off_code_ent10.grid(row=9,column=1,sticky="w",padx=3,pady=3)
        off_other_ent22.grid(row=10,column=1,sticky="w",padx=3,pady=3)
        off_dt1_ent11.grid(row=11,column=1,sticky="w",padx=3,pady=3)
        off_dt2_ent12.grid(row=12,column=1,sticky="w",padx=3,pady=3)
        # memo_dt_ent21.grid(row=13,column=1,sticky="w",padx=3,pady=3)
        # sus_dt_ent13.grid(row=14,column=1,sticky="w",padx=3,pady=3)
        # dt_exp_ent14.grid(row=15,column=1,sticky="w",padx=3,pady=3)
        off_rem_ent15.grid(row=0,column=3,sticky="w",padx=3,pady=3)
        add_rem_ent16.grid(row=1,column=3,sticky="w",padx=3,pady=3)
        ref_no_ent17.grid(row=2,column=3,sticky="w",padx=3,pady=3)
        ref_dt_ent18.grid(row=3,column=3,sticky="w",padx=3,pady=3)
        from_ent19.grid(row=4,column=3,sticky="w",padx=3,pady=3)
        # ent20.grid(row=5,column=3,sticky="w",padx=3,pady=3)
       
        submit_btn=tk.Button(self, text='Submit',command=submit)
        submit_btn.grid(row=21,column=4)
        submit_btn.bind("<Return>",lambda e: submit())

 

        self.place(width=800,height=500, x=0,y=0)
        self.update_idletasks()

        for widget in self.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)


        def go_next_entry(event, entry_list, this_index):
            next_index = (this_index + 1) % len(entry_list)
            while next_index != this_index:
                if isinstance(entry_list[next_index], DateEntry) and not entry_list[next_index].get():
                    next_index = (next_index + 1) % len(entry_list)
                else:
                    entry_list[next_index].focus_set()
                    break
                if next_index == this_index:
                    event.widget.tk_focusNext().focus()
        
        def go_previous_entry(event,entry_list,this_index):
            previous_index=(this_index-1) % len(entry_list)
            while previous_index != this_index:
                if isinstance(entry_list[previous_index], DateEntry) and not entry_list[previous_index].get():
                    previous_index = (previous_index-1) % len(entry_list)
                else:
                    entry_list[previous_index].focus_set()   
                    break
                if previous_index == this_index:
                    break 


        def check_empty_date_entry(event, entry_list, this_index):
            if isinstance(event.widget, DateEntry) and not event.widget.get():
                event.widget.selection_clear()
            go_next_entry(event, entry_list, this_index+1)

        entries = [child for child in self.winfo_children() if isinstance(child, (tk.Label,tk.Entry,tk.Button))]
        
        for idx, entry in enumerate(entries):                                   # GPT
            entry.bind('<Return>', lambda e, idx=idx: go_next_entry(e, entries, idx))
            entry.bind('<Down>',lambda e, idx=idx: go_next_entry(e, entries, idx))
            entry.bind('<Up>',lambda e,idx=idx: go_previous_entry(e,entries,idx))
            entry.bind('<Delete>', clear_date)
  
