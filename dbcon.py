import pymysql
from tkinter import messagebox

#Establish Data base connection
def connect1():
    try:
        conn1=pymysql.connect(user='root',password='edp123',host='localhost',database='mydatabase')
        return conn1
    except Exception:
        messagebox.showerror("DatabaseError","Database connectivity error-Please contact EDP")

# Query Offmas into off_Code ComboxBox List
def offcode():
        mydboffcode=connect1()
        off_cursor=mydboffcode.cursor()
        off_cursor.execute(query='SELECT OFFEN,OFFENCE FROM OFFMAS')
        off_data = []
        for row in off_cursor:
            off_data.append({"O_CODE": row[0], "OFFENCE": row[1]})
        return off_data

def lastcaseno():
    global tcaseno
    mydbcase=connect1()
    caseno_cursor=mydbcase.cursor()
    # caseno_cursor.execute(query='SELECT LAST(SNO) FROM ALLDIS')
    caseno_cursor.execute(query='select sno from alldis ORDER BY id DESC LIMIT 1')

    record = caseno_cursor.fetchone()
    if record:
         tcaseno=int(record[0])
         tcaseno=tcaseno + 1
    else:
         tcaseno=1
    return tcaseno

# def corr_data():
#      mycorrdata=connect1()
#      corrdata_cursor=mycorrdata.cursor()
#      return c




