import tkinter as tk

from tkcalendar import DateEntry
class Entry1(tk.Frame):              # GPT

    def __init__(self, master=None):
        super().__init__(master,width=900,height=500,background='#C9C0BB')

        def submit():
            # print("pass")
            pass
        # self.off_seledate=tk.StringVar()
        
        #Label and Entry Widgets
        lbl1=tk.Label(self,text="Cat No :",bg='#C9C0BB')
        lbl2=tk.Label(self,text="Current :",bg='#C9C0BB')
        lbl12=tk.Label(self,text="Offence Date :",bg='#C9C0BB')
        
        lbl1.grid(row=0, column=0, padx=3, pady=3, sticky="e")
        lbl2.grid(row=1, column=0, padx=3, pady=3, sticky="e")
        lbl12.grid(row=2,column=0,padx=3,pady=3,sticky="e")
        
        ent1=tk.Entry(self,width=10)   #vcat no
        ent2=tk.Entry(self,width=10)   #current
        ent12=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')  # off date

        ent1.grid(row=0,column=1,sticky="w",padx=3,pady=3)
        ent2.grid(row=1,column=1,sticky="w",padx=3,pady=3)
        ent12.place(x=100, y=100, width=150, height=25)
       
        submit_btn=tk.Button(self, text='Submit',command=submit)
        submit_btn.grid(row=21,column=4)
        submit_btn.bind("<Return>",lambda e: submit())

        self.grid(row=0,column=1,padx=5,pady=5,sticky='nsew')
        self.grid_propagate(False)
       
        for widget in self.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)

        def go_next_entry(event, entry_list, this_index):                       # GPT
            next_index = (this_index + 1) % len(entry_list)
            entry_list[next_index].focus_set()
        
        def go_previous_entry(event, entry_list, this_index):                   # GPT
            previous_index = (this_index - 1) % len(entry_list)
            entry_list[previous_index].focus_set()
        entries = [child for child in self.winfo_children() if isinstance(child, (tk.Label,tk.Entry,tk.Button))]
        
        for idx, entry in enumerate(entries):                                   # GPT
            entry.bind('<Return>', lambda e, idx=idx: go_next_entry(e, entries, idx))
            entry.bind('<Down>',lambda e, idx=idx: go_next_entry(e, entries, idx))
            entry.bind('<Up>',lambda e,idx=idx: go_previous_entry(e,entries,idx))