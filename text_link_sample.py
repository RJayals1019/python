import tkinter as tk
from tkinter import ttk


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        # create custom style for hyperlink-like text
        style = ttk.Style()
        style.theme_use('default')  # add this line to fix the TclError
        style.map("Hyperlink.TLabel",
                  foreground=[("active", "blue"), ("!disabled", "red")],
                  cursor=[("active", "hand2")],
                  underline=[("active", "1")])

        # create the hyperlink-like text
        label = ttk.Label(self, text="1.Open toplevel frame", style="Hyperlink.TLabel")
        label.pack(padx=20, pady=20)

        # bind a function to be called when the hyperlink is clicked
        label.bind("<Button-1>", self.open_toplevel)

    def open_toplevel(self, event):
        # create the toplevel frame and disable the main window
        toplevel = tk.Toplevel(self)
        toplevel.grab_set()

        # create exit button to close the toplevel frame and enable the main window
        exit_button = ttk.Button(toplevel, text="Exit", command=toplevel.destroy)
        exit_button.pack(padx=20, pady=20)


app = App()
app.mainloop()
