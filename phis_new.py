import tkinter as tk
from tkinter import *
from datetime import datetime
from PIL import Image
import dbcon
import pymysql
from docxtpl import DocxTemplate
import os
import shutil
import subprocess
from tkinter import messagebox
from tkcalendar import DateEntry
import mysql.connector
# from tabulate import tabulate
from prettytable import PrettyTable
import os
import textwrap
from collections import Counter
import calendar


################################ Punishment History Print Details
class phis(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')
       
        ### Frame
        self.topframe=tk.LabelFrame(self,text='Print Punishment History',background='#C9C0BB')
        self.topframe.grid(row=0,column=0)


        def phis_print():
            global pnstno
            pnstno=ent_staff.get()
            print(pnstno)
            phis_conn = dbcon.connect1()
            
            
            ###-------------------------NPMAS DATA QUERY
            npmasquery_cur=phis_conn.cursor()
            npmasquery='SELECT * FROM NPMAS WHERE STNO=%s'
            pvalue=(pnstno,)
            npmasquery_cur.execute(npmasquery,pvalue)
            npmas_result=npmasquery_cur.fetchall()
            
            filename1=f"PH{pnstno}.txt"
            filepath = os.path.join("D:\\", filename1)
            
            if npmas_result:
                column_names=[column[0] for column in npmasquery_cur.description]
                # print(column_names)
                npmas_result_dict=[dict(zip(column_names,row)) for row in npmas_result]
                # print(npmas_result_dict)
                for record in npmas_result_dict:
                    # with open(filepath,"w") as file:
                        content_head='T.N.S.T.C (SALEM) LTD., DHARMAPURI REGION : : PUNISHMENT HISTORY \n'
                        content_head+='-'*96+"\n"
                        npmas_dob= datetime.strptime(str(record['DOB']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        npmas_doj= datetime.strptime(str(record['DOJ']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        npmas_retd= datetime.strptime(str(record['RTDATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        global dotreg
                        dotreg=record['DOCT']       ##### date of regular
                        content_head+=f'Name          :{record["NAME"]:<25} | Date of Birth      :{npmas_dob:<15}\n'
                        content_head+=f'Staff No.     :{record["STNO"]}-{record["OSTNO"]:<15} | Date of Join       :{npmas_doj:<15}\n'
                        content_head+=f"Father's Name :{record['FNAME']:<25} | Date of Regular    :{record['DOCT']:<15}\n"
                        content_head+=f'Designation   :{record["DESIGN"]:<26}| Date of Retirement :{npmas_retd:<15}\n'
                        content_head+=f'Branch        :{record["BRNAME"]:<26}|\n'

                        # file.write('T.N.S.T.C (SALEM) LTD., DHARMAPURI REGION : : PUNISHMENT HISTORY \n')
                        # file.write('-'*96+"\n")
                        # npmas_dob= datetime.strptime(str(record['DOB']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        # npmas_doj= datetime.strptime(str(record['DOJ']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        # npmas_retd= datetime.strptime(str(record['RTDATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                        # global dotreg
                        # dotreg=record['DOCT']       ##### date of regular
                        # file.write(f'Name          :{record["NAME"]:<25} | Date of Birth      :{npmas_dob:<15}\n')
                        # file.write(f'Staff No.     :{record["STNO"]}-{record["OSTNO"]:<15} | Date of Join       :{npmas_doj:<15}\n')
                        # file.write(f"Father's Name :{record['FNAME']:<25} | Date of Regular    :{record['DOCT']:<15}\n")
                        # file.write(f'Designation   :{record["DESIGN"]:<26}| Date of Retirement :{npmas_retd:<15}\n')
                        # file.write(f'Branch        :{record["BRNAME"]:<26}|\n')
                        # file.write(content_head)
                    # file.close()
                        # with open(filepath,"w") as file: 
                        #     file.write(content_head)
                        #     file.close()
            else:   
                tk.messagebox.showinfo("Info","No data")
                content_head=' Nil'
            ###-------------------------END OF NPMAS DATE QUERY

            ###---------------------- PALLDIS DATA QUERY                     
            phis_cur = phis_conn.cursor()
            pquery = 'SELECT * FROM palldis WHERE STNO = %s'
            pvalue = (pnstno,)
            phis_cur.execute(pquery, pvalue)
            result = phis_cur.fetchall()
            fomas_cur=phis_conn.cursor()
            fomas_query='SELECT * FROM FOMAS'
            fomas_cur.execute(fomas_query)
            fomas_result=fomas_cur.fetchall()
            if fomas_result:
                column_names=[column[0] for column in fomas_cur.description]
                # fomas_result_dict=[dict(zip(column_names,row)) for row in fomas_result]
                fomas_result_dict = {row[0]: row[1] for row in fomas_result}
                # print(fomas_result_dict)
            content1=' '
            if result:
                content1+=content_head
                column_names=[column[0] for column in phis_cur.description]
                # print(column_names)
                result_dict = [dict(zip(column_names, row)) for row in result]
                # print(result_dict)
                x=PrettyTable()
                x.field_names=['SN','OFF_DATE','VEH','OFFENCE','PUNISHMENT','FILE/YEAR','F.O.DATE']      ##PUNISHMENT','FILE/YEAR','F.O.DATE']
                x.align['OFFENCE']='l'
                x.align['PUNISHMENT']='l'
                max_width=15
                min_width=15
                line_count1=0
                row_count = 0
                page_limit = 25
                # with open(filepath, "w") as file:
                for j, record in enumerate(result_dict):
                    line_count1+=1
                    
                    if record['DATE'] and record['DATE']!= '':
                        date_fmt=  datetime.strptime(str(record['DATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                    else:
                        date_fmt='-'
                    ### TO PRINT THE PUNISHMNET CODE TO TEXT
                    forder_list=list(record['PUN'])
                    forder_output=[]
                    for i,char in enumerate(forder_list):
                        # If the current character is a digit, concatenate it with the previous digit(s)
                        if char.isdigit() and i>0 and forder_list[i-1].isdigit():
                            forder_output[-1]=forder_output[-1]+char
                        else:
                            # Get the corresponding meaning from the 'pun_dic' dictionary
                            if char in fomas_result_dict:
                                forder_output.append(fomas_result_dict[char])
                            else:
                                forder_output.append(char)
                    #join the all the strings
                    prt_punis=' '.join(forder_output)    
                    

                    # x.add_row([j+1,date_fmt,record['VEH'],record['OFF'],prt_punis])
                    # Set the OFFENCE attribute to a fixed width of 15 characters and enable word wrap

                    wrapped_offence = textwrap.fill(record['OFF'], width=20)
                    wrapped_punish = textwrap.fill(prt_punis,width=20)
                    wrapped_file_year=record['SNO']+'/'+str(record['YEA'])
                    if record['FO_DATE'] and record['FO_DATE']!= '':
                        date_fmt1=  datetime.strptime(str(record['FO_DATE']),'%Y-%m-%d').strftime('%d-%m-%Y')
                    else:
                        date_fmt1='-'
                    x.add_row([j+1,date_fmt,record['VEH'],wrapped_offence,wrapped_punish,wrapped_file_year,date_fmt1])
                    
                
                with open(filepath, "w") as file:
                    file.write(str(x))
                file.close()
            else:
                tk.messagebox.showinfo("Info","No data")
            ####------------------END OF PALLDIS DATA QUERY
            
            ####------------------QUERY ALLDIS DATA
            y=PrettyTable()
            y.field_names=['SN','OFF_DATE','VEH','OFFENCE','PUNISHMENT','FILE/YEAR','F.O.DATE'] 
            y.header = False
            y.align['OFFENCE']='l'
            y.align['PUNISHMENT']='l'
            max_width=15
            y.min_width['VEH']=4
            y.min_width['F.O.DATE']=10
            y.min_width['OFFENCE']=20
            y.min_width['PUNISHMENT']=18
            
            aldis_cur=phis_conn.cursor()
            aldisquery='SELECT * FROM alldis WHERE STNO=%s'
            pvalue=(pnstno,)
            aldis_cur.execute(aldisquery,pvalue)
            aldis_result=aldis_cur.fetchall()
         
            offmas_cur=phis_conn.cursor()
            offmasquery='SELECT OFFEN, OFFENCE FROM offmas'
            offmas_cur.execute(offmasquery)
            offmas_result=offmas_cur.fetchall()
            
            if offmas_result:
                offmas_offence=[]
                column_names=[column[0] for column in offmas_cur.description]
                offmas_result_dict={row[0]:row[1] for row in offmas_result}
                # print(offmas_result_dict)
                for row in offmas_result:
                    offmas_result_dict1 = dict(zip(column_names, row))
                    offmas_offence.append(offmas_result_dict1['OFFENCE'])
                    # offmas_offence.append(offmas_result_dict['OFFENCE'])
                # print(offmas_offence)

            if aldis_result:
                with open(filepath,"a") as file:
                    file.write('\n')
                    file.write('File in process..\n')
                file.close()
                column_names = [column[0] for column in aldis_cur.description]
                SN=0
                al_dis_offences_list=[]
                al_dis_offences_dict={}
                for record in aldis_result:
                    al_dis_record = dict(zip(column_names, record))
                    al_dis_offences_list.append(al_dis_record)
    
                for idx,record in enumerate(al_dis_offences_list):
                    # print(record['OFF_REM'])
                
                    if record['DT_OFFEN'] is not None:
                        date_fmt2=  datetime.strptime(str(record['DT_OFFEN']),'%Y-%m-%d').strftime('%d-%m-%Y')
                    else:
                        date_fmt2='-'
                    
                    off_code1=record['OFFEN']
                    if off_code1:
                        off_code1_dis=offmas_result_dict.get(off_code1)
                    else:
                        off_code1_dis=record['OFF_REM']
                    # print(off_code1_dis)
                    SN=+1
                    # wrapped_sn=textwrap.fill(str(SN),width=3)    
                    wrapped_offence2=textwrap.fill(off_code1_dis,width=20)
                    wrapped_punish2=textwrap.fill('**pending**',width=20)
                    wrapped_file_year1=record['SNO']+'/'+str(record['YEA'])
                    wrapped_vehno1 = textwrap.fill(record['VEHNO'], width=20)
                    wrapped_fodate1=textwrap.fill(' ',width=10)
                    y.add_row([idx+1,date_fmt2,wrapped_vehno1,wrapped_offence2,wrapped_punish2,wrapped_file_year1,wrapped_fodate1])
                    SN=+1
                        # print(y)    
                with open(filepath,"a") as file:
                        file.write(str(y))
                file.close()#
            
        # else:
            # FOR ABSTRACT
            offences =[record['OFF'] for record in result_dict]
            for record in al_dis_offences_list:
                if record['OFFEN']:
                    off_code2=[]
                    toff_code2=offmas_result_dict.get(off_code1)
                    off_code2.append(toff_code2)
                    offences.extend(off_code2)
                else:
                    off_code2=record['OFF_REM']
                    offences.extend(off_code2)
            offence_counts = Counter(offences)
            
            offence_list = []
            count_list = []
            for offence, count in offence_counts.items():
                offence_list.append(offence)
                count_list.append(count)

            with open(filepath,"a") as file:
                file.write('\nABSTRACT\n')
                file.write('--------\n')
                # Calculate the number of offenses to be printed in each column
                num_offences = len(offence_list)
                num_offences_col1 = (num_offences + 1) // 2
                num_offences_col2 = num_offences - num_offences_col1

                # Print offenses and counts in two columns
                for i in range(num_offences_col1):
                    file.write(f"{offence_list[i]:<33} - {count_list[i]}")
                    if i + num_offences_col1 < num_offences:
                            file.write(f"    {offence_list[i+num_offences_col1]:<33} - {count_list[i+num_offences_col1]}")
                    file.write("\n")
                   
            file.close()
            ####------------------END OF ALLDIS DAT
            
            ####------------------- START OF YTD DATA
            yt99_cur=phis_conn.cursor()
            yt99query='SELECT * FROM YTD9009 WHERE YSTNO=%s'
            pvalue=(pnstno,)
            yt99_cur.execute(yt99query,pvalue)
            yt99_result=yt99_cur.fetchall()
            # print(yt99_result)
            if yt99_result:
                yt99_list=[]
                yt99_dict={}
                ytcolumn=[column[0] for column in yt99_cur .description]
                for record in yt99_result:
                    yt99_dict= dict(zip(ytcolumn, record))
                    yt99_list.append(yt99_dict)
                # print(yt99_list)
                # print(dotreg)
                year_reg=dotreg[6:10]
                # print(year_reg)
            else:
                tk.messagebox.showinfo("No leave details" "No Leave details for this employee")

            #### ************
            ytd_abs_dict={'2006':'1','2007':'2','2008':'3','2009':'4','2010':'5','2011':'6','2012':'7','2013':'8','2014':'9','2015':'10','2016':'11',
              '2017':'12','2018':'13','2019':'14','2020':'15','2021':'16','2022':'17','2023':'18','2024':'19','2025':'20','2026':'21',
              '2027':'22','2028':'23','2029':'24','2030':'25','2031':'26','2032':'27','2033':'28','2034':'29',
              '2035':'30','2036':'31','2037':'32','2038':'33','2039':'34','2040':'35','2041':'36','2042':'37',
              '2043':'38','2044':'39','2045':'40','2046':'41','2047':'42','2048':'43','2049':'44','2050':'45'}

            # ytd=[{'id': 12947, 'YSTNO': 'S2D120529', 'OYSTNO': 'DR8013', 'DUTY1': 0, 'WO1': 0, 'LLP1': 0, 'ABS1': 0, 'SUS1': 0, 'LEV1': 0, 
            # 'DUTY2': 0, 'WO2': 0, 'LLP2': 0, 'ABS2': 0, 'SUS2': 0, 'LEV2': 0, 'DUTY3': 0, 'WO3': 0, 'LLP3': 0, 'ABS3': 0, 'SUS3': 0, 'LEV3': 0,
            # 'DUTY4': 0, 'WO4': 0, 'LLP4': 0, 'ABS4': 0, 'SUS4': 0, 'LEV4': 0, 'DUTY5': 0, 'WO5': 0, 'LLP5': 0, 'ABS5': 0, 'SUS5': 0, 'LEV5': 0, 
            # 'DUTY6': 0, 'WO6': 0, 'LLP6': 0, 'ABS6': 0, 'SUS6': 0, 'LEV6': 0, 'DUTY7': 0, 'WO7': 0, 'LLP7': 0, 'ABS7': 0, 'SUS7': 0, 'LEV7': 0, 
            # 'DUTY8': 0, 'WO8': 0, 'LLP8': 0, 'ABS8': 0, 'SUS8': 0, 'LEV8': 0, 'DUTY9': 0, 'WO9': 0, 'LLP9': 0, 'ABS9': 0, 'SUS9': 0, 'LEV9': 0,
            # 'DUTY10': 0, 'WO10': 0, 'LLP10': 0, 'ABS10': 0, 'SUS10': 0, 'LEV10': 0, 'DUTY11': 229, 'WO11': 34, 'LLP11': 6, 'ABS11': 47, 'SUS11': 0, 
            # 'LEV11': 38, 'DUTY12': 151, 'WO12': 16, 'LLP12': 0, 'ABS12': 168, 'SUS12': 0, 'LEV12': 23, 'DUTY13': 196, 'WO13': 16, 'LLP13': 0, 
            # 'ABS13': 70, 'SUS13': 7, 'LEV13': 60, 'DUTY14': 190, 'WO14': 14, 'LLP14': 5, 'ABS14': 72, 'SUS14': 0, 'LEV14': 53, 'DUTY15': 154, 
            # 'WO15': 11, 'LLP15': 2, 'ABS15': 135, 'SUS15': 0, 'LEV15': 47, 'DUTY16': 76, 'WO16': 133, 'LLP16': 67, 'ABS16': 31, 'SUS16': 0, 
            # 'LEV16': 54, 'DUTY17': 157, 'WO17': 65, 'COF17': 28, 'LLP17': 11, 'ABS17': 75, 'SUS17': 0, 'LEV17': 31, 'DUTY18': 108, 'WO18': 16, 
            # 'LLP18': 0, 'ABS18': 173, 'SUS18': 31, 'LEV18': 14, 'COF18': 23, 'DUTY19': 0, 'WO19': 0, 'LLP19': 0, 'ABS19': 0, 'SUS19': 0, 'LEV19': 0,
            # 'COF19': None, 'DUTY20': 0, 'WO20': 0, 'LLP20': 0, 'ABS20': 0, 'SUS20': 0, 'LEV20': 0, 'COF20': None, 'CLCB': 12, 'MLCB': 26, 'ELCB': 15}]

            z=PrettyTable()
            z.field_names=['YEAR','DUTY','LLP','WOF','ABS','SUS','LEV','COF','DUTY %','LEV %','ABS %'] 
            max_width=5
            min_width=5

            cur_year=datetime.now().year
            cur_month=datetime.now().month
            if cur_month <=3:
                abs_year=cur_year-2
            elif cur_month>3:
                abs_year=cur_year-1
            year_reg=2015
            ydays=366 if calendar.isleap(year_reg) else 365
            # print(ydays)
            # # print(cur_year)
            # # print(abs_year)
            # filename1=f"PH.txt"
            # filepath = os.path.join("D:\\", filename1)


            with open(filepath,"a") as file:
                file.write('                            LEAVE DETAILS \n')
                file.write('                            ------------- \n')
                file.write('                   C.L=12.0   E.L=14.5,  M.L=26.0\n\n')
                file.write('YEAR WISE DUTY DETAILS                               ELIGIBLE LEAVE 16.9 %\n')
            file.close()



            for yearwise in range(year_reg+1,abs_year):
                start_year=ytd_abs_dict[str(year_reg+1)]
                # print(''.join(['DUTY',start_year]))
                tempduty=''.join(['DUTY',start_year])
                tempwof=''.join(['WO',start_year])
                templlp=''.join(['LLP',start_year])
                tempabs=''.join(['ABS',start_year])
                tempsus=''.join(['SUS',start_year])
                templev=''.join(['LEV',start_year])
                
                if int(start_year)>=int(17):
                    tempcof=''.join(['COF',start_year])
                
                # print(tempduty)
                # YDUTYDAYS=ytd[tempduty]
                ydutydays= float(yt99_list[0][tempduty])
                ywof=float(yt99_list[0][tempwof])
                yllp=float(yt99_list[0][templlp])
                yabs=float(yt99_list[0][tempabs])
                ysus=float(yt99_list[0][tempsus])
                ylev=float(yt99_list[0][templev])
                if int(start_year)>=int(17):
                    float(ycof=yt99_list[0][tempcof])
                else:
                    ycof='-'    
                dut_per=round(float(ydutydays/ydays)*100,2)
                lev_per=round(float(ylev/ydays)*100,2)
                abs_per=round(float(yabs/ydays)*100,2)
                year_reg+=1
                # yearlbl1=abs_year
                # yearlbl2=start_year
                year_lbl=str(year_reg-1)+'-'+str(year_reg)
                w_year_lbl=textwrap.fill(year_lbl,width=9)
                w_ydutydays=textwrap.fill(str(ydutydays),width=5)
                w_ywof=textwrap.fill(str(ywof),width=5)
                w_yllp=textwrap.fill(str(yllp),width=5)
                w_yabs=textwrap.fill(str(yabs),width=5)
                w_ysus=textwrap.fill(str(ysus),width=5)
                w_ylev=textwrap.fill(str(ylev),width=5)
                w_ycof=textwrap.fill(str(ycof),width=5)
                
                w_dut_per=textwrap.fill(str(dut_per),width=5)
                w_lev_per=textwrap.fill(str(lev_per),width=5)
                w_abs_per=textwrap.fill(str(abs_per),width=5)
                z.add_row([w_year_lbl,w_ydutydays,w_ywof,w_yllp,w_yabs,w_ysus,w_ylev,w_ycof,w_dut_per,w_lev_per,w_abs_per])

            with open(filepath,"a") as file:
                file.write(str(z))
                file.write('\n')
                
            file.close()
            ######################################  CURRENT YEAR DUTY DETAILS
            with open(filepath,"a") as file:
                file.write(' CURRENT YEAR DUTY DETAILS\n')
                # file.write(str(z))
            file.close()
            za=PrettyTable()
            za.field_names=['YEAR','DUTY','LLP','ABS','SUS','LEV','COF'] 
            max_width=5
            min_width=5
            if cur_month <=3:
                abs_year=cur_year-2
            elif cur_month>3:
                abs_year=cur_year-1
            ytdays_mon={'4':'1','5':'2','6':'3','7':'4', '8':'5', '9':'6' ,'10':'7', '11':'8' ,'12':'9' ,'1':'10', '2':'11' ,'3':'12'}
            # pnstno='S2D120529'
            # print(pnstno)
            phis_conn = dbcon.connect1()
            ytmon_cur=phis_conn.cursor()
            ytmon_query='SELECT * FROM YTDAYS WHERE STNO=%s'
            pvalue=(pnstno,)
            ytmon_cur.execute(ytmon_query,pvalue)
            ytmon_result=ytmon_cur.fetchall()
            # print(ytmon_result)        
            ytmon_result_list=[]    
            if ytmon_result:
                column_names=[column[0] for column in ytmon_cur.description]
                # print(column_names)
                for row in ytmon_result:
                    # ytmon_result_dict=dict(zip(row,column_names))
                    ytmon_result_dict = {col_name: value for col_name, value in zip(column_names, row)}
                    ytmon_result_list.append(ytmon_result_dict)
            # print(ytmon_result_list)
                    i=1
                    for montwise in range(i,cur_month-1):
                        if i==1:
                            mon_name='APR'
                        elif i==2:    
                            mon_name='MAY'
                        elif i==3:    
                            mon_name='JUN'
                        elif i==4:    
                            mon_name='JUL'
                        elif i==5:    
                            mon_name='AUG'
                        elif i==6:    
                            mon_name='SEP'
                        elif i==7:    
                            mon_name='OCT'
                        elif i==8:    
                            mon_name='NOV'
                        elif i==9:    
                            mon_name='DEC'
                        elif i==10:    
                            mon_name='JAN'
                        elif i==11:    
                            mon_name='FEB'
                        elif i==12:    
                            mon_name='MAR'


                        # tmon_duty=''.join(['DUT',str(i)])
                        # mon_duty=if float(ytmon_result_list[0][tmon_duty]) not None:
                        # mon_duty = float(ytmon_result_list[0][tmon_duty]) if ytmon_result_list[0][tmon_duty] is not None else ' - '
                        mon_duty = float(ytmon_result_list[0][''.join(['DUT',str(i)])]) if ytmon_result_list[0][''.join(['DUT',str(i)])] is not None else ' - '
                        mon_llp = float(ytmon_result_list[0][''.join(['LLP',str(i)])]) if ytmon_result_list[0][''.join(['LLP',str(i)])] is not None else ' - '
                        mon_abs = float(ytmon_result_list[0][''.join(['ABS',str(i)])]) if ytmon_result_list[0][''.join(['ABS',str(i)])] is not None else ' - '
                        mon_sus = float(ytmon_result_list[0][''.join(['SUS',str(i)])]) if ytmon_result_list[0][''.join(['SUS',str(i)])] is not None else ' - '
                        mon_lev = float(ytmon_result_list[0][''.join(['LEV',str(i)])]) if ytmon_result_list[0][''.join(['LEV',str(i)])] is not None else ' - '
                        mon_cof = float(ytmon_result_list[0][''.join(['COF',str(i)])]) if ytmon_result_list[0][''.join(['COF',str(i)])] is not None else ' - '
                        
                        
                        w_mon_duty=textwrap.fill(str(mon_duty),width=4)
                        w_llp_duty=textwrap.fill(str(mon_llp),width=4)
                        w_abs_duty=textwrap.fill(str(mon_abs),width=4)
                        w_sus_duty=textwrap.fill(str(mon_sus),width=4)
                        w_lev_duty=textwrap.fill(str(mon_lev),width=4)
                        w_cof_duty=textwrap.fill(str(mon_cof),width=4)
                        za.add_row([mon_name,w_mon_duty,w_llp_duty,w_abs_duty,w_sus_duty,w_lev_duty,w_cof_duty])
                        # print(tmon_duty)
                        # print(str(mon_name),w_mon_duty,llp_duty,abs_duty,lev_duty,cof_duty)
                        i+=1
                    
                with open(filepath,"a") as file:
                    file.write(str(za))
                file.close()
                lbl_ph_info.config(text='File created at D:\\ and printing')
       
                # empname_lbl.config(text='....')
            ########## PRINT TO LPT1
            # printer_port = "LPT1"
            # command = f"COPY /B  {filepath} {printer_port}"

            # subprocess.run(command, shell=True)
            ########## END OF PRINT CODE
        


            ################################### END OF CURRENT YEAR DUYT DETAILS

                        #### ************


                        ####------------------- CLOSE OF YTD DATA

        def change_caps(event):
            ucaps=ent_staff.get()   #### cahnge to upper case
            ent_staff.delete(0,tk.END)
            ent_staff.insert(0,ucaps.upper())
    
        # WIDGETS
        lbl_staff=tk.Label(self.topframe,text='New Staff No :',background='#C9C0BB')
        lbl_staff.grid(row=0,column=0)
        ent_staff=tk.Entry(self.topframe,width=15)
        ent_staff.bind("<FocusOut>",change_caps)
        ent_staff.grid(row=0,column=1)
        bnt_submit=tk.Button(self.topframe,text='Submit',command=phis_print)
        bnt_submit.grid(row=0,column=2)
        
        lbl_ostaff=tk.Label(self.topframe,text='Old Staff No :',background='#C9C0BB')
        lbl_ostaff.grid(row=1,column=0)
        ent_ostaff=tk.Entry(self.topframe,width=15)
        ent_ostaff.grid(row=1,column=1)
        bnt_osubmit=tk.Button(self.topframe,text='Submit')
        bnt_osubmit.grid(row=1,column=2)
        lbl_ph_info=tk.Label(self.topframe,text=' ',background='#C9C0BB')
        lbl_ph_info.grid(row=2,column=0,columnspan=2)


        
        ############################ PADDING AND ALIGMENT
        for widget in self.topframe.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
            widget.update_idletasks()

        ########################### TO DISPLAY ALL WIDGETS
        self.place(width=800,height=500, x=0,y=0)
        self.grid_propagate(False)
        self.update_idletasks()



if __name__ == "__main__":
     root = tk.Tk()
     phis_show = phis(root)
     phis_show.mainloop()

