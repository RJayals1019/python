import tkinter as tk
from tkinter import filedialog
import dbfread
import pymysql

def browse_dbase4_table():
    file_path = filedialog.askopenfilename(filetypes=[("dBase IV Files", "*.dbf")])
    if file_path:
        update_mysql_table(file_path)

def update_mysql_table(file_path):
    # Connect to MySQL database
    db_connection = pymysql.connect(
        host="local_host",
        user="root",
        password="password",
        database="mydatabase"
    )
    cursor = db_connection.cursor()

    # Delete all records from MySQL table
    delete_query = "DELETE FROM your_table"
    cursor.execute(delete_query)
    db_connection.commit()

    # Read records from dBase IV table
    table = dbfread.DBF(file_path)

    # Get the field names from the dBase IV table
    field_names = table.field_names

    # Generate the placeholder string for the insert query
    placeholders = ', '.join(['%s'] * len(field_names))

    # Insert records into MySQL table
    insert_query = f"INSERT INTO your_table ({', '.join(field_names)}) VALUES ({placeholders})"
    for record in table:
        values = tuple(record.values())
        cursor.execute(insert_query, values)

    db_connection.commit()
    db_connection.close()

# Create a Tkinter window
window = tk.Tk()

# Add a button to browse the dBase IV table
browse_button = tk.Button(window, text="Browse", command=browse_dbase4_table)
browse_button.pack()

# Start the Tkinter event loop
window.mainloop()
