import tkinter as tk
import dbcon
import datetime
from dateutil.relativedelta import relativedelta
from datetime import datetime
import os

def mon_abs_results(row,row1,startdate,enddate):
    mon_abs_con1=dbcon.connect1()
    mon_abs_cur1=mon_abs_con1.cursor()
    query="SELECT count(cat) FROM ALLDIS where cat =%s and seat_no =%s and DT_OFFEN between %s and %s"
    mon_abs_cur1.execute(query,(row,row1,startdate,enddate))
    mon_abs_result2=mon_abs_cur1.fetchall()
    return mon_abs_result2[0]
def mon_abs_results1(row,row1,startdate):
    mon_abs_con1=dbcon.connect1()
    mon_abs_cur1=mon_abs_con1.cursor()
    query="SELECT count(cat) FROM ALLDIS where cat =%s and seat_no =%s and DT_OFFEN < %s"
    mon_abs_cur1.execute(query,(row,row1,startdate))
    mon_abs_result2=mon_abs_cur1.fetchall()
    return mon_abs_result2[0]
def mon_abs_report():
    line1=''
    Tmp_CatName =""
    tot3mon = 0
    tot6mon = 0
    tot9mon = 0
    tot1mon = 0
    tot2mon = 0
    tot1mor = 0
    cumtotal = 0
    CatName = ""
    Start3Mon = datetime.today()
    End3Mon = Start3Mon - relativedelta(months = 3)
    Start6Mon = End3Mon - relativedelta(days=1)
    End6Mon = Start6Mon - relativedelta(months=3)
    Start9Mon = End6Mon - relativedelta(days=1)
    End9Mon = Start9Mon - relativedelta(months = 3)
    Start12Mon = End9Mon - relativedelta(days=1)
    End12Mon = Start12Mon - relativedelta(months = 3)
    Start1Year = End12Mon - relativedelta(days=1)
    End1Year = Start1Year - relativedelta(months = 12)
    Start2Year = End1Year - relativedelta(days=1)
    mon_abs_con=dbcon.connect1()
    mon_abs_cur=mon_abs_con.cursor()
    if os.path.exists("abs.txt"):
         os.remove("abs.txt")
    file1 = open("abs.txt","a")
    file1.write('%-15s %-55s\n' %("","TamilNadu State Tansport Coporation(Salem) Ltd.,salem-7"))
    file1.write('%-15s %-55s\n' %("","-------------------------------------------------------"))
    file1.write('%10s %-65s %-8s\n' %("","CATEGOTY - WISE & SEAT - WISE PENDING DISCIPLINARY CASES AS ON - ",Start3Mon.strftime('%b')+"-"+Start3Mon.strftime('%Y')))
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")
    file1.write(":")    
    file1.write("\n")
    file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|","Category","|","SEAT","|","0 - 3","|","4 - 6","|","7 - 9","|"," 1 ","|","1 - 2","|"," more 2","|"," TOTAL","|"))
    file1.write("\n")
    file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|","","|","No","|","Months","|","Months","|","Months","|"," Years ","|","Years","|"," Years","|"," ","|"))
    file1.write("\n")
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")
    file1.write(":")   
   
    query1="SELECT distinct cat FROM ALLDIS order by cat"
    mon_abs_cur.execute(query1)
    mon_abs_result=mon_abs_cur.fetchall()
    for row in mon_abs_result:
        print(row[0])
        if row[0] == 1:
            Tmp_CatName = "ADMIN"
        if row[0] == 2:
            Tmp_CatName = "TECH" 
        if row[0] == 3:
            Tmp_CatName = "DRIVER"  
        if row[0] == 4:
            Tmp_CatName = "CONDUCTOR"      
        print(Tmp_CatName)      
        query2 = "select distinct SEAT_NO from alldis where cat= %s order by SEAT_NO"
        mon_abs_cur.execute(query2,row)
        mon_abs_result1 = mon_abs_cur.fetchall()
        for row1  in mon_abs_result1:
            mon3reports = mon_abs_results(row[0],row1[0],End3Mon,Start3Mon)
            tot3mon +=  mon3reports[0]
            mon6reports = mon_abs_results(row[0],row1[0],End6Mon,Start6Mon)
            tot6mon +=  mon6reports[0]
            mon9reports = mon_abs_results(row[0],row1[0],End9Mon,Start9Mon)
            tot9mon += mon9reports[0]
            mon1reports = mon_abs_results(row[0],row1[0],End12Mon,Start12Mon)
            tot1mon +=  mon1reports[0]
            mon2reports = mon_abs_results(row[0],row1[0],End1Year,Start1Year)
            tot2mon += mon2reports[0]
            mor1reports = mon_abs_results1(row[0],row1[0],Start2Year)
            tot1mor += mor1reports[0]
            total = mon3reports[0] + mon6reports[0] + mon9reports[0] + mon1reports[0] + mon2reports[0] + mor1reports[0]
            file1.write("\n")
          #  print(str(mon3reports)+" "+str(mon6reports)+" "+str(mon9reports)+""+str(mon1reports)+""+str(mon2reports)+""+str(mor1reports))
            if Tmp_CatName != CatName :
                if len(CatName.lstrip()) > 0 :
                    values = range(92)
                    file1.write(":")   
                    for i in values:
                        file1.write("-")
                    file1.write(":")      
                    file1.write("\n") 
                    file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|",Tmp_CatName,"|",row1[0],"|",str(mon3reports[0]),"|",str(mon6reports[0]),"|",str(mon9reports[0]),"|",str(mon1reports[0]),"|",str(mon2reports[0]),"|",str(mor1reports[0]),"|",total,"|"))
                else: 
                    file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|",Tmp_CatName,"|",row1[0],"|",str(mon3reports[0]),"|",str(mon6reports[0]),"|",str(mon9reports[0]),"|",str(mon1reports[0]),"|",str(mon2reports[0]),"|",str(mor1reports[0]),"|",total,"|"))   
                CatName = Tmp_CatName
            else:
                file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|","","|",row1[0],"|",str(mon3reports[0]),"|",str(mon6reports[0]),"|",str(mon9reports[0]),"|",str(mon1reports[0]),"|",str(mon2reports[0]),"|",str(mor1reports[0]),"|",total,"|"))     
    file1.write("\n")
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")  
    file1.write(":")    
    file1.write("\n")  
    cumtotal =  tot3mon + tot6mon + tot9mon + tot1mon + tot2mon + tot1mor 
    file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|"," TOTAL ","|","","|",tot3mon,"|",tot6mon,"|",tot9mon,"|",tot1mon,"|",tot2mon,"|",tot1mor,"|",cumtotal,"|"))                  
    file1.write("\n")
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")
    file1.write(":")      
    file1.close 

mon_abs_report()