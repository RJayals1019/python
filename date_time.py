from datetime import datetime
import os
import calendar
import tkinter
import textwrap
from prettytable import PrettyTable
import dbcon

ytd_abs_dict={'2006':'1','2007':'2','2008':'3','2009':'4','2010':'5','2011':'6','2012':'7','2013':'8','2014':'9','2015':'10','2016':'11',
              '2017':'12','2018':'13','2019':'14','2020':'15','2021':'16','2022':'17','2023':'18','2024':'19','2025':'20','2026':'21',
              '2027':'22','2028':'23','2029':'24','2030':'25','2031':'26','2032':'27','2033':'28','2034':'29',
              '2035':'30','2036':'31','2037':'32','2038':'33','2039':'34','2040':'35','2041':'36','2042':'37',
              '2043':'38','2044':'39','2045':'40','2046':'41','2047':'42','2048':'43','2049':'44','2050':'45'}

ytd=[{'id': 12947, 'YSTNO': 'S2D120529', 'OYSTNO': 'DR8013', 'DUTY1': 0, 'WO1': 0, 'LLP1': 0, 'ABS1': 0, 'SUS1': 0, 'LEV1': 0, 
  'DUTY2': 0, 'WO2': 0, 'LLP2': 0, 'ABS2': 0, 'SUS2': 0, 'LEV2': 0, 'DUTY3': 0, 'WO3': 0, 'LLP3': 0, 'ABS3': 0, 'SUS3': 0, 'LEV3': 0,
  'DUTY4': 0, 'WO4': 0, 'LLP4': 0, 'ABS4': 0, 'SUS4': 0, 'LEV4': 0, 'DUTY5': 0, 'WO5': 0, 'LLP5': 0, 'ABS5': 0, 'SUS5': 0, 'LEV5': 0, 
  'DUTY6': 0, 'WO6': 0, 'LLP6': 0, 'ABS6': 0, 'SUS6': 0, 'LEV6': 0, 'DUTY7': 0, 'WO7': 0, 'LLP7': 0, 'ABS7': 0, 'SUS7': 0, 'LEV7': 0, 
  'DUTY8': 0, 'WO8': 0, 'LLP8': 0, 'ABS8': 0, 'SUS8': 0, 'LEV8': 0, 'DUTY9': 0, 'WO9': 0, 'LLP9': 0, 'ABS9': 0, 'SUS9': 0, 'LEV9': 0,
 'DUTY10': 0, 'WO10': 0, 'LLP10': 0, 'ABS10': 0, 'SUS10': 0, 'LEV10': 0, 'DUTY11': 229, 'WO11': 34, 'LLP11': 6, 'ABS11': 47, 'SUS11': 0, 
 'LEV11': 38, 'DUTY12': 151, 'WO12': 16, 'LLP12': 0, 'ABS12': 168, 'SUS12': 0, 'LEV12': 23, 'DUTY13': 196, 'WO13': 16, 'LLP13': 0, 
 'ABS13': 70, 'SUS13': 7, 'LEV13': 60, 'DUTY14': 190, 'WO14': 14, 'LLP14': 5, 'ABS14': 72, 'SUS14': 0, 'LEV14': 53, 'DUTY15': 154, 
 'WO15': 11, 'LLP15': 2, 'ABS15': 135, 'SUS15': 0, 'LEV15': 47, 'DUTY16': 76, 'WO16': 133, 'LLP16': 67, 'ABS16': 31, 'SUS16': 0, 
 'LEV16': 54, 'DUTY17': 157, 'WO17': 65, 'COF17': 28, 'LLP17': 11, 'ABS17': 75, 'SUS17': 0, 'LEV17': 31, 'DUTY18': 108, 'WO18': 16, 
 'LLP18': 0, 'ABS18': 173, 'SUS18': 31, 'LEV18': 14, 'COF18': 23, 'DUTY19': 0, 'WO19': 0, 'LLP19': 0, 'ABS19': 0, 'SUS19': 0, 'LEV19': 0,
 'COF19': None, 'DUTY20': 0, 'WO20': 0, 'LLP20': 0, 'ABS20': 0, 'SUS20': 0, 'LEV20': 0, 'COF20': None, 'CLCB': 12, 'MLCB': 26, 'ELCB': 15}]

z=PrettyTable()
z.field_names=['YEAR','DUTY','LLP','WOF','ABS','SUS','LEV','COF','DUTY %','LEV %','ABS %'] 
max_width=5
min_width=5

cur_year=datetime.now().year
cur_month=datetime.now().month
if cur_month <=3:
    abs_year=cur_year-2
elif cur_month>3:
    abs_year=cur_year-1
year_reg=2015
ydays=366 if calendar.isleap(year_reg) else 365
# print(ydays)
# # print(cur_year)
# # print(abs_year)
filename1=f"PH.txt"
filepath = os.path.join("D:\\", filename1)


with open(filepath,"w") as file:
    file.write('                            LEAVE DETAILS \n')
    file.write('                            ------------- \n')
    file.write('                   C.L=12.0   E.L=14.5,  M.L=26.0\n\n')
    file.write('YEAR WISE DUTY DETAILS                               ELIGIBLE LEAVE 16.9 %\n')
file.close()



for yearwise in range(year_reg+1,abs_year):
    start_year=ytd_abs_dict[str(year_reg+1)]
    # print(''.join(['DUTY',start_year]))
    tempduty=''.join(['DUTY',start_year])
    tempwof=''.join(['WO',start_year])
    templlp=''.join(['LLP',start_year])
    tempabs=''.join(['ABS',start_year])
    tempsus=''.join(['SUS',start_year])
    templev=''.join(['LEV',start_year])
    
    if int(start_year)>=int(17):
        tempcof=''.join(['COF',start_year])
    
    # print(tempduty)
    # YDUTYDAYS=ytd[tempduty]
    ydutydays= float(ytd[0][tempduty])
    ywof=float(ytd[0][tempwof])
    yllp=float(ytd[0][templlp])
    yabs=float(ytd[0][tempabs])
    ysus=float(ytd[0][tempsus])
    ylev=float(ytd[0][templev])
    if int(start_year)>=int(17):
        float(ycof=ytd[0][tempcof])
    else:
        ycof='-'    
    dut_per=round(float(ydutydays/ydays)*100,2)
    lev_per=round(float(ylev/ydays)*100,2)
    abs_per=round(float(yabs/ydays)*100,2)
    year_reg+=1
    # yearlbl1=abs_year
    # yearlbl2=start_year
    year_lbl=str(year_reg-1)+'-'+str(year_reg)
    w_year_lbl=textwrap.fill(year_lbl,width=9)
    w_ydutydays=textwrap.fill(str(ydutydays),width=5)
    w_ywof=textwrap.fill(str(ywof),width=5)
    w_yllp=textwrap.fill(str(yllp),width=5)
    w_yabs=textwrap.fill(str(yabs),width=5)
    w_ysus=textwrap.fill(str(ysus),width=5)
    w_ylev=textwrap.fill(str(ylev),width=5)
    w_ycof=textwrap.fill(str(ycof),width=5)
    
    w_dut_per=textwrap.fill(str(dut_per),width=5)
    w_lev_per=textwrap.fill(str(lev_per),width=5)
    w_abs_per=textwrap.fill(str(abs_per),width=5)
    z.add_row([w_year_lbl,w_ydutydays,w_ywof,w_yllp,w_yabs,w_ysus,w_ylev,w_ycof,w_dut_per,w_lev_per,w_abs_per])

with open(filepath,"a") as file:
    file.write(str(z))
    file.write('\n')
    
file.close()
######################################  CURRENT YEAR DUTY DETAILS
with open(filepath,"a") as file:
    file.write(' CURRENT YEAR DUTY DETAILS\n')
    # file.write(str(z))
file.close()
za=PrettyTable()
za.field_names=['YEAR','DUTY','LLP','ABS','SUS','LEV','COF'] 
max_width=5
min_width=5
if cur_month <=3:
    abs_year=cur_year-2
elif cur_month>3:
    abs_year=cur_year-1
ytdays_mon={'4':'1','5':'2','6':'3','7':'4', '8':'5', '9':'6' ,'10':'7', '11':'8' ,'12':'9' ,'1':'10', '2':'11' ,'3':'12'}
pnstno='S2D120529'
print(pnstno)
phis_conn = dbcon.connect1()
ytmon_cur=phis_conn.cursor()
ytmon_query='SELECT * FROM YTDAYS WHERE STNO=%s'
pvalue=(pnstno,)
ytmon_cur.execute(ytmon_query,pvalue)
ytmon_result=ytmon_cur.fetchall()
# print(ytmon_result)        
ytmon_result_list=[]    
if ytmon_result:
    column_names=[column[0] for column in ytmon_cur.description]
    # print(column_names)
    for row in ytmon_result:
        # ytmon_result_dict=dict(zip(row,column_names))
        ytmon_result_dict = {col_name: value for col_name, value in zip(column_names, row)}
        ytmon_result_list.append(ytmon_result_dict)
# print(ytmon_result_list)
        i=1
        for montwise in range(i,cur_month-1):
            if i==1:
                mon_name='APR'
            elif i==2:    
                mon_name='MAY'
            elif i==3:    
                mon_name='JUN'
            elif i==4:    
                mon_name='JUL'
            elif i==5:    
                mon_name='AUG'
            elif i==6:    
                mon_name='SEP'
            elif i==7:    
                mon_name='OCT'
            elif i==8:    
                mon_name='NOV'
            elif i==9:    
                mon_name='DEC'
            elif i==10:    
                mon_name='JAN'
            elif i==11:    
                mon_name='FEB'
            elif i==12:    
                mon_name='MAR'


            # tmon_duty=''.join(['DUT',str(i)])
            # mon_duty=if float(ytmon_result_list[0][tmon_duty]) not None:
            # mon_duty = float(ytmon_result_list[0][tmon_duty]) if ytmon_result_list[0][tmon_duty] is not None else ' - '
            mon_duty = float(ytmon_result_list[0][''.join(['DUT',str(i)])]) if ytmon_result_list[0][''.join(['DUT',str(i)])] is not None else ' - '
            mon_llp = float(ytmon_result_list[0][''.join(['LLP',str(i)])]) if ytmon_result_list[0][''.join(['LLP',str(i)])] is not None else ' - '
            mon_abs = float(ytmon_result_list[0][''.join(['ABS',str(i)])]) if ytmon_result_list[0][''.join(['ABS',str(i)])] is not None else ' - '
            mon_sus = float(ytmon_result_list[0][''.join(['SUS',str(i)])]) if ytmon_result_list[0][''.join(['SUS',str(i)])] is not None else ' - '
            mon_lev = float(ytmon_result_list[0][''.join(['LEV',str(i)])]) if ytmon_result_list[0][''.join(['LEV',str(i)])] is not None else ' - '
            mon_cof = float(ytmon_result_list[0][''.join(['COF',str(i)])]) if ytmon_result_list[0][''.join(['COF',str(i)])] is not None else ' - '
            
            
            w_mon_duty=textwrap.fill(str(mon_duty),width=4)
            w_llp_duty=textwrap.fill(str(mon_llp),width=4)
            w_abs_duty=textwrap.fill(str(mon_abs),width=4)
            w_sus_duty=textwrap.fill(str(mon_sus),width=4)
            w_lev_duty=textwrap.fill(str(mon_lev),width=4)
            w_cof_duty=textwrap.fill(str(mon_cof),width=4)
            za.add_row([mon_name,w_mon_duty,w_llp_duty,w_abs_duty,w_sus_duty,w_lev_duty,w_cof_duty])
            # print(tmon_duty)
            # print(str(mon_name),w_mon_duty,llp_duty,abs_duty,lev_duty,cof_duty)
            i+=1
           
    with open(filepath,"a") as file:
        file.write(str(za))
    file.close()





################################### END OF CURRENT YEAR DUYT DETAILS
   

