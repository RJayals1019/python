import tkinter as tk

from tkcalendar import DateEntry
class Entry1(tk.Frame):              # GPT

    def __init__(self, master=None):
        super().__init__(master,width=900,height=500,background='#C9C0BB')

        def submit():
            # print("pass")
            pass
        # self.off_seledate=tk.StringVar()
        
        #Label and Entry Widgets
        lbl1=tk.Label(self,text="Cat No :",bg='#C9C0BB')
        lbl2=tk.Label(self,text="Current :",bg='#C9C0BB')
        lbl3=tk.Label(self,text="Seat No :",bg='#C9C0BB')
        lbl4=tk.Label(self,text="Case No :",bg='#C9C0BB')
        lbl5=tk.Label(self,text="Category :",bg='#C9C0BB')
        lbl6=tk.Label(self,text="Month :",bg='#C9C0BB')
        lbl7=tk.Label(self,text="Year :",bg='#C9C0BB')
        lbl8=tk.Label(self,text="Emp.No :",bg='#C9C0BB')
        lbl9=tk.Label(self,text="Branch :",bg='#C9C0BB')
        lbl10=tk.Label(self,text="vehicle No :",bg='#C9C0BB')
        lbl11=tk.Label(self,text="Offence Code :",bg='#C9C0BB')
        lbl12=tk.Label(self,text="Offence Date :",bg='#C9C0BB')
        lbl13=tk.Label(self,text="Memo Date :",bg='#C9C0BB')
        lbl14=tk.Label(self,text="Suspension Date :",bg='#C9C0BB')
        lbl15=tk.Label(self,text="Date of Exp :",bg='#C9C0BB')
        lbl16=tk.Label(self,text="Offence Remarks :",bg='#C9C0BB')
        lbl17=tk.Label(self,text="Additional Remarks :",bg='#C9C0BB')
        lbl18=tk.Label(self,text="Ref. No :",bg='#C9C0BB')
        lbl19=tk.Label(self,text="Ref. Date :",bg='#C9C0BB')
        lbl20=tk.Label(self,text="From :",bg='#C9C0BB')


        lbl1.grid(row=0, column=0, padx=3, pady=3, sticky="e")
        lbl2.grid(row=1, column=0, padx=3, pady=3, sticky="e")
        lbl3.grid(row=2, column=0, padx=3, pady=3, sticky="e")
        lbl4.grid(row=3,column=0,padx=3,pady=3,sticky="e")
        lbl5.grid(row=4,column=0,padx=3,pady=3,sticky="e")
        lbl6.grid(row=5,column=0,padx=3,pady=3,sticky="e")
        lbl7.grid(row=6,column=0,padx=3,pady=3,sticky="e")
        lbl8.grid(row=7,column=0,padx=3,pady=3,sticky="e")
        lbl9.grid(row=8,column=0,padx=3,pady=3,sticky="e")
        lbl10.grid(row=9,column=0,padx=3,pady=3,sticky="e")
        lbl11.grid(row=10,column=0,padx=3,pady=3,sticky="e")
        lbl12.grid(row=11,column=0,padx=3,pady=3,sticky="e")
        lbl13.grid(row=12,column=0,padx=3,pady=3,sticky="e")
        lbl14.grid(row=13,column=0,padx=3,pady=3,sticky="e")
        lbl15.grid(row=0,column=2,padx=3,pady=3,sticky="e")
        lbl16.grid(row=1,column=2,padx=3,pady=3,sticky="e")
        lbl17.grid(row=2,column=2,padx=3,pady=3,sticky="e")
        lbl18.grid(row=3,column=2,padx=3,pady=3,sticky="e")
        lbl19.grid(row=4,column=2,padx=3,pady=3,sticky="e")
        lbl20.grid(row=5,column=2,padx=3,pady=3,sticky="e")

        
        ent1=tk.Entry(self,width=10)   #vcat no
        ent2=tk.Entry(self,width=10)   #current
        ent3=tk.Entry(self,width=10)   #seat no
        ent4=tk.Entry(self,width=10)   # case no
        ent5=tk.Entry(self,width=10)   # category
        ent6=tk.Entry(self,width=10)   # Month
        ent7=tk.Entry(self,width=10)   # year 
        ent8=tk.Entry(self,width=10)   #emp No.
        ent9=tk.Entry(self,width=10)   # Branch
        ent10=tk.Entry(self,width=10)  #vehicle
        ent11=tk.Entry(self,width=10)  # offcode
        # ent12=DateEntry(self,selectmode='day',date_pattern='dd-mm-y')  # off date
        ent13=tk.Entry(self,width=10)   # memo date
        ent14=tk.Entry(self,width=10)  # sus date
        # ent15=tk.Entry(self,width=10)  # date of exp
        ent15 = DateEntry(self, selectmode='day', date_pattern='dd-mm-y')
        ent16=tk.Entry(self,width=15)  # offe remar
        ent17=tk.Entry(self,width=15)  # additional remarks
        ent18=tk.Entry(self,width=15)  # ref .no
        ent19=tk.Entry(self,width=10)  # ref .date   
        ent20=tk.Entry(self,width=15)  # from


        ent1.grid(row=0,column=1,sticky="w",padx=3,pady=3)
        ent2.grid(row=1,column=1,sticky="w",padx=3,pady=3)
        ent3.grid(row=2,column=1,sticky="w",padx=3,pady=3)
        ent4.grid(row=3,column=1,sticky="w",padx=3,pady=3)
        ent5.grid(row=4,column=1,sticky="w",padx=3,pady=3)
        ent6.grid(row=5,column=1,sticky="w",padx=3,pady=3)
        ent7.grid(row=6,column=1,sticky="w",padx=3,pady=3)
        ent8.grid(row=7,column=1,sticky="w",padx=3,pady=3)
        ent9.grid(row=8,column=1,sticky="w",padx=3,pady=3)
        ent10.grid(row=9,column=1,sticky="w",padx=3,pady=3)
        ent11.grid(row=10,column=1,sticky="w",padx=3,pady=3)
        # ent12.grid(row=11,column=1,sticky="w",padx=3,pady=3)
        # ent12.place(x=250,y=250)
        ent13.grid(row=12,column=1,sticky="w",padx=3,pady=3)
        ent14.grid(row=13,column=1,sticky="w",padx=3,pady=3)
        ent15.grid(row=0,column=3,sticky="w",padx=3,pady=3)
        ent16.grid(row=1,column=3,sticky="w",padx=3,pady=3)
        ent17.grid(row=2,column=3,sticky="w",padx=3,pady=3)
        ent18.grid(row=3,column=3,sticky="w",padx=3,pady=3)
        ent19.grid(row=4,column=3,sticky="w",padx=3,pady=3)
        ent20.grid(row=5,column=3,sticky="w",padx=3,pady=3)
       
        submit_btn=tk.Button(self, text='Submit',command=submit)
        submit_btn.grid(row=21,column=4)
        submit_btn.bind("<Return>",lambda e: submit())

        self.grid(row=0,column=1,padx=5,pady=5,sticky='nsew')
        self.grid_propagate(False)
       
        for widget in self.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)

        def go_next_entry(event, entry_list, this_index):                       # GPT
            next_index = (this_index + 1) % len(entry_list)
            entry_list[next_index].focus_set()
        
        def go_previous_entry(event, entry_list, this_index):                   # GPT
            previous_index = (this_index - 1) % len(entry_list)
            entry_list[previous_index].focus_set()
        entries = [child for child in self.winfo_children() if isinstance(child, (tk.Label,tk.Entry,tk.Button))]
        
        for idx, entry in enumerate(entries):                                   # GPT
            entry.bind('<Return>', lambda e, idx=idx: go_next_entry(e, entries, idx))
            entry.bind('<Down>',lambda e, idx=idx: go_next_entry(e, entries, idx))
            entry.bind('<Up>',lambda e,idx=idx: go_previous_entry(e,entries,idx))