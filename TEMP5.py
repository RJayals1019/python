from datetime import datetime
import dbcon
import textwrap
from collections import Counter

pnstno='S2M900047'

import os
from prettytable import PrettyTable
# global pnstno
# pnstno=ent_staff.get()
print(pnstno)
phis_conn = dbcon.connect1()


            
filename1=f"PH{pnstno}.txt"
filepath = os.path.join("D:\\", filename1)
y=PrettyTable()
y.field_names=['SN','OFF_DATE','VEH','OFFENCE','PUNISHMENT','FILE/YEAR','F.O.DATE'] 
y.header = False
y.align['OFFENCE']='l'
y.align['PUNISHMENT']='l'
max_width=15
y.min_width['VEH']=4
y.min_width['F.O.DATE']=10
y.min_width['OFFENCE']=20
y.min_width['PUNISHMENT']=18

aldis_cur=phis_conn.cursor()
aldisquery='SELECT * FROM alldis WHERE STNO=%s'
pvalue=(pnstno,)
aldis_cur.execute(aldisquery,pvalue)
aldis_result=aldis_cur.fetchall()

offmas_cur=phis_conn.cursor()
offmasquery='SELECT OFFEN, OFFENCE FROM offmas'
offmas_cur.execute(offmasquery)
offmas_result=offmas_cur.fetchall()

phis_cur = phis_conn.cursor()
pquery = 'SELECT * FROM palldis WHERE STNO = %s'
pvalue = (pnstno,)
phis_cur.execute(pquery, pvalue)
result = phis_cur.fetchall()

fomas_cur=phis_conn.cursor()
fomas_query='SELECT * FROM FOMAS'
fomas_cur.execute(fomas_query)
fomas_result=fomas_cur.fetchall()
# if fomas_result:
#     column_names=[column[0] for column in fomas_cur.description]
#     # fomas_result_dict=[dict(zip(column_names,row)) for row in fomas_result]
#     fomas_result_dict = {row[0]: row[1] for row in fomas_result}
#     # print(fomas_result_dict)

if result:
    column_names=[column[0] for column in phis_cur.description]
    # print(column_names)
    result_dict = [dict(zip(column_names, row)) for row in result]



if offmas_result:
    offmas_offence=[]
    column_names=[column[0] for column in offmas_cur.description]
    offmas_result_dict={row[0]:row[1] for row in offmas_result}

    for row in offmas_result:
        offmas_result_dict1 = dict(zip(column_names, row))
        offmas_offence.append(offmas_result_dict1['OFFENCE'])


if aldis_result:
    with open(filepath,"a") as file:
        file.write('File in process..\n')
    file.close()
    column_names = [column[0] for column in aldis_cur.description]
    SN=0
    al_dis_offences_list=[]
    al_dis_offences_dict={}
    for record in aldis_result:
        al_dis_record = dict(zip(column_names, record))
        al_dis_offences_list.append(al_dis_record)

    for idx,record in enumerate(al_dis_offences_list):
        # print(record['OFF_REM'])
    
        if record['DT_OFFEN'] is not None:
            date_fmt2=  datetime.strptime(str(record['DT_OFFEN']),'%Y-%m-%d').strftime('%d-%m-%Y')
        else:
            date_fmt2='-'
        
        off_code1=record['OFFEN']
        if off_code1:
            off_code1_dis=offmas_result_dict.get(off_code1)
        else:
            off_code1_dis=record['OFF_REM']
        # print(off_code1_dis)
        SN=+1
        # wrapped_sn=textwrap.fill(str(SN),width=3)    
        wrapped_offence2=textwrap.fill(off_code1_dis,width=20)
        wrapped_punish2=textwrap.fill('**pending**',width=20)
        wrapped_file_year1=record['SNO']+'/'+str(record['YEA'])
        wrapped_vehno1 = textwrap.fill(record['VEHNO'], width=20)
        wrapped_fodate1=textwrap.fill(' ',width=10)
        y.add_row([idx+1,date_fmt2,wrapped_vehno1,wrapped_offence2,wrapped_punish2,wrapped_file_year1,wrapped_fodate1])
        SN=+1
            # print(y)    
    else:
        with open(filepath,"a") as file:
            file.write('File in process.....NIL...\n')
        file.close()
    with open(filepath,"a") as file:
        file.write(str(y))
    file.close()
    # else:
    offences=[]
    offences =[record['OFFEN'] for record in al_dis_offences_list]
    print(offences)
    # offences.extend(al_dis_offences)
    # offences.extend(offmas_offence)
    # print(offences)
    offence_counts = Counter(offences)
    # print(offences)
    # print(al_dis_offences)
    # sorted_counts = dict(sorted(offence_counts.items()))

    offence_list = []
    count_list = []
    for offence, count in offence_counts.items():
        offence_list.append(offence)
        count_list.append(count)
    # print(offence_counts)
    with open(filepath,"a") as file:
        file.write('\nABSTRACT\n')
        file.write('--------\n')
        # Calculate the number of offenses to be printed in each column
        num_offences = len(offence_list)
        num_offences_col1 = (num_offences + 1) // 2
        num_offences_col2 = num_offences - num_offences_col1

        # Print offenses and counts in two columns
        # for i in range(num_offences_col1):
        #     file.write(f"{offence_list[i]:<33} - {count_list[i]}")
        #     if i + num_offences_col1 < num_offences:
        #             file.write(f"    {offence_list[i+num_offences_col1]:<33} - {count_list[i+num_offences_col1]}")
        #     file.write("\n")
            
    file.close()
    ####------------------END OF ALLDIS DAT