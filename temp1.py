import pymysql
from tkinter import messagebox

def offcode():
    try:
        conn=pymysql.connect(user='root',password='password',host='localhost',database='mydatabase')
        off_cursor=conn.cursor()
        off_cursor.execute(query='SELECT * FROM OFFMAS')
        off_code1=[]
        off_name=[]
        for row in off_cursor:
            off_code1.append(row[1])
            off_name.append(row[2])
        print(off_code1, off_name)
    except Exception as e:
        messagebox.showerror("DatabaseError","Database connectivity error: {}".format(e))

offcode()
