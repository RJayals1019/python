import pymysql
from dbfread import DBF, Field
from dbfread.fields import Field
# Connect to MySQL database
conn = pymysql.connect(host='localhost', user='root', password='password', db='mydatabase')
cur = conn.cursor()

# Fetch structure of alldis table
cur.execute("DESCRIBE alldis")
fields = cur.fetchall()
field_names = [field[0] for field in fields]
field_types = [field[1] for field in fields]

# Create list of Field objects for dbfread
dbf_fields = [Field(name, field_type) for name, field_type in zip(field_names, field_types)]

# Create dBaseIV table
dbf_table = DBF('alldis1.dbf', new=True, fields=dbf_fields)

# Fetch data from alldis table
cur.execute("SELECT * FROM alldis")
data = cur.fetchall()

# Append data to dBaseIV table
for row in data:
    rec = dbf_table.new_record()
    for i, field in enumerate(field_names):
        rec[field] = row[i]
    rec.store()

# Close database connection
conn.close()
