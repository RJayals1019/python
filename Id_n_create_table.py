#  create mysql table with id field created and insert increment value

import pymysql
from dbfread import DBF
import traceback

# open the dbf file
table = DBF('YTDAYS.DBF')

# map the field types from dbf to MySQL
type_map = {
    'C': 'VARCHAR(255)',
    'N': 'INT',
    'D': 'DATE',
    'L': 'BOOL'
}

# get the field names and data types from the dbf file
field_names = [field.name for field in table.fields]
data_types = [type_map.get(field.type, 'VARCHAR(255)') for field in table.fields]

# add an id field to the field names and data types
field_names.insert(0, 'id')
data_types.insert(0, 'INT PRIMARY KEY AUTO_INCREMENT')

# create the MySQL table
try:
    # connect to the MySQL server
    conn = pymysql.connect(
        host='localhost',
        user='root',
        password='password',
        db='mydatabase'
    )

    # create a cursor object
    cursor = conn.cursor()

    # build the create table query
    create_query = 'CREATE TABLE IF NOT EXISTS YTDAYS ('
    for i in range(len(field_names)):
        create_query += f'{field_names[i]} {data_types[i]}, '
    create_query = create_query[:-2] + ')'

    # print the create table query for debugging
    # print(create_query)

    # execute the create table query
    cursor.execute(create_query)

    # insert records into the MySQL table
    for i, record in enumerate(table):
        # add the id value to the record
        record_values = [i + 1] + [record[field] for field in field_names[1:]]
        insert_query = f"INSERT INTO YTDAYS ({', '.join(field_names)}) VALUES ({', '.join(['%s'] * len(field_names))})"
        cursor.execute(insert_query, record_values)

    # commit the transaction
    conn.commit()

except Exception as e:
    print('Error creating table:')
    traceback.print_exc()

else:
    print('Table created and data inserted successfully!')

finally:
    # close the cursor and connection
    cursor.close()
    conn.close()
