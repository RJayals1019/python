import tkinter as tk
import dbcon
from datetime import datetime

def mon_abs_report():
    line1=''
    gap=' '*3
    mon_abs_con=dbcon.connect1()
    mon_abs_cur=mon_abs_con.cursor()
    #query1="SELECT * FROM ALLDIS"
    query1="SELECT CAT,SEAT_NO,DT_OFFEN FROM ALLDIS order by CAT,SEAT_NO"
    mon_abs_cur.execute(query1)
    mon_abs_result=mon_abs_cur.fetchall()
    # print(mon_abs_result)

    mon_abs_result_list=[]
    l1=''
    if mon_abs_result:
        column_names=[column[0] for column in mon_abs_cur.description ]
        for record in mon_abs_result:
            mon_abs_result_dict=dict(zip(column_names,record))
            mon_abs_result_list.append(mon_abs_result_dict)
    # sorted_mon_abs_result_list=sorted(mon_abs_result_list,key=lambda x:x[])
    sorted_mon_abs_result= sorted(mon_abs_result_list,key=lambda x:(x['CAT'],x['SEAT_NO']))
    # sorted_mon_abs_result= sorted(mon_abs_result_list,key=lambda x:x['CAT'])
    # for items in sorted_mon_abs_result:
    #     l1+=str(items)

    # with open('D:\TXT1.TXT','w') as file:
    #     file.write(str(l1))


    
    # Initialize counters
    cat_wise = {}
    seat_wise = {}
    
    global cat_admin3,cat_tech3,cat_dri3,cat_cond3
    global cat_admin6,cat_tech6,cat_dri6, cat_cond6 
    global cat_admin9,cat_tech9,cat_dri9,cat_cond9 
    global cat_admin12,cat_tech12,cat_dri12,cat_cond12 
    global cat_admin24,cat_tech24,cat_dri24,cat_cond24 
    global cat_admina24,cat_techa24,cat_dria24,cat_conda24 
    global cat_admintot,cat_techtot,cat_dritot,cat_condtot 
    
    cat_admin3,cat_tech3,cat_dri3,cat_cond3=0,0,0,0
    cat_admin6,cat_tech6,cat_dri6, cat_cond6=0,0,0,0
    cat_admin9,cat_tech9,cat_dri9,cat_cond9=0,0,0,0
    cat_admin12,cat_tech12,cat_dri12,cat_cond12=0,0,0,0
    cat_admin24,cat_tech24,cat_dri24,cat_cond24=0,0,0,0
    cat_admina24,cat_techa24,cat_dria24,cat_conda24=0,0,0,0
    cat_admintot,cat_techtot,cat_dritot,cat_condtot=0,0,0,0

    # Process the data
    # print(mon_abs_result_list)

    
    uniq_cat=sorted(set(item['CAT'] for item in sorted_mon_abs_result))
    print(uniq_cat)

    uniq_seat=sorted(set(int(item['SEAT_NO']) for item in sorted_mon_abs_result))
    # print(uniq_seat)
    tseat_count=len(uniq_seat)
    # print(tseat_count)
    tseat={}
    tseat_values={}
    uniq_dseat=[]

    for idx,uniq_seat_items in enumerate(uniq_seat):
        # tseat[id]=uniq_seat_items
        # tseat_values[idx] = tseat[id]  
        tseat = 'D'+str(uniq_seat_items)
        uniq_dseat.append(tseat)
        # print(tseat)
    # for idx,item in range(uniq_dseat):
    seat_1=[ item for item in sorted_mon_abs_result  if item['SEAT_NO']=='1']
    # print(seat_1)
    
    print(uniq_dseat)
    print (len(uniq_dseat))
    def abs_1():
        global cat_admin3,cat_tech3,cat_dri3,cat_cond3
        global cat_admin6,cat_tech6,cat_dri6, cat_cond6 
        global cat_admin9,cat_tech9,cat_dri9,cat_cond9 
        global cat_admin12,cat_tech12,cat_dri12,cat_cond12 
        global cat_admin24,cat_tech24,cat_dri24,cat_cond24 
        global cat_admina24,cat_techa24,cat_dria24,cat_conda24 
        global cat_admintot,cat_techtot,cat_dritot,cat_condtot 
     
      

    # for item in uniq_cat:
    for item in sorted_mon_abs_result:

        cat = item['CAT']
        seat = item['SEAT_NO']
        if item['DT_OFFEN'] is None:
            months_since_offence=730
        else:
            date_of_offence = datetime.strptime(str(item['DT_OFFEN']), '%Y-%m-%d')
            months_since_offence = (datetime.now() - date_of_offence).days // 30
        if months_since_offence < 3:
             if(cat == 1):
                 if(seat == 1):
                     cat_admin13 = cat_admin13 + 1
                 if(seat == 2):
                     cat_admin23 = cat_admin23 + 1    

        print("tcat:"+str(cat)+"seat no"+str(seat)+'-'+str(months_since_offence))
      
mon_abs_report()