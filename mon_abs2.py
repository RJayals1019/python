import tkinter as tk
import dbcon
import datetime
from dateutil.relativedelta import relativedelta
from datetime import datetime
import os

def mon_abs_begin(cat,startdate):
    mon_abs_con1=dbcon.connect1()
    mon_abs_cur1=mon_abs_con1.cursor()
    query="SELECT count(cat) FROM ALLDIS where CAT =%s AND CRT_DATE < %s and LENGTH(trim(FO_CODE)) = 0 "
    mon_abs_cur1.execute(query,(cat,startdate))
    mon_abs_begin=mon_abs_cur1.fetchall()
    return mon_abs_begin
def mon_abs_receipt(row,row1,startdate,endDate):
    mon_abs_con1=dbcon.connect1()
    mon_abs_cur1=mon_abs_con1.cursor()
    query="SELECT count(cat) FROM ALLDIS where CAT =%s AND  CRT_DATE BETWEEN %s AND %s and LENGTH(trim(FO_CODE)) > 0"
    mon_abs_cur1.execute(query,(row,row1,startdate,endDate))
    mon_abs_receipt=mon_abs_cur1.fetchall()
    return mon_abs_receipt
def mon_abs_disposed(row,row1,startdate,endDate):
    mon_abs_con1=dbcon.connect1()
    mon_abs_cur1=mon_abs_con1.cursor()
    query="SELECT count(cat) FROM ALLDIS where cat = %s and CRT_DATE BETWEEN %s AND %s and LENGTH(trim(FO_CODE)) > 0"
    mon_abs_cur1.execute(query,(row,row1,startdate,endDate))
    mon_abs_Disposed=mon_abs_cur1.fetchall()
    return mon_abs_Disposed
def mon_abs_Closing(row,row1,startdate,endDate):
    mon_abs_con1=dbcon.connect1()
    mon_abs_cur1=mon_abs_con1.cursor()
    query="SELECT count(cat) FROM ALLDIS where cat = %s and CRT_DATE BETWEEN %s AND %s and LENGTH(trim(FO_CODE)) > 0"
    mon_abs_cur1.execute(query,(row,row1,startdate,endDate))
    mon_abs_close=mon_abs_cur1.fetchall()
    return mon_abs_close
def mon_abs_report():
    line1=''
    Tmp_CatName =""
    tot3mon = 0
    tot6mon = 0
    tot9mon = 0
    tot1mon = 0
    tot2mon = 0
    tot1mor = 0
    cumtotal = 0
    CatName = ""
    Start3Mon = datetime.today()
    End3Mon = Start3Mon - relativedelta(months = 3)
    Start6Mon = End3Mon - relativedelta(days=1)
    End6Mon = Start6Mon - relativedelta(months=3)
    Start9Mon = End6Mon - relativedelta(days=1)
    End9Mon = Start9Mon - relativedelta(months = 3)
    Start12Mon = End9Mon - relativedelta(days=1)
    End12Mon = Start12Mon - relativedelta(months = 3)
    Start1Year = End12Mon - relativedelta(days=1)
    End1Year = Start1Year - relativedelta(months = 12)
    Start2Year = End1Year - relativedelta(days=1)
    mon_abs_con=dbcon.connect1()
    mon_abs_cur=mon_abs_con.cursor()
    if os.path.exists("abs.txt"):
         os.remove("abs.txt")
    file1 = open("abs.txt","a")
    file1.write('%-15s %-55s\n' %("","TamilNadu State Tansport Coporation(Salem) Ltd.,salem-7"))
    file1.write('%-15s %-55s\n' %("","-------------------------------------------------------"))
    file1.write('%10s %-65s %-8s\n' %("","PARTICULARS SHOWING DISCIPLINARY CASES PENDING FOR THE MONTH OF - ",Start3Mon.strftime('%b')+"-"+Start3Mon.strftime('%Y')))
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")
    file1.write(":")    
    file1.write("\n")
    file1.write('%1s %-4s %1s %-20s %1s %-5s %1s %-5s %1s %-5s %1s %-5s %1s %-5s %1s' %("|","SNO.","|","  DESCRIPTION  ","|","ADMIN","|","TECH.","|","CONDR","|","DRIVR","|","TOTAL","|"))
    file1.write("\n")
    file1.write('%1s %-4s %1s %-20s %1s %-5s %1s %-5s %1s %-5s %1s %-5s %1s %-5s %1s' %("|","","|","","|","","|","","|","","|","","|","","|"))
    file1.write("\n")
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")
    file1.write(":")
    values1 = range(4) 
    r = 1
    for r in values1:
        if r == 1:
            Tmp_CatNameRow11 = "No Of Cases Pending "
            Tmp_CatNameRow12 = "At The Begining     "
        if r == 2:    
            Tmp_CatNameRow21 = "No Of Cases Received "
            Tmp_CatNameRow22 = "During The Month     "
        if r == 3:
            Tmp_CatNameRow31 = "No Of Cases Disposed "
            Tmp_CatNameRow32 = "During The Month     "
        if r ==4:    
            Tmp_CatNameRow41 = "No Of Cases Pending  "
            Tmp_CatNameRow42 = "At The End Of Month  "
        j = 1
        row1 = 0    
        row2 = 0    
        row3 = 0    
        row4 = 0 
        i = 0;   
        for j in values1:
            if r == 1:
              row1[i] = mon_abs_begin(j,'2023-02-01')
            if r == 2:
               row2[i] = mon_abs_receipt(j,'2023-02-01')
            if r == 3:
                row3[i]=mon_abs_disposed(j,'2023-02-01')
            if r == 4:
                row4[i] = mon_abs_Closing(j,'2023-02-01') 
            i = i + 1               
    file1.write("\n")
    if Tmp_CatName != CatName :
       if len(CatName.lstrip()) > 0 :
            values = range(92)
            file1.write(":")   
            for i in values:
               file1.write("-")
            file1.write(":")      
            file1.write("\n") 
            file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|",Tmp_CatName,"|",row1[0],"|",str(mon3reports[0]),"|",str(mon6reports[0]),"|",str(mon9reports[0]),"|",str(mon1reports[0]),"|",str(mon2reports[0]),"|",str(mor1reports[0]),"|",total,"|"))
       else: 
            file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|",Tmp_CatName,"|",row1[0],"|",str(mon3reports[0]),"|",str(mon6reports[0]),"|",str(mon9reports[0]),"|",str(mon1reports[0]),"|",str(mon2reports[0]),"|",str(mor1reports[0]),"|",total,"|"))   
            CatName = Tmp_CatName
    else:
        file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|","","|",row1[0],"|",str(mon3reports[0]),"|",str(mon6reports[0]),"|",str(mon9reports[0]),"|",str(mon1reports[0]),"|",str(mon2reports[0]),"|",str(mor1reports[0]),"|",total,"|"))     
    file1.write("\n")
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")  
    file1.write(":")    
    file1.write("\n")  
    cumtotal =  tot3mon + tot6mon + tot9mon + tot1mon + tot2mon + tot1mor 
    file1.write('%1s %-12s %1s %-5s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s %-7s %1s' %("|"," TOTAL ","|","","|",tot3mon,"|",tot6mon,"|",tot9mon,"|",tot1mon,"|",tot2mon,"|",tot1mor,"|",cumtotal,"|"))                  
    file1.write("\n")
    file1.write(":")
    values = range(92)
    for i in values:
        file1.write("=")
    file1.write(":")      
    file1.close 

mon_abs_report()