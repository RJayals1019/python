from tkinter import * 
from tkinter import messagebox

root = Tk()
root.title("Login Form")
root.geometry("850x450+300+200")
root.configure(bg ="white")
root.resizable(False,False)

img = PhotoImage(file="D:\main_python\DISP\image\orglogo.png")
Label(root,image=img,bg="white").place(x=10,y=10)

frame = Frame(root,width=350,height=350,bg="white")
frame.place(x=480,y=70)

heading = Label(frame,text="sign in",fg="blue",bg="white",font=("Microsoft YaHei UI Light",23,"bold"))
heading.place(x=110,y=5)

def on_enter(e):
    user.delete(0,"end")
def on_leave(e):
    name = user.get()    
    if(name ==""):
        user.insert(0,"UserName")
        messagebox.showwarning(title="alert",message="User Name is Empty")
        user.focus()
    

user = Entry(frame,width=25,fg="navy blue",border=0,font=("Microsoft YaHei UI Light",11,"bold"))
user.place(x=30,y=80)
user.insert(0,"UserName")
user.bind("<FocusIn>",on_enter)
user.bind("<FocusOut>",on_leave)

Frame(frame,width=295,height=2,bg="black").place(x=25,y=107)

def on_enter(e):
    code.delete(0,"end")
def on_leave(e):
    code1 = code.get()    
    if(code1 ==""):
        code.insert(0,"PassWord")
     #   messagebox.showwarning(title="alert",message="Password is Empty")
     #   code.focus()
    

code = Entry(frame,width=25,fg="navy blue",border=0,font=("Microsoft YaHei UI Light",11,"bold"))
code.place(x=30,y=150)
code.insert(0,"Password")
code.bind("<FocusIn>",on_enter)
code.bind("<FocusOut>",on_leave)

Frame(frame,width=295,height=2,bg="black").place(x=25,y=177)

Button(frame,width=39,pady=7,text="sign in",bg="#57a1f8",fg="white",border=0).place(x=35,y=204)
label = Label(frame,text="Don't Have an Account?",fg="black",bg="white",font=("Microsoft YaHei UI Light",11,"bold"))
label.place(x=55,y=270)

sign_up =Button(frame,width=6,text="sign up",border=0,bg="white",cursor="hand2",fg="blue")
sign_up.place(x=250,y=270)
root.mainloop()
