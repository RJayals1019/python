# print("god is great")
import tkinter as tk
from tkinter import *
from datetime import datetime
from PIL import Image, ImageTk

from tkinter import messagebox
from tkcalendar import DateEntry

## frames import
import entry2
## database connection import
import dbcon
# off_data=[dbcon.offcode]
## Correction Frame import
import corr_entry
from corr_entry import class_corr
## Memo Print import
import new_memo_prt
## Memo_template import
import memo_template
# ## FO class impoer
# import fo_class
import phis
import npmas_update
import ytd_update
import ytd_mon
import pyreports 
from pyreports import reports

root = Tk()
root.title("TNSTC (SLM) LTD., Disciplinary Menu")
x1=root.winfo_screenmmwidth()//2
y1=int(root.winfo_screenheight()*0.1)
root.geometry('950x580+'+str(x1)+'+'+str(y1))
root.resizable(height=False,width=False)
root.configure(bg='white')

image1 = PhotoImage(file="d:\main_python\disp\image\logo1.png")
original_image1 = image1.subsample(1,1)  # resize image using subsample
Label(root, image=original_image1,width=125,height=100,bg="white").grid(row=0, column=0, padx=0, pady=0)

Top_frame=tk.Frame(root,width=425,height=100,background="white")
Top_frame.grid(row=0,column=1,padx=5,pady=5,sticky='nsew')
Top_frame.grid_propagate(False)

Label(Top_frame, 
              text="TAMILNADU STATE TRANSPORT COPORATION (SLM) LTD.,",foreground="navy blue"
      ,font ="Courier 17 bold",bg="white").grid(row=0, column=1, padx=5, pady=2)
Label(Top_frame, 
              text=" SALEM - 636 007 ",foreground="navy blue"
      ,font ="Courier 14 bold",bg="white").grid(row=1, column=1, padx=5, pady=2)

Label(Top_frame, 
              text="DISCIPLINARY WING ",foreground="magenta"
      ,font ="Courier 17 bold",bg="white").grid(row=2, column=1, padx=5, pady=2)

left_frame=tk.Frame(root,width=125,height=450,background="#DADBDD")
left_frame.grid(row=1,column=0,padx=5,pady=5,rowspan=2,sticky='nsew')
left_frame.grid_propagate(False)

right_top_frame=tk.Frame(root,width=800,height=450,background="#D3D3D3")
right_top_frame.grid(row=1,column=1,padx=5,pady=5,rowspan=2)
right_top_frame.grid_propagate(False)

image = ImageTk.PhotoImage(file="d:\main_python\disp\image\img1.jpg")
original_image = image
Label(right_top_frame, image=original_image,width=700,height=430,bg="white").grid(row=2, column=2, padx=40, pady=10)
def close_window (): 
    root.destroy()

def show_frame(frame):
    # frame.tkraise()
    global current_frame
    if current_frame:
        current_frame.destroy()
    current_frame = frame
    current_frame.tkraise()

current_frame = None

btn1=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Legal Data Entry',bg='white',command=lambda: show_frame(entry2.Entry2(right_top_frame)))
btn2=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Data Correction',bg='white',command=lambda:show_frame(corr_entry.class_corr(right_top_frame)))
btn3=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Memo Print',bg='white', command=lambda:show_frame(new_memo_prt.memo_prt_class(right_top_frame)))
btn4=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Memo Template',bg='white',command=lambda:show_frame(memo_template.memo_template_class(right_top_frame)))
btn5=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='PH Print',bg='white',command=lambda:show_frame(phis.phis(right_top_frame)))
btn6=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Staff Master Update',bg='white',command=lambda:show_frame(npmas_update.npmas_update(right_top_frame)))
btn7=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='YTD9009 Update',bg='white',command=lambda:show_frame(ytd_update.ytd9009_update(right_top_frame)))
btn8=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='YT  Mon Update',bg='white',command=lambda:show_frame(ytd_mon.ytdmon_update(right_top_frame)))
btn9=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Reports',bg='white',command=lambda:show_frame(pyreports.reports(right_top_frame)))
btn10=Button(left_frame,width=15,font ="Helvetica 9 bold",fg ="navy blue",text='Exit',bg='white',command=close_window)

btn1.grid(row=0,column=0,padx=5,pady=5)
btn2.grid(row=1,column=0,padx=5,pady=5)
btn3.grid(row=2,column=0,padx=5,pady=5)
btn4.grid(row=3,column=0,padx=5,pady=5)
btn5.grid(row=4,column=0,padx=5,pady=5)
btn6.grid(row=5,column=0,padx=5,pady=5)
btn7.grid(row=6,column=0,padx=5,pady=5)
btn8.grid(row=7,column=0,padx=5,pady=5)
btn9.grid(row=8,column=0,padx=5,pady=5)
btn10.grid(row=9,column=0,padx=5,pady=5)

root.mainloop()