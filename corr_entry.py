import tkinter as tk
from tkinter import ttk
import dbcon
from datetime import datetime
from tkinter import messagebox
from tkcalendar import DateEntry
import calen_class
# import fo_class

class class_corr(tk.Frame):
    
    def __init__(self,master=None):
        super().__init__(master,width=800,height=540,background='white')

    # Frames
        self.top_frame=tk.LabelFrame(self,text='Basic',font ="Helvetica 9 bold",fg ="navy blue",width=790,height=100,background='lightcyan')
        self.bottom_frame=tk.LabelFrame(self,text='Correction Entry',font ="Helvetica 9 bold",fg ="navy blue",width=920,height=650,background='oldlace')
        self.down_frame=tk.LabelFrame(self,text='Descrption',font ="Helvetica 9 bold",fg ="navy blue",width=920,height=50,background='tan')

        self.top_frame.grid(row=0,column=0)
        self.bottom_frame.grid(row=1,column=0,sticky='w')
        self.down_frame.grid(row=13,column=0,sticky='w')
        lbl_updated=tk.Label(self,text=' ', bg='#C9C0BB')
        lbl_updated.grid(row=2,column=0,sticky='w')

        # Load data from alldis table
        def load_data():
            # for widget in self.bottom_frame.winfo_children():
            #     if isinstance(widget, tk.Entry):  # Check if widget is an entry widget
            #         # widget.config(insertbackground='white')
            #         # widget.config(bg='white')
            #         widget.config(highlightbackground='white')
            #         widget.config(highlightcolor='white')
            #         print('Works ')
            
            # # ent_enq_dt1.config(bg='white')
            lbl_updated.config(text='...')    
            case_ent_data=case_ent.get()
            seat_ent_data=seat_ent.get()
            curr_ent_data=curr_ent.get().strip()
            if curr_ent_data.isdigit():
                curr_ent_data = int(curr_ent_data)
            else:
                curr_ent_data = None

            #INTIAL DELETE
            case_entb.configure(state='normal')  
            case_entb.delete(0,tk.END)
            seat_entb.configure(state='normal')
            seat_entb.delete(0,tk.END)
            curr_entb.configure(state='normal')
            curr_entb.delete(0,tk.END)
            case_entb.configure(state='readonly') 
            seat_entb.configure(state='readonly') 
            curr_entb.configure(state='readonly')   
            cat_ent2.delete(0,tk.END)          
            month_ent5.delete(0,tk.END)
            year_ent6.delete(0, tk.END)
            empno_ent7.delete(0, tk.END)
            branch_ent8.delete(0, tk.END)
            veh_ent9.delete(0, tk.END)
            off_code_ent10.delete(0, tk.END)
            off_other_ent22.delete(0, tk.END)
            off_dt1_ent11.delete(0, tk.END)
            off_dt2_ent12.delete(0, tk.END)
            off_rem_ent15.delete(0, tk.END)
            add_rem_ent16.delete(0, tk.END)
            ref_no_ent17.delete(0, tk.END)
            from_ent19.delete(0, tk.END)
            
            ent_memo_ack.delete(0,tk.END)
            ent_sus_ack_dt1.delete(0,tk.END)
            ent_scn_snt_dt1.delete(0,tk.END)
            ent_scn_ack_dt1.delete(0,tk.END)
            
            ent_enq_dt1.delete(0,tk.END)
            ent_enq_dt2.delete(0,tk.END)
            ent_enq_dt3.delete(0,tk.END)
            ent_enq_dt4.delete(0,tk.END)
            ent_enq_dt5.delete(0,tk.END)
            ent_enq_dt6.delete(0,tk.END)

            ent_enq_ack_dt1.delete(0,tk.END)
            ent_enq_ack_dt2.delete(0,tk.END)
            ent_enq_ack_dt3.delete(0,tk.END)
            ent_enq_ack_dt4.delete(0,tk.END)
            ent_enq_ack_dt5.delete(0,tk.END)

            ent_sus_dt.delete(0,tk.END)                                            
            ent_sus_rev.delete(0,tk.END)
            ent_fo_code.delete(0,tk.END)
            ent_fo_rem.delete(0,tk.END)
            ent_fo_date.delete(0,tk.END)
            ent_no_inst.delete(0,tk.END)
            ent_crt_dt.delete(0,tk.END)
            ent_details1.delete(0,tk.END)



            corr_data=dbcon.connect1()
            corr_data=corr_data.cursor()
            with corr_data as cursor:
                corr_query = "SELECT * FROM ALLDIS WHERE SNO=%s AND SEAT_NO=%s AND CRTNO=%s"
                cursor.execute(corr_query, (case_ent_data, seat_ent_data, curr_ent_data))
                corr_result=cursor.fetchall()

                if corr_result:
                    for widget in self.bottom_frame.winfo_children():
                        widget.configure(state='normal')
                        
                    for row in corr_result:
                        global cat_rec, case_rec, seat_rec, crt_rec, empno_rec, month_rec, year_rec, branch_rec, veh_rec, off_code_rec, off_other_rec, off_dt1_rec, off_dt2_rec, off_rem_rec, add_rem_rec, ref_no_rec, ref_dt_rec, from_rec
                        global ent_memo_ack_rec,ent_sus_ack_dt1_rec,  ent_scn_snt_dt1_rec, ent_scn_ack_dt1_rec, ent_enq_dt1_rec,  ent_enq_dt2_rec, ent_enq_dt3_rec
                        global ent_enq_dt4_rec,  ent_enq_dt5_rec, ent_enq_dt6_rec,ent_enq_ack_dt1_rec, ent_enq_ack_dt2_rec,  ent_enq_ack_dt3_rec, ent_enq_ack_dt4_rec 
                        global ent_enq_ack_dt5_rec, ent_sus_dt_rec, ent_sus_rev_rec,  ent_fo_code_rec, ent_fo_rem_rec,  ent_fo_date_rec, ent_no_inst_rec, ent_crt_dt_rec, ent_details1_rec
 

                        # print(row)
                        cat_rec=row[1]
                        case_rec=row[4]
                        seat_rec=row[5]
                        crt_rec=row[6]
                        empno_rec=row[7]
                        month_rec=row[2]
                        year_rec=row[3]
                        branch_rec=row[10]
                        veh_rec=row[11]
                        off_code_rec=row[12]
                        off_other_rec=row[50]
                        off_dt1_rec=row[15]
                        off_dt2_rec=row[51]
                        off_rem_rec=row[13]
                        add_rem_rec=row[14]
                        ref_no_rec=row[52]
                        ref_dt_rec=row[53]
                        from_rec=row[54]
                        ent_memo_ack_rec=row[55]
                        ent_sus_ack_dt1_rec=row[56]
                        ent_scn_snt_dt1_rec=row[58]
                        ent_scn_ack_dt1_rec=row[57]
                        
                        ent_enq_dt1_rec=row[59]
                        ent_enq_dt2_rec=row[60]
                        ent_enq_dt3_rec=row[61]
                        ent_enq_dt4_rec=row[62]
                        ent_enq_dt5_rec=row[63]
                        ent_enq_dt6_rec=row[64]

                        ent_enq_ack_dt1_rec=row[65]
                        ent_enq_ack_dt2_rec=row[66]
                        ent_enq_ack_dt3_rec=row[67]
                        ent_enq_ack_dt4_rec=row[68]
                        ent_enq_ack_dt5_rec=row[69]

                        ent_sus_dt_rec=row[16]                                     
                        ent_sus_rev_rec=row[17]
                        ent_fo_code_rec=row[34]
                        ent_fo_rem_rec=row[35]
                        ent_fo_date_rec=row[36]
                        ent_no_inst_rec=row[37]
                        ent_crt_dt_rec=row[38]
                        ent_details1_rec=row[41]



                        # cat_ent2.delete(0,tk.END)
                        if cat_rec==1:
                            cat_ent2.current(0)
                        elif cat_rec==2:
                            cat_ent2.current(3)    
                        elif cat_rec==3:
                            cat_ent2.current(2)    
                        elif cat_rec==4:
                            cat_ent2.current(1)    

                        case_entb.delete(0,tk.END)
                        case_entb.insert(0,case_rec)
                        seat_entb.delete(0,tk.END)
                        seat_entb.insert(0,seat_rec)
                        curr_entb.delete(0,tk.END)
                        curr_entb.insert(0,crt_rec)
                        case_entb.configure(state='readonly')
                        seat_entb.configure(state='readonly')
                        curr_entb.configure(state='readonly')
                        month_ent5.delete(0,tk.END)
                        month_ent5.insert(0,month_rec)
                        year_ent6.delete(0, tk.END)
                        year_ent6.insert(0,year_rec)
                        empno_ent7.delete(0, tk.END)
                        empno_ent7.insert(0, empno_rec)
                        branch_ent8.delete(0, tk.END)
                        branch_ent8.insert(0,branch_rec)
                        veh_ent9.delete(0, tk.END)
                        if veh_ent9:
                            veh_ent9.insert(0,veh_rec)
                        else:
                            veh_ent9.insert(0,' ')    
                        off_code_ent10.delete(0, tk.END)
                        off_code_ent10.set(off_code_rec)
                        off_code_lbl.config(text=' ')
                        off_other_ent22.delete(0, tk.END)
                        if off_other_rec:
                            off_other_ent22.insert(0, str(off_other_rec))
                        else :
                            off_other_ent22.insert(0, ' ')      

                        off_dt1_ent11.delete(0, tk.END)
                        if off_dt1_rec:
                            offdate_obj1 = datetime.strptime(str(off_dt1_rec), '%Y-%m-%d')
                            date_str1 = datetime.strftime(offdate_obj1, '%d-%m-%Y')
                            off_dt1_ent11.insert(0, date_str1)
                        else:    
                            off_dt1_ent11.delete(0, tk.END)

                        off_dt2_ent12.delete(0, tk.END)
                        if off_dt2_rec:
                            # print(off_dt2_rec)
                            offdate_obj3 = datetime.strptime(str(off_dt2_rec), '%Y-%m-%d')
                            date_str2 = datetime.strftime(off_dt2_rec, '%d-%m-%Y')
                            off_dt2_ent12.insert(0,date_str2)
                        else:
                            # print(off_dt2_rec)
                            off_dt2_ent12.delete(0,tk.END)

                        off_rem_ent15.delete(0, tk.END)
                        off_rem_ent15.insert(0, off_rem_rec)
                        add_rem_ent16.delete(0, tk.END)
                        add_rem_ent16.insert(0, add_rem_rec)
                        ref_no_ent17.delete(0, tk.END)
                        if ref_no_rec:
                            ref_no_ent17.insert(0, str(ref_no_rec))
                        else:
                            ref_no_ent17.insert(0,' ')

                        ref_dt_ent18.delete(0, tk.END)
                        if ref_dt_rec:
                            refdate_obj1=datetime.strptime(str(ref_dt_rec),'%Y-%m-%d')
                            refdate_obj=datetime.strftime(refdate_obj1,'%d-%m-%Y')
                            ref_dt_ent18.insert(0,refdate_obj)
                        else:
                            ref_dt_ent18.insert(0,' ') 

                        # ref_dt_ent18.insert(0, ref_dt_rec)
                        from_ent19.delete(0, tk.END)
                        if from_rec:
                            from_ent19.insert(0, str(from_rec))
                        else:
                            from_ent19.insert(0,' ')  
                        
                        ent_memo_ack.delete(0,tk.END)
                        if ent_memo_ack_rec:
                            ent_memo_ack_dt_obj=datetime.strptime(str(ent_memo_ack_rec),'%Y-%m-%d')
                            ent_memo_ack_dt_str=datetime.strftime(ent_memo_ack_dt_obj,'%d-%m-%Y')
                            ent_memo_ack.insert(0,ent_memo_ack_dt_str)
                        else:
                            ent_memo_ack.insert(0,' ')    

                        ent_sus_ack_dt1.delete(0,tk.END)
                        if ent_sus_ack_dt1_rec:
                            ent_sus_ack_dt1_obj=datetime.strptime(str(ent_sus_ack_dt1_rec),'%Y-%m-%d')
                            ent_sus_ack_dt1_str=datetime.strftime(ent_sus_ack_dt1_obj,'%d-%m-%Y')
                            ent_sus_ack_dt1.insert(0,ent_sus_ack_dt1_str)
                        else:
                            ent_sus_ack_dt1.insert(0,' ')    

                        
                        ent_scn_snt_dt1.delete(0,tk.END)
                        if ent_scn_snt_dt1_rec:
                            print(ent_scn_snt_dt1_rec)
                            ent_scn_snt_dt1_obj=datetime.strptime(str(ent_scn_snt_dt1_rec),'%Y-%m-%d')
                            ent_scn_snt_dt1_str=datetime.strftime(ent_scn_snt_dt1_obj,'%d-%m-%Y')
                            ent_scn_snt_dt1.insert(0,ent_scn_snt_dt1_str)
                            print(ent_scn_snt_dt1_str)
                        else:    
                            ent_scn_snt_dt1.insert(0,' ')
                            
                        ent_scn_ack_dt1.delete(0,tk.END)
                        if ent_scn_ack_dt1_rec:
                            print(ent_scn_ack_dt1_rec)
                            ent_scn_ack_dt1_obj=datetime.strptime(str(ent_scn_ack_dt1_rec),'%Y-%m-%d')
                            ent_scn_ack_dt1_str=datetime.strftime(ent_scn_ack_dt1_obj,'%d-%m-%Y')
                            ent_scn_ack_dt1.insert(0,ent_scn_ack_dt1_str)
                            print(ent_scn_ack_dt1_str)
                        else:    
                            ent_scn_snt_dt1.insert(0,' ')

                        ent_enq_dt1.delete(0,tk.END)
                        if ent_enq_dt1_rec:
                            ent_enq_dt1_obj=datetime.strptime(str(ent_enq_dt1_rec),'%Y-%m-%d')
                            ent_enq_dt1_str=datetime.strftime(ent_enq_dt1_obj,'%d-%m-%Y')
                            ent_enq_dt1.insert(0,ent_enq_dt1_str)
                        else:    
                            ent_enq_dt1.insert(0,' ')
                        
                        ent_enq_dt2.delete(0,tk.END)
                        if ent_enq_dt2_rec:
                            ent_enq_dt2_obj=datetime.strptime(str(ent_enq_dt2_rec),'%Y-%m-%d')
                            ent_enq_dt2_str=datetime.strftime(ent_enq_dt2_obj,'%d-%m-%Y')
                            ent_enq_dt2.insert(0,ent_enq_dt2_str)
                        else:    
                            ent_enq_dt2.insert(0,' ')

                        ent_enq_dt3.delete(0,tk.END)
                        if ent_enq_dt3_rec:
                            ent_enq_dt3_obj=datetime.strptime(str(ent_enq_dt3_rec),'%Y-%m-%d')
                            ent_enq_dt3_str=datetime.strftime(ent_enq_dt3_obj,'%d-%m-%Y')
                            ent_enq_dt3.insert(0,ent_enq_dt3_str)
                        else:    
                            ent_enq_dt3.insert(0,' ')
                             
                        ent_enq_dt4.delete(0,tk.END)
                        if ent_enq_dt4_rec:
                            ent_enq_dt4_obj=datetime.strptime(str(ent_enq_dt4_rec),'%Y-%m-%d')
                            ent_enq_dt4_str=datetime.strftime(ent_enq_dt4_obj,'%d-%m-%Y')
                            ent_enq_dt4.insert(0,ent_enq_dt4_str)
                        else:    
                            ent_enq_dt4.insert(0,' ')
                        
                        ent_enq_dt5.delete(0,tk.END)
                        if ent_enq_dt5_rec:
                            ent_enq_dt5_obj=datetime.strptime(str(ent_enq_dt5_rec),'%Y-%m-%d')
                            ent_enq_dt5_str=datetime.strftime(ent_enq_dt5_obj,'%d-%m-%Y')
                            ent_enq_dt5.insert(0,ent_enq_dt5_str)
                        else:    
                            ent_enq_dt5.insert(0,' ')
                            
                        ent_enq_dt6.delete(0,tk.END)
                        if ent_enq_dt6_rec:
                            ent_enq_dt6_obj=datetime.strptime(str(ent_enq_dt6_rec),'%Y-%m-%d')
                            ent_enq_dt6_str=datetime.strftime(ent_enq_dt6_obj,'%d-%m-%Y')
                            ent_enq_dt6.insert(0,ent_enq_dt6_str)
                        else:    
                            ent_enq_dt6.insert(0,' ')

                        ent_enq_ack_dt1.delete(0,tk.END)                
                        if ent_enq_ack_dt1_rec:
                            ent_enq_ack_dt1_obj=datetime.strptime(str(ent_enq_ack_dt1_rec),'%Y-%m-%d')
                            ent_enq_ack_dt1_str=datetime.strftime(ent_enq_ack_dt1_obj,'%d-%m-%Y')
                            ent_enq_ack_dt1.insert(0,ent_enq_ack_dt1_str)
                        else:
                            ent_enq_ack_dt1.insert(0,' ')

                        ent_enq_ack_dt2.delete(0,tk.END)                
                        if ent_enq_ack_dt2_rec:
                            ent_enq_ack_dt2_obj=datetime.strptime(str(ent_enq_ack_dt2_rec),'%Y-%m-%d')
                            ent_enq_ack_dt2_str=datetime.strftime(ent_enq_ack_dt2_obj,'%d-%m-%Y')
                            ent_enq_ack_dt2.insert(0,ent_enq_ack_dt2_str)
                        else:
                            ent_enq_ack_dt2.insert(0,' ')
                            
                        ent_enq_ack_dt3.delete(0,tk.END)                
                        if ent_enq_ack_dt3_rec:
                            ent_enq_ack_dt3_obj=datetime.strptime(str(ent_enq_ack_dt3_rec),'%Y-%m-%d')
                            ent_enq_ack_dt3_str=datetime.strftime(ent_enq_ack_dt3_obj,'%d-%m-%Y')
                            ent_enq_ack_dt3.insert(0,ent_enq_ack_dt3_str)
                        else:
                            ent_enq_ack_dt3.insert(0,' ')
                        
                        ent_enq_ack_dt4.delete(0,tk.END) 
                        if ent_enq_ack_dt4_rec:
                            ent_enq_ack_dt4_obj=datetime.strptime(str(ent_enq_ack_dt4_rec),'%Y-%m-%d')
                            ent_enq_ack_dt4_str=datetime.strftime(ent_enq_ack_dt4_obj,'%d-%m-%Y')
                            ent_enq_ack_dt4.insert(0,ent_enq_ack_dt4_str)
                        else:
                            ent_enq_ack_dt4.insert(0,' ')

                        ent_enq_ack_dt5.delete(0,tk.END)                
                        if ent_enq_ack_dt5_rec:
                            ent_enq_ack_dt5_obj=datetime.strptime(str(ent_enq_ack_dt5_rec),'%Y-%m-%d')
                            ent_enq_ack_dt5_str=datetime.strftime(ent_enq_ack_dt5_obj,'%d-%m-%Y')
                            ent_enq_ack_dt5.insert(0,ent_enq_ack_dt5_str)
                        else:
                            ent_enq_ack_dt5.insert(0,' ')
                        
                        ent_sus_dt.delete(0, tk.END)
                        if ent_sus_dt_rec:
                            ent_sus_dt_obj=datetime.strptime(str(ent_sus_dt_rec),'%Y-%m-%d')
                            ent_sus_dt_str=datetime.strftime(ent_sus_dt_obj,'%d-%m-%Y')
                            ent_sus_dt.insert(0,ent_sus_dt_str)
                        else:
                            ent_sus_dt.insert(0,' ')   
                        
                        ent_sus_rev.delete(0, tk.END)
                        if ent_sus_rev_rec:
                            ent_sus_rev_obj=datetime.strptime(str(ent_sus_rev_rec),'%Y-%m-%d') 
                            ent_sus_rev_str=datetime.strftime(ent_sus_rev_obj,'%d-%m-%Y')
                            ent_sus_rev.insert(0,ent_sus_rev_str)
                        else:
                            ent_sus_rev.insert(0,' ')        
                        
                        ent_fo_code.delete(0,tk.END)
                        if ent_fo_code_rec:
                            ent_fo_code.insert(0,ent_fo_code_rec)
                        else:    
                            ent_fo_code.insert(0 ,' ')

                        ent_fo_rem.delete(0,tk.END)
                        if ent_fo_rem_rec:
                            ent_fo_rem.insert(0,ent_fo_rem_rec)
                        else:    
                            ent_fo_rem.insert(0 ,' ')
                            
                        ent_fo_date.delete(0,tk.END)
                        if ent_fo_date_rec:
                            ent_fo_date_rec_obj=datetime.strptime(str(ent_fo_date_rec),'%Y-%m-%d')
                            ent_fo_date_rec_str=datetime.strftime(ent_fo_date_rec_obj,'%d-%m-%Y')
                            ent_fo_date.insert(0,ent_fo_date_rec_str)
                        else:
                            ent_fo_date.insert (0,'  ') 
                           
                        ent_no_inst.delete(0,tk.END)
                        if ent_no_inst_rec:
                            ent_no_inst.insert(0,ent_no_inst_rec)
                        else:
                            ent_no_inst.insert(0,' ')         
                    
                        ent_crt_dt.delete(0,tk.END)
                        if ent_crt_dt_rec:
                            ent_crt_dt_rec_obj=datetime.strptime(str(ent_crt_dt_rec),'%Y-%m-%d')
                            ent_crt_dt_rec_str=datetime.strftime(ent_crt_dt_rec_obj,'%d-%m-%Y')
                            ent_crt_dt.insert(0,ent_crt_dt_rec_str)
                        else:
                            ent_crt_dt.insert (0,'  ') 
                        
                        ent_details1.delete(0,tk.END)
                        if ent_details1_rec:
                            ent_details1.insert(0,  ent_details1_rec)
                        else:     
                            ent_details1.insert(0,' ')

                    cat_ent2.focus_set()
                                      

                else:
                    for widget in self.bottom_frame.winfo_children():
                        widget.configure(state='disabled')
                    messagebox.showerror("No Data","No Data avaialble-Check your entries ")
            
            cursor.close()
            corr_data.close()

        def update_corr():
            for widget in self.bottom_frame.winfo_children():
                #  if isinstance(widget, tk.Entry):  # Check if widget is an entry widget
                #     widget.config(bg='white')
                if type(self.winfo_children) is tk.Entry:
                    self.winfo_children.config(insertbackground='white')

            if cat_ent2.get()=='ADMIN':
                up_cat=1
            elif cat_ent2.get()=='CONDUCTOR':    
                up_cat=4
            elif cat_ent2.get()=='DRIVER':
                up_cat=3
            elif cat_ent2.get()=='TECH':
                up_cat=2 

            up_month_rec1=month_ent5.get()
            up_year_ent6=year_ent6.get()
            up_empno_ent7=empno_ent7.get()
            up_branch_ent8=branch_ent8.get()
            if veh_ent9.get():
                up_veh_ent9=veh_ent9.get()
            else:
                up_veh_ent9=' '
            
            up_off_code_ent10=off_code_ent10.get()

            if off_other_ent22.get():
                up_off_other_ent22=off_other_ent22. get()
            else:   
                up_off_other_ent22=' ' 

            if off_dt1_ent11.get():
                off_dt1_ent11lbl='OFFENCE DATE 1'
                try:
                    up_off_dt1_ent11= (lambda x: datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(off_dt1_ent11.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:  {off_dt1_ent11lbl}')
            else:
                up_off_dt1_ent11=None 

            if off_dt2_ent12.get():
                off_dt2_ent12lbl='OFFENCE DATE 2'
                try:
                    up_off_dt2_ent12=(lambda x: datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(off_dt2_ent12.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:  {off_dt2_ent12lbl}')
            else:
                up_off_dt2_ent12=None

            if off_rem_ent15.get():
                up_off_rem_ent15=off_rem_ent15. get()
            else:
                up_off_rem_ent15=' '

            if add_rem_ent16.get():
                up_add_rem_ent16=add_rem_ent16. get()
            else:
                up_add_rem_ent16=' '

            if ref_no_ent17.get():
                
                up_ref_no_ent17=ref_no_ent17. get()
            else:
                up_ref_no_ent17=' '

            if ref_dt_ent18.get().strip():
                ref_dt_ent18lbl='REFERENCE DATE'
                try:
                    up_ref_dt_ent18=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ref_dt_ent18.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:  {ref_dt_ent18lbl}')    
            else:
                up_ref_dt_ent18=None

            if from_ent19.get():
                up_from_ent19=from_ent19.get()
            else:
                up_from_ent19=' '    
            ########################################################## STORE DATA TO UPDATE VALUE
            if ent_memo_ack.get().strip():
                ent_memo_acklbl="MEMO ACK DATE"
                try:
                    up_memo_ack=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_memo_ack.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:{ent_memo_acklbl}')    
            else:
                up_memo_ack=None
            if ent_sus_ack_dt1.get().strip():
                ent_sus_ack_dt1lbl="SUSPENSION ACK DATE"
                try:
                    up_ent_sus_ack_dt1=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_sus_ack_dt1.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:  {ent_sus_ack_dt1lbl}')    

            else:
                up_ent_sus_ack_dt1=None
                
            # ent_scn_snt_dt1_rec=row[58]
            if ent_scn_snt_dt1.get().strip():
                ent_scn_snt_dt1lbl="SHOW CAUSE NOTICE DATE"
                try:
                    up_ent_scn_snt_dt1=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_scn_snt_dt1.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:{ent_scn_snt_dt1lbl}')    
            else:
                up_ent_scn_snt_dt1=None
            
            # ent_scn_ack_dt1_rec=row[57]
            if ent_scn_ack_dt1.get().strip():
                ent_scn_ack_dt1lbl="SHOWCAUSE NOTICE ACK DATE"
                try:
                    up_ent_scn_ack_dt1=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_scn_ack_dt1.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_scn_ack_dt1lbl}')    
            else:
                up_ent_scn_ack_dt1=None

            # ent_enq_dt1_rec=row[59]
            if ent_enq_dt1.get().strip():
                ent_enq_dt1lbl="Enquiry date 1"
                try:
                    up_ent_enq_dt1=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_dt1.get())
                except ValueError:
                   tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_enq_dt1lbl}')
            else:
                up_ent_enq_dt1=None

            # ent_enq_dt2_rec=row[60]
            if ent_enq_dt2.get().strip():
                ent_enq_dt2lbl='ENQUIRY DATE 2'
                try:
                    up_ent_enq_dt2=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_dt2.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_enq_dt2lbl}')
            else:
                up_ent_enq_dt2=None
                
            # ent_enq_dt3_rec=row[61]
            if ent_enq_dt3.get().strip():
                ent_enq_dt3lbl='ENQUIRY DATE 3'
                try:
                    up_ent_enq_dt3=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_dt3.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMA : {ent_enq_dt3lbl}')
            else:
                up_ent_enq_dt3=None

            # ent_enq_dt4_rec=row[62]
            if ent_enq_dt4.get().strip():
                ent_enq_dt4lbl='ENQUIRY DATE 4'
                try:
                    up_ent_enq_dt4=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_dt4.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_enq_dt4lbl}')    
            else:
                up_ent_enq_dt4=None

            # ent_enq_dt5_rec=row[63]
            if ent_enq_dt5.get().strip():
                ent_enq_dt5lbl='ENQUIRY DATE 5'
                try:
                    up_ent_enq_dt5=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_dt5.get())
                except ValueError:
                    tk.messagebox.showinfo('Error',f'WRONG DATE FORMAT :{ent_enq_dt5lbl}')    
            else:
                up_ent_enq_dt5=None
            # ent_enq_dt6_rec=row[64]
            if ent_enq_dt6.get().strip():
                ent_enq_dt6lbl='ENQUIRY DATE 6'
                try:
                    up_ent_enq_dt6=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_dt6.get())
                except ValueError:
                    tk.messagebox.showinfo('Error',f'WRONG DATE FORMAT: {ent_enq_dt6lbl}')
            else:
                up_ent_enq_dt6=None
                      
            # ent_enq_ack_dt1_rec=row[65]
            if ent_enq_ack_dt1.get().strip():
                ent_enq_ack_dt1lbl='ENQUIRY ACK DATE 1'
                try:
                    up_ent_enq_ack_dt1=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_ack_dt1.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_enq_ack_dt1lbl}')
            else:
                up_ent_enq_ack_dt1=None

            # ent_enq_ack_dt2_rec=row[66]
            if ent_enq_ack_dt2.get().strip():
                ent_enq_ack_dt2lbl='ENQUIRY ACK DATE 2'
                try:
                    up_ent_enq_ack_dt2=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_ack_dt2.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_enq_ack_dt2lbl}')
            else:
                up_ent_enq_ack_dt2=None
            # ent_enq_ack_dt3_rec=row[67]
            if ent_enq_ack_dt3.get().strip():
                ent_enq_ack_dt3lbl='ENQUIRY ACK DATE 3'
                try:
                    up_ent_enq_ack_dt3=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_ack_dt3.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:{ent_enq_ack_dt3lbl}')
            else:
                up_ent_enq_ack_dt3=None
            # ent_enq_ack_dt4_rec=row[68]
            if ent_enq_ack_dt4.get().strip():
                ent_enq_ack_dt4lbl='ENQUIRY ACK DATE 4'
                try:
                    up_ent_enq_ack_dt4=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_ack_dt4.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT: {ent_enq_ack_dt4lbl}')
            else:
                up_ent_enq_ack_dt4=None
            # ent_enq_ack_dt5_rec=row[69]
            if ent_enq_ack_dt5.get().strip():
                ent_enq_ack_dt5lbl='ENQUIRY ACK DATE 5'
                try:
                    up_ent_enq_ack_dt5=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_enq_ack_dt5.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORMAT:{ent_enq_ack_dt5lbl}')
            else:
                up_ent_enq_ack_dt5=None
            # ent_sus_dt_rec=row[16]
            if ent_sus_dt.get().strip():
                ent_sus_dtlbl='SUSPENSION DATE'
                try:
                    up_ent_sus_dt=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_sus_dt.get())
                except ValueError:
                    tk.messagebox.showinfo("Error",f'WRONG DATE FORAMT: {ent_sus_dtlbl}')
            else:
                up_ent_sus_dt=None 

            # ent_sus_rev_rec=row[17]
            if ent_sus_rev.get().strip():
                ent_sus_revlbl="SUSPENSION REVOKE DATE"
                try:
                    up_ent_sus_rev=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_sus_rev.get())
                except ValueError:
                    tk.messagebox.showinfo('Erro',f'WRONG DATE FORMAT: {ent_sus_revlbl}')
            else:
                up_ent_sus_rev=None 

            # ent_fo_code_rec=row[34]
            if ent_fo_code.get():
                up_ent_fo_code=ent_fo_code.get()
            else:
                up_ent_fo_code=None
                  
            # ent_fo_rem_rec=row[35]
            if ent_fo_rem.get():
                up_ent_fo_rem=ent_fo_rem.get()
            else:
                up_ent_fo_rem=None
            # ent_fo_date_rec=row[36]
            if ent_fo_date.get().strip():
                ent_fo_datelbl='  FO DATE'
                try:
                    up_ent_fo_date=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_fo_date.get())
                except ValueError:
                    tk.messagebox.showinfo('Error',f'WRONG DATE FORMAT: {ent_fo_datelbl}')
            else:
                up_ent_fo_date=None 

            # ent_no_inst_rec=row[37]
            if ent_no_inst.get() is not None and ent_no_inst.get().strip() != '':
                # print(type(ent_no_inst.get()))
                up_ent_no_inst_val=ent_no_inst.get().strip()
                # print(type(up_ent_no_inst_val))
                up_ent_no_inst=str(up_ent_no_inst_val)
                # print(type(up_ent_no_inst))
                # print(ent_no_inst.get())
            else:
                up_ent_no_inst=None
                # print('else'+ent_no_inst.get())

            # ent_crt_dt_rec=row[38]
            if ent_crt_dt.get().strip():
                ent_crt_dtlbl='CRT DATE'
                try:
                    up_ent_crt_dt=(lambda x:datetime.strptime(x,'%d-%m-%Y').strftime('%Y-%m-%d'))(ent_crt_dt.get())
                except ValueError:
                    tk.messagebox.showinfo('Error',f'WRONG DATE FORMAT: {ent_crt_dtlbl}')
            else:
                up_ent_crt_dt=None 

            # ent_details1_rec=row[41]
            if ent_details1.get():
                up_ent_details1=ent_details1.get()
            else:
                up_ent_details=None
            # update query
            try:
                upcorr_conn = dbcon.connect1()
                upcorr_data = upcorr_conn.cursor()

                update_query = ''' UPDATE ALLDIS SET CAT=%s,MTH=%s,YEA=%s,STNO=%s,BRANCH=%s,VEHNO=%s,OFFEN=%s,OFF_OTHERS=%s,DT_OFFEN=%s,
                                   DT_OFFEN2=%s,OFF_REM=%s,OFF_REM2=%s,REF_NO=%s,REF_DATE=%s,REF_FROM=%s,MEMO_ACK_DT=%s,
                                   SUS_ACK_DT=%s,SCN_SNT_DT=%s,SCN_ACK_DT=%s,ENQ_DT1=%s,ENQ_DT2=%s,ENQ_DT3=%s,ENQ_DT4=%s,ENQ_DT5=%s,ENQ_DT6=%s,
                                   ENQ_ACK_DT1=%s,ENQ_ACK_DT2=%s,ENQ_ACK_DT3=%s,ENQ_ACK_DT4=%s,ENQ_ACK_DT5=%s,
                                   DT_SUS=%s,SUS_REVOK=%s,FO_CODE=%s,FO_REM=%s,FO_DATE=%s,NO_INST=%s,CRT_DATE=%s,DETAILS=%s
                                   WHERE SNO=%s AND SEAT_NO=%s AND CRTNO=%s '''
                value=(up_cat,up_month_rec1,up_year_ent6, up_empno_ent7, up_branch_ent8, up_veh_ent9, up_off_code_ent10, 
                       up_off_other_ent22, up_off_dt1_ent11, up_off_dt2_ent12, up_off_rem_ent15, up_add_rem_ent16, up_ref_no_ent17,
                       up_ref_dt_ent18, up_from_ent19,up_memo_ack,up_ent_sus_ack_dt1,up_ent_scn_snt_dt1,up_ent_scn_ack_dt1,
                       up_ent_enq_dt1,up_ent_enq_dt2,up_ent_enq_dt3,up_ent_enq_dt4,up_ent_enq_dt5,up_ent_enq_dt6, 
                       up_ent_enq_ack_dt1,up_ent_enq_ack_dt2,up_ent_enq_ack_dt3,up_ent_enq_ack_dt4,up_ent_enq_ack_dt5,
                       up_ent_sus_dt,up_ent_sus_rev,up_ent_fo_code,up_ent_fo_rem,up_ent_fo_date,
                       up_ent_no_inst,up_ent_crt_dt,up_ent_details1,
                       case_rec,seat_rec, crt_rec)
                
                upcorr_data.execute(update_query, value     )
                upcorr_conn.commit()
                print(upcorr_data.rowcount, "records updated successfully.")
                lbl_updated.config(text='Updated sucessfully')
                ##after updation clear screen
                for widget in self.bottom_frame.winfo_children():
                    widget.configure(state='normal')
                    if isinstance(widget, tk.Entry):
                        widget.delete(0, tk.END)
                    widget.configure(state='disabled')    
            except Exception as e:
                print("Error updating records:", e)
                # tk.messagebox.showinfo("Error",f'Please check the entries {e}')
            finally:
                upcorr_data.close()
                upcorr_conn.close()

        def prt_fo():
            pass
        
        def fo_close():
            try:
                fo_close=dbcon.connect1()
                fo_data_cusror=fo_close.cursor()
                query='SELECT ID FROM alldis WHERE SNO=%s and SEAT_NO=%s and CRTNO=%s'
                values=(case_rec, seat_rec, int(crt_rec))
                fo_data_cusror.execute(query,values)
                fo_result=fo_data_cusror.fetchone()
                global fo_closeid
                fo_closeid=fo_result
                print(fo_closeid)
                fo_data_cusror.close()
                fo_close.close()
                
            except Exception as e:
                error_message = str(e) 
                print(error_message) 
                tk.messagebox.showinfo("Get alldis ID Error",{error_message})

            try:
                fo_process_con=dbcon.connect1()
                fo_process_cur=fo_process_con.cursor()
                from_table='alldis'
                to_table='trans_alldis'
                try:
                    print(fo_closeid)
                    fo_process_cur.execute(f'DESCRIBE {from_table}')
                    field_names=[field[0] for field in fo_process_cur.fetchall()]
                    field_names_str=','.join(field_names)
                    
                    # insert_query= f'INSERT INTO {to_table}({field_names_str})'\
                    #         f'SELECT {field_names_str} FROM {from_table}'\
                    #         f' WHERE id= %s'
                    ############################### first column excluded
                    insert_query = f'INSERT INTO {to_table}({",".join(field_names[1:])}) ' \
                    f'SELECT {",".join(field_names[1:])} FROM {from_table} ' \
                    f'WHERE id = %s'

                    fo_process_cur.execute(insert_query,(fo_closeid,))
                    fo_process_con.commit()
                    
                    select_query1='SELECT * FROM ALLDIS WHERE ID=%s'
                    select_values=(fo_closeid)
                    fo_process_cur.execute(select_query1,select_values)
                    fo_select_result=fo_process_cur.fetchall()
                    # print(fo_select_result)
                    # fo_process_cur.close()
                    for row in fo_select_result:
                        global fo_cat_rec, fo_case_rec, fo_seat_rec, fo_crt_rec, fo_empno_rec, fo_month_rec, fo_year_rec, fo_branch_rec, fo_veh_rec, fo_off_code_rec, fo_off_other_rec, fo_off_dt1_rec, fo_off_dt2_rec, fo_off_rem_rec, fo_add_rem_rec, fo_ref_no_rec, fo_ref_dt_rec, fo_from_rec
                        global fo_ent_memo_ack_rec,fo_ent_sus_ack_dt1_rec,  fo_ent_scn_snt_dt1_rec, fo_ent_scn_ack_dt1_rec, fo_ent_enq_dt1_rec,  fo_ent_enq_dt2_rec, fo_ent_enq_dt3_rec
                        global fo_ent_enq_dt4_rec,  fo_ent_enq_dt5_rec, fo_ent_enq_dt6_rec,fo_ent_enq_ack_dt1_rec, fo_ent_enq_ack_dt2_rec,  fo_ent_enq_ack_dt3_rec, fo_ent_enq_ack_dt4_rec 
                        global fo_ent_enq_ack_dt5_rec, fo_ent_sus_dt_rec, fo_ent_sus_rev_rec,  fo_ent_fo_code_rec, fo_ent_fo_rem_rec,  fo_ent_fo_date_rec, fo_ent_no_inst_rec, fo_ent_crt_dt_rec, fo_ent_details1_rec
 

                        # print(row)
                        fo_cat_rec=row[1]
                        fo_case_rec=row[4]
                        fo_seat_rec=row[5]
                        fo_crt_rec=row[6]
                        fo_empno_rec=row[7]
                        fo_oempno_rec=row[9]
                        fo_month_rec=row[2]
                        fo_year_rec=row[3]
                        fo_branch_rec=row[10]
                        fo_veh_rec=row[11]
                        fo_off_code_rec=row[12]
                        fo_off_other_rec=row[50]
                        fo_off_dt1_rec=row[15]
                        fo_off_dt2_rec=row[51]
                        fo_off_rem_rec=row[13]
                        fo_add_rem_rec=row[14]
                        fo_ref_no_rec=row[52]
                        fo_ref_dt_rec=row[53]
                        fo_from_rec=row[54]
                        fo_ent_memo_ack_rec=row[55]
                        fo_ent_sus_ack_dt1_rec=row[56]
                        fo_ent_scn_snt_dt1_rec=row[58]
                        fo_ent_scn_ack_dt1_rec=row[57]
                        
                        fo_ent_enq_dt1_rec=row[59]
                        fo_ent_enq_dt2_rec=row[60]
                        fo_ent_enq_dt3_rec=row[61]
                        fo_ent_enq_dt4_rec=row[62]
                        fo_ent_enq_dt5_rec=row[63]
                        fo_ent_enq_dt6_rec=row[64]

                        fo_ent_enq_ack_dt1_rec=row[65]
                        fo_ent_enq_ack_dt2_rec=row[66]
                        fo_ent_enq_ack_dt3_rec=row[67]
                        fo_ent_enq_ack_dt4_rec=row[68]
                        fo_ent_enq_ack_dt5_rec=row[69]

                        fo_ent_sus_dt_rec=row[16]                                     
                        fo_ent_sus_rev_rec=row[17]
                        fo_ent_fo_code_rec=row[34]
                        fo_ent_fo_rem_rec=row[35]
                        fo_ent_fo_date_rec=row[36]
                        fo_ent_no_inst_rec=row[37]
                        fo_ent_crt_dt_rec=row[38]
                        fo_ent_details1_rec=row[41]

                    try:
                        insert_query1='''
                        INSERT INTO PALLDIS (STNO,OSTNO,DATE,VEH,OFF,PUN,SNO,CRTNO,SEAT_NO,FO_DATE,YEA) 
                        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                        '''
                        insert_value1=(fo_empno_rec,fo_oempno_rec,fo_off_dt1_rec,fo_veh_rec,fo_ent_fo_code_rec,fo_off_rem_rec,
                                       fo_case_rec,fo_crt_rec,fo_seat_rec,fo_ent_fo_date_rec,fo_year_rec)
                        fo_process_cur.execute(insert_query1,(insert_value1))
                        fo_process_con.commit()

                        if fo_process_cur.rowcount > 0:
                        # Insert successful, delete the record from the from_table
                            delete_query = f'DELETE FROM {from_table} WHERE id = %s'
                            fo_process_cur.execute(delete_query, (fo_closeid,))
                            fo_process_con.commit()
                            print("Record deleted successfully from 'from_table'.")
                            ## after fo_close disable the bottom frame
                            for widget in self.bottom_frame.winfo_children():
                                # widget.configure(state='normal')
                                if isinstance(widget, tk.Entry):
                                    widget.delete(0, tk.END)
                                widget.configure(state='disabled')
                        else:
                            print("Insert failed, record not deleted.")
                    except Exception as e:
                        error_message2=str(e)
                        print(error_message2)

                except Exception as e:
                    error_message = str(e)  # Get the error message as a string
                    print(error_message)
                    tk.messagebox.showinfo("trans_alldis Insert Error",{error_message})

                fo_process_cur.execute(insert_query, (fo_closeid,))
                fo_process_con.commit()

                fo_process_cur.close()
                fo_process_con.close()

            except Exception as e:
                error_message1=str(e)
                tk.messagebox.showinfo("Alldis&Palldiss",F'{error_message1}')

            



        #Label Widgets 
        case_lbl=tk.Label(self.top_frame,text='Case no',font ="Helvetica 9 bold",fg ="navy blue",bg='lightcyan')
        seat_lbl=tk.Label(self.top_frame,text='Seat no',font ="Helvetica 9 bold",fg ="navy blue",bg='lightcyan')
        curr_lbl=tk.Label(self.top_frame,text='Current no',font ="Helvetica 9 bold",fg ="navy blue",bg='lightcyan')
        #Entry Widgets
        case_ent=tk.Entry(self.top_frame,width=13,font ="Helvetica 9 bold",fg ="navy blue")
        seat_ent=tk.Entry(self.top_frame,width=13,font ="Helvetica 9 bold",fg ="navy blue")
        curr_ent=tk.Entry(self.top_frame,width=13,font ="Helvetica 9 bold",fg ="navy blue")
        
        # Load Data
        load_btn=tk.Button(self.top_frame,text='Get Data',font ="Helvetica 9 bold",fg ="navy blue",command=load_data)
        load_btn.grid(row=0,column=6,padx=5,sticky='EW')
        case_lbl.grid(row=0,column=0,padx=5,sticky='EW')
        seat_lbl.grid(row=0,column=2,padx=5,sticky='EW')
        curr_lbl.grid(row=0,column=4,padx=5,sticky='EW')
        case_ent.grid(row=0,column=1, padx=5, pady=5, sticky='EW')
        seat_ent.grid(row=0,column=3, padx=5, pady=5, sticky='EW')
        curr_ent.grid(row=0,column=5, padx=5, pady=5, sticky='EW')
        
        #Bottom Frame widgets
        global tcase1
        tcase1=dbcon.lastcaseno()
        off_data=dbcon.offcode()
        offence_values = [d["O_CODE"] for d in off_data]
        sorted_offence_values = sorted(offence_values, key=lambda x: x)
        # offence_remarks = [d["OFFENCE"] for d in off_data]

        def on_off_sele(event):
            selected_offence = off_code_ent10.get()
            for d in off_data:
                if d["O_CODE"] == selected_offence:
                    # offence_remarks = d["OFFENCE"]
                    offence_remarks=d["OFFENCE"][:25] + "..." if len(d["OFFENCE"]) > 25 else d["OFFENCE"]
                    off_lbl.configure(text=offence_remarks)
                    break

        #Label Widgets 
        case_lblb=tk.Label(self.bottom_frame,text='Case no',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        seat_lblb=tk.Label(self.bottom_frame,text='Seat no',font ="Helvetica 9 bold",fg ="navy blue",bg="oldlace")
        curr_lblb=tk.Label(self.bottom_frame,text='Current no',font ="Helvetica 9 bold",fg ="navy blue",bg="oldlace")
        # Entry Widgets
        case_entb=tk.Entry(self.bottom_frame,width=12,font ="Helvetica 9 bold",fg ="navy blue")
        seat_entb=tk.Entry(self.bottom_frame,width=12,font ="Helvetica 9 bold",fg ="navy blue")
        curr_entb=tk.Entry(self.bottom_frame,width=12,font ="Helvetica 9 bold",fg ="navy blue")
        # lbl_updated=tk.Label(self,text='Kaleem ', bg='#C9C0BB').grid(row=2,column=0,sticky='w')

        cat_lbl=tk.Label(self.bottom_frame,text="Category",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        month_lbl=tk.Label(self.bottom_frame,text="Month",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")  
        year_lbl=tk.Label(self.bottom_frame,text="Year",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")      
        empno_lbl=tk.Label(self.bottom_frame,text="Emp No",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")       
        branch_lbl=tk.Label(self.bottom_frame,text="Branch",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")      
        veh_lbl=tk.Label(self.bottom_frame,text="Vehicle No",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        off_code_lbl=tk.Label(self.bottom_frame,text="Offence Code",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        off_dt1_lbl=tk.Label(self.bottom_frame,text="Off.Date1",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        off_dt2_lbl=tk.Label(self.bottom_frame,text="Off.Date2",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        off_lbl=tk.Label(self.down_frame,text='Offence',width=35,anchor='w',font ="Helvetica 9 bold",fg ="navy blue", bg="White")
        offoth_lbl=tk.Label(self.bottom_frame,text='Oth.Offence',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        off_rem_lbl=tk.Label(self.bottom_frame,text="Off.Remarks",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        add_rem_lbl=tk.Label(self.bottom_frame,text="Add.Remarks",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        ref_no_lbl=tk.Label(self.bottom_frame,text="Ref.No",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
       
        cat_lbl.grid(row=0, column=0, padx=3, pady=3, sticky="w")
        month_lbl.grid(row=1,column=0,padx=3,pady=3,sticky="w")
        year_lbl.grid(row=2,column=0,padx=3,pady=3,sticky="w")
        empno_lbl.grid(row=3,column=0,padx=3,pady=3,sticky="w")
        branch_lbl.grid(row=4,column=0,padx=3,pady=3,sticky="w")
        veh_lbl.grid(row=5,column=0,padx=3,pady=3,sticky="w")
        off_code_lbl.grid(row=6,column=0,padx=3,pady=3,sticky="w")
        offoth_lbl.grid(row=7,column=0,padx=3,pady=3,sticky="w")
        off_dt1_lbl.grid(row=8,column=0,padx=3,pady=3,sticky="w")
        off_dt2_lbl.grid(row=9,column=0,padx=3,pady=3,sticky="w")
        off_lbl.grid(row=13,column=1,padx=3,pady=3,sticky="w")
        off_rem_lbl.grid(row=10,column=0,padx=3,pady=3,sticky="w")
        add_rem_lbl.grid(row=11,column=0,padx=3,pady=3,sticky="w")
        ref_no_lbl.grid(row=12,column=0,padx=3,pady=3,sticky="w")

        cat_list=["ADMIN","CONDUCTOR","DRIVER","TECH"]
        cat_ent2=ttk.Combobox(self.bottom_frame,values=cat_list,width=15)
        # cat_ent2.focus_set()
        month_ent5=tk.Entry(self.bottom_frame,width=15)                                        # category
        year_ent6=tk.Entry(self.bottom_frame,width=15)                                         # Month
        empno_ent7=tk.Entry(self.bottom_frame,width=15)                                        # year 
        branch_ent8=tk.Entry(self.bottom_frame,width=15)                                       #emp No.
        veh_ent9=tk.Entry(self.bottom_frame,width=15)                                          # Branch
        off_code_ent10=ttk.Combobox(self.bottom_frame,values=sorted_offence_values,width=5)                                       #vehicle
        off_code_ent10.bind('<<ComboboxSelected>>',on_off_sele)
        off_other_ent22=tk.Entry(self.bottom_frame,width=15)
        off_dt1_ent11=tk.Entry(self.bottom_frame,width=12)
        off_dt2_ent12=tk.Entry(self.bottom_frame,width=12)
        off_rem_ent15=tk.Entry(self.bottom_frame,width=25)                                      # Date of Exp
        add_rem_ent16=tk.Entry(self.bottom_frame,width=25)                                      # offe remar
        ref_no_ent17=tk.Entry(self.bottom_frame,width=25) 

       # curr_ent4=tk.Entry(self.bottom_frame,width=15)                                         # case no
                                              # additional remarks
        cat_ent2.grid(row=0,column=1,sticky="w",padx=3,pady=3)
        month_ent5.grid(row=1,column=1,sticky="w",padx=3,pady=3)
        year_ent6.grid(row=2,column=1,sticky="w",padx=3,pady=3)
        empno_ent7.grid(row=3,column=1,sticky="w",padx=3,pady=3)
        branch_ent8.grid(row=4,column=1,sticky="w",padx=3,pady=3)
        veh_ent9.grid(row=5,column=1,sticky="w",padx=3,pady=3)
        off_code_ent10.grid(row=6,column=1,sticky="w",padx=3,pady=3)
        off_other_ent22.grid(row=7,column=1,sticky="w",padx=3,pady=3)
        off_dt1_ent11.grid(row=8,column=1,sticky="w",padx=3,pady=3)
        off_dt2_ent12.grid(row=9,column=1,sticky="w",padx=3,pady=3)
        off_rem_ent15.grid(row=10,column=1,sticky="w",padx=3,pady=3)
        add_rem_ent16.grid(row=11,column=1,sticky="w",padx=3,pady=3)
        ref_no_ent17.grid(row=12,column=1,sticky="w",padx=3,pady=3)

        
        ############# Label for right_bottom entry
        # from_lbl=tk.Label(self.bottom_frame,text="From :",bg='#C9C0BB')
        ref_dt_lbl=tk.Label(self.bottom_frame,text="Ref.Date",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        ref_dt_lbl.grid(row=0,column=2,sticky="w")
        from_lbl=tk.Label(self.bottom_frame,text="From",font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        from_lbl.grid(row=1,column=2,sticky="w")
        lbl_memo_ack_dt1=tk.Label(self.bottom_frame,text='MemoAckDate',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_memo_ack_dt1.grid(row=2,column=2,sticky="w")
        lbl_sus_ack_dt1=tk.Label(self.bottom_frame,text='SusAckDate',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_sus_ack_dt1.grid(row=3,column=2,sticky="w")
        
        lbl_scn_snt_dt1=tk.Label(self.bottom_frame,text='SCN_SentDate',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_scn_snt_dt1.grid(row=4,column=2,sticky="w")
        
        lbl_scn_ack_dt1=tk.Label(self.bottom_frame,text='SCN_AckDate1',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_scn_ack_dt1.grid(row=5,column=2,sticky="w")
        
        lbl_enq_dt1=tk.Label(self.bottom_frame,text='Enquiry Date1',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_dt1.grid(row=6,column=2,sticky="w")
        lbl_enq_dt2=tk.Label(self.bottom_frame,text='Enquiry Date2',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_dt2.grid(row=7,column=2,sticky="w")
        lbl_enq_dt3=tk.Label(self.bottom_frame,text='Enquiry Date3',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_dt3.grid(row=8,column=2,sticky="w")
        lbl_enq_dt4=tk.Label(self.bottom_frame,text='Enquiry Date4',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_dt4.grid(row=9,column=2,sticky="w")
        lbl_enq_dt5=tk.Label(self.bottom_frame,text='Enquiry Date5',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_dt5.grid(row=10,column=2,sticky="w")       
        lbl_enq_dt6=tk.Label(self.bottom_frame,text='Enquiry Date6',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_dt6.grid(row=11,column=2,sticky="w")
        lbl_enq_ack_dt1=tk.Label(self.bottom_frame,text='Enq.Ack Date1',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_ack_dt1.grid(row=12,column=2,sticky="W")
        
        ref_dt_ent18=tk.Entry(self.bottom_frame,width=12)
        ref_dt_ent18.grid(row=0,column=3,sticky="w",padx=3,pady=3)
        from_ent19=tk.Entry(self.bottom_frame,width=12) 
        from_ent19.grid(row=1,column=3,sticky="w",padx=3,pady=3)
        ent_memo_ack=tk.Entry(self.bottom_frame,width=12) 
        ent_memo_ack.grid(row=2,column=3,sticky="w")
        ent_sus_ack_dt1=tk.Entry(self.bottom_frame,width=12)
        ent_sus_ack_dt1.grid(row=3,column=3,sticky="w") 
        ent_scn_snt_dt1=tk.Entry(self.bottom_frame,width=12)
        ent_scn_snt_dt1.grid(row=4,column=3,sticky="w")
        ent_scn_ack_dt1=tk.Entry(self.bottom_frame,width=12)
        ent_scn_ack_dt1.grid(row=5,column=3,sticky="w")
        ent_enq_dt1=tk.Entry(self.bottom_frame,width=12)
        ent_enq_dt1.grid(row=6,column=3,sticky="w")
        ent_enq_dt2 =tk.Entry(self.bottom_frame,width=12)
        ent_enq_dt2.grid(row=7,column=3,sticky="w")
        ent_enq_dt3=tk.Entry(self.bottom_frame,width=12)
        ent_enq_dt3.grid(row=8,column=3,sticky="w")
        ent_enq_dt4=tk.Entry(self.bottom_frame,width=12)
        ent_enq_dt4.grid(row=9,column=3,sticky="w")
        ent_enq_dt5=tk.Entry(self.bottom_frame,width=12)
        ent_enq_dt5.grid(row=10,column=3,sticky="w")
        ent_enq_dt6=tk.Entry(self.bottom_frame,width=12)
        ent_enq_dt6.grid(row=11,column=3,sticky="w")
        ent_enq_ack_dt1=tk.Entry(self.bottom_frame,width=12)
        ent_enq_ack_dt1.grid(row=12,column=3,sticky="w")

        lbl_enq_ack_dt2=tk.Label(self.bottom_frame,text='Enq.Ack Date2',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_ack_dt2.grid(row=0,column=4,sticky="w")
        lbl_enq_ack_dt3=tk.Label(self.bottom_frame,text='Enq.Ack Date3',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_ack_dt3.grid(row=1,column=4,sticky="w")
        lbl_enq_ack_dt4=tk.Label(self.bottom_frame,text='Enq.Ack Date4',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_ack_dt4.grid(row=2,column=4,sticky="w")
        lbl_enq_ack_dt5=tk.Label(self.bottom_frame,text='Enq.Ack Date5',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace")
        lbl_enq_ack_dt5.grid(row=3,column=4,sticky="w")       
        lbl_sus_dt=tk.Label(self.bottom_frame,text='Sus Date ',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=4,column=4,sticky='w')
        lbl_sus_rev=tk.Label(self.bottom_frame,text='Sus Revok',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=5,column=4,sticky='w')
        lbl_fo_code=tk.Label(self.bottom_frame,text='FO CODE',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=6,column=4,sticky='w')
        lbl_fo_rem=tk.Label(self.bottom_frame,text='FO Remarks',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=7,column=4,sticky='w')
        lbl_fo_date=tk.Label(self.bottom_frame,text='FO Date',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=8,column=4,sticky='w')
        lbl_no_inst =tk.Label(self.bottom_frame,text='No Inst',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=9,column=4,sticky='w')
        lbl_crt_dt =tk.Label(self.bottom_frame,text='CRT DATE',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=10,column=4,sticky='w')
        lbl_details1 =tk.Label(self.bottom_frame,text='Details',font ="Helvetica 9 bold",fg ="navy blue", bg="oldlace").grid(row=11,column=4,sticky='w')
       
        ent_enq_ack_dt2 =tk.Entry(self.bottom_frame,width=12)
        ent_enq_ack_dt2.grid(row=0,column=5)
        ent_enq_ack_dt3=tk.Entry(self.bottom_frame,width=12)
        ent_enq_ack_dt3.grid(row=1, column=5)
        ent_enq_ack_dt4=tk.Entry(self.bottom_frame,width=12)
        ent_enq_ack_dt4.grid(row=2,column=5)
        ent_enq_ack_dt5=tk.Entry(self.bottom_frame,width=12)
        ent_enq_ack_dt5.grid(row=3,column=5)
        
        ent_sus_dt=tk.Entry(self.bottom_frame,width=12)
        ent_sus_dt.grid(row=4,column=5)                                       
        ent_sus_rev=tk.Entry(self.bottom_frame,width=12)
        ent_sus_rev.grid(row=5,column=5)                                       
        ent_fo_code=tk.Entry(self.bottom_frame,width=12)
        ent_fo_code.grid(row=6,column=5)                                               
        ent_fo_rem=tk.Entry(self.bottom_frame,width=12)
        ent_fo_rem.grid(row=7,column=5)                                               
        ent_fo_date=tk.Entry(self.bottom_frame,width=12)
        ent_fo_date.grid(row=8,column=5)                                                 
        ent_no_inst=tk.Entry(self.bottom_frame,width=12)
        ent_no_inst.grid(row=9,column=5)                                                 
        ent_crt_dt=tk.Entry(self.bottom_frame,width=12)
        ent_crt_dt.grid(row=10,column=5)                                 
        ent_details1=tk.Entry(self.bottom_frame,width=12)
        ent_details1.grid(row=11,column=5)                                 

        
        case_lblb.grid(row=0,column=6,sticky="e")
        seat_lblb.grid(row=1,column=6,sticky="e")
        curr_lblb.grid(row=2,column=6,sticky="e")
        case_entb.grid(row=0,column=7,sticky="w")
        seat_entb.grid(row=1,column=7,sticky="w")
        curr_entb.grid(row=2,column=7,sticky="w")
        
        # case_entb=tk.Entry(self.bottom_frame,width=10).grid(row=7,column=5,sticky="w")
        # seat_entb=tk.Entry(self.bottom_frame,width=10).grid(row=8,column=5,sticky="w")
        # curr_entb=tk.Entry(self.bottom_frame,width=10).grid(row=9,column=5,sticky="w")
        
        updt_btn=tk.Button(self.bottom_frame,text='Update',command=update_corr)
        updt_btn.grid(row=3,column=7,sticky='w')
        prt_fo_btn=tk.Button(self.bottom_frame,text='FO Print',command=prt_fo)
        prt_fo_btn.grid(row=4,column=7,sticky='w')
        fo_close_btn=tk.Button(self.bottom_frame,text='FO CLOSE',command=fo_close)
        fo_close_btn.grid(row=5,column=7,sticky='w')

        self.place(width=800,height=550, x=0,y=0)
        self.grid_propagate(False)
        self.update_idletasks()

        for widget in self.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
        for widget in self.top_frame.winfo_children():
            # widget.grid_configure(padx=15,pady=5,ipadx=1,ipady=1)
            widget.grid_propagate(False)
        for widget in self.bottom_frame.winfo_children():
            widget.grid_propagate(False)
            widget.configure(state='disabled')
       
        
        


# # # To check this frame
# if __name__ == "__main__":
#     root = tk.Tk()
#     corr = class_corr(root)
#     corr.mainloop()