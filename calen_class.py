from tkcalendar import DateEntry

class NullableDateEntry(DateEntry):
    def __init__(self, master=None, **kw):
        super().__init__(master, **kw)
        self.bind('<<DateEntrySelected>>', self._on_select)
        self.bind('<Escape>', self._on_escape)
        self.bind('<Tab>',self._on_tab)
        self._is_blank = False

    def set_date(self, date):
        if date is None:
            self._is_blank = True
            self.delete(0, 'end')
        else:
            self._is_blank = False
            super().set_date(date)

    def get(self):
        if self._is_blank:
            return None
        else:
            return super().get()

    def _on_select(self, event=None):
        self._is_blank = False

    def _on_escape(self, event=None):
        self.set_date(None)

    def _on_tab(self, event=None):
        # if self.get_date() is None:
        #     self._is_blank = True
        #     self.delete(0, 'end')
        # else:
        #     self._is_blank = False
        if self.get_date() is None and self.focus_get() == self:
            self._is_blank = True
            self.delete(0, 'end')
        else:
            self._is_blank = False
