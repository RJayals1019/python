from datetime import datetime
import dbcon
import textwrap
pnstno='S2M900047'

phis_conn = dbcon.connect1()

aldis_cur=phis_conn.cursor()
aldisquery='SELECT * FROM alldis WHERE STNO=%s'
pvalue=(pnstno,)
aldis_cur.execute(aldisquery,pvalue)
aldis_result=aldis_cur.fetchall()
aldis_result_list=[] 
aldis_result_dict={}
# print(aldis_cur.description)
if aldis_result :
    keynames=[column[0] for column in aldis_cur.description]
    for record in aldis_result:
        aldis_result_dict = dict(zip(keynames, record))
        aldis_result_list.append(aldis_result_dict)
        
# print(aldis_result_list)
for idx,record in enumerate(aldis_result_list):
    print(idx,record['STNO'])
    