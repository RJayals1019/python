import tkinter as tk
from tkinter import ttk

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        
        # create custom style for hyperlink-like text
        style = ttk.Style()
        style.map("Hyperlink.TButton",
                  foreground=[("active", "blue"), ("!disabled", "blue")])
        
        # create list of report names
        reports = ["Report 1", "Report 2", "Report 3", "Report 4", "Report 5"]
        
        # create dictionary to store report selections
        self.selected_reports = {}
        for report in reports:
            self.selected_reports[report] = tk.BooleanVar()
        
        # create the hyperlink-like text for each report
        for report in reports:
            button = ttk.Button(self, text=report, style="Hyperlink.TButton",
                                command=lambda r=report: self.toggle_report(r))
            button.pack(padx=20, pady=5)
        
        # create button to display selected reports
        report_button = ttk.Button(self, text="Display Selected Reports", command=self.display_reports)
        report_button.pack(padx=20, pady=10)
        
    def toggle_report(self, report):
        self.selected_reports[report].set(not self.selected_reports[report].get())
        
    def display_reports(self):
        selected = [report for report, selected in self.selected_reports.items() if selected.get()]
        if selected:
            print("Selected reports:", selected)
        else:
            print("No reports selected")

app = App()
app.mainloop()
