import tkinter as tk
from tkinter import *
from datetime import datetime
from PIL import Image
import dbcon
import pymysql
from docxtpl import DocxTemplate
import os
import shutil
import subprocess
from tkinter import messagebox
from tkcalendar import DateEntry
import mysql.connector
# from tabulate import tabulate
from prettytable import PrettyTable
import os
import textwrap
from collections import Counter
import calendar
import mon_abs
from mon_abs import mon_abs_report
class reports(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')
       
        ### Frame
        self.topframe=tk.LabelFrame(self,text='Reports',background='#C9C0BB')
        self.topframe.grid(row=0,column=0)


        
        def month_pend():
            mon_abs.mon_abs_report()

        mon_rep_bnt=tk.Button(self.topframe,text='Mon Pen',background='#C9C0BB',command=month_pend)
        mon_rep_bnt.grid(row=0,column=0)


        for widget in self.topframe.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
            widget.update_idletasks()
        
        self.place(width=800,height=500, x=0,y=0)
        self.grid_propagate(False)
        self.update()