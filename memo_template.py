import tkinter as tk
from tkinter import *
import tkinter.ttk as ttk
import dbcon
import datetime
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo

class memo_template_class(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master,width=800,height=500,background='#C9C0BB')
        self.top_frame=tk.LabelFrame(self, text='Memo Templates', width=790, height=400, background='#C9C0BB')
        self.top_frame.grid(row=0,column=0)
       
        memo_conn=dbcon.connect1()
        def update_memo(event):
            tree = event.widget
            selection = tree.selection()
            if not selection:
                return
            item = selection[0]
            column = tree.identify_column(event.x)
            if column == '#3':
                tree.item(item, tags=("editing",))
                tree.selection_set(item)
                tree.focus(item)
                tree.editing_id = item
                tree.editing_col = column
                #tree.set(item, column=column, value="")
                tree.editing_val = tree.set(item, column=column)
                tree.bind('<Key>', edit_key)  # Bind the key event for editing

        def edit_key(event):
            tree = event.widget
            if tree.editing_id:
                item = tree.editing_id
                column = tree.editing_col
                current_val = tree.set(item, column=column)
                new_val = current_val
                if event.keysym == 'BackSpace':
                    new_val = current_val[:-1]  # Remove last character
                elif event.char.isprintable():
                    new_val = current_val + event.char  # Add typed character
                # Check if the new value is in date format, assuming column 3 is a date entry field
                if not is_valid_date(new_val) and new_val != "":
                    # Set the cell background to red to indicate invalid input
                    tree.set(item, column=column, value=new_val)
                    tree.item(item, tags=("editing", "error"))
                else:
                    # Update the cell with the new value
                    tree.set(item, column=column, value=new_val)

        def is_valid_date(date_str):
            # Implement your date validation logic here, return True if valid, False otherwise
            # You can use a date parsing library like datetime to validate date format
            # Example: return True if date_str is in format yyyy-mm-dd, else False
            try:
                datetime.datetime.strptime(date_str, '%Y-%m-%d')
                return True
            except ValueError:
                return False

        
        def save_memo_template(event):
            tree = event.widget
            item = tree.editing_id
            column = tree.editing_col
            new_value = tree.set(item, column)
            values = tree.item(item, 'values')  # Retrieve the values of the item
            if values:
                offen = values[0]  # Extract the value from the 'values' tuple
            else:
                offen = None
            memo_conn1=dbcon.connect1()
            memo_conn= memo_conn1.cursor()
            with memo_conn as cursor:
                 query = "UPDATE MEMO_MASTER SET MEMO_TEMPLATE=%s WHERE OFFEN=%s"
                 cursor.execute(query, (new_value, offen))
            memo_conn1.commit()
            tk.messagebox.showinfo("success","Data Updated SuccessFully...!!!")
        def select_files(event):
            tree = event.widget
            selection = tree.selection()
            if not selection:
                return
            item = selection[0]
            column = tree.identify_column(event.x)
            filetypes = (('text files', '*.txt'),('All files', '*.*'))
            filenames = fd.askopenfilenames(title='Open files',
                                            initialdir='/',
                                            filetypes=filetypes)
            showinfo(title='Selected Files',message=filenames)
            tree.set(item, column=column, value=filenames)
        
        def end_editing(event):
            tree = event.widget
            selection = tree.selection()
            if not selection:
                return
            item = selection[0]
            column = tree.identify_column(event.x)
            if column != '#1' and column != '#2':
                tree.set(item, column=column, value="")
        tree=ttk.Treeview(self.top_frame,height=20)
        tree.grid(row=1, column=0, sticky="nsew")
        tree.focus_set()
        tree["columns"]  =("offen", "offence", "memo_template")
        tree.column("offen", width=55,minwidth=15,stretch=NO)
        tree.column("offence", width=200,minwidth=15,stretch=NO)
        tree.column("memo_template", width=500,minwidth=15)
        tree.column('#0', width=0, stretch=NO) # Hide the first column
        # Set the column headings
        tree.heading("offen", text="Offen",anchor='w')
        tree.heading("offence", text="Name",anchor='w')
        tree.heading("memo_template", text="Memo Template",anchor='w')
        with memo_conn.cursor() as cursor:
            cursor.execute("SELECT OFFEN,OFFENCE,MEMO_TEMPLATE FROM MEMO_MASTER")
            rows=cursor.fetchall()
            for row in rows:
                tree.insert("","end",values=row)

        tree.tag_configure("editing",background="blue")
        tree.bind("<Button-1>",update_memo)
        tree.bind("<Return>",save_memo_template)
        tree.bind("<BackSpace>", end_editing)
        tree.bind("<Button-3>",select_files)
        #tk.Button(root,text='Test',command= save_memo_template).pack()
        memo_conn.close()



        for widget in self.top_frame.winfo_children():
            widget.grid_configure(padx=2,pady=3,ipadx=1,ipady=1)
            widget.grid_propagate(False)
            widget.update_idletasks()
        
        # self.pack(fill=BOTH, expand=True)
        self.place(width=800,height=500, x=0,y=0)
        self.grid_propagate(False)
        self.update_idletasks()
        

# # # # To check this frame
if __name__ == "__main__":
    root = tk.Tk()
    memo_template = memo_template_class(root)
    memo_template.mainloop()
   
   